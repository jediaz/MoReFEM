import os
import subprocess




if __name__ == "__main__":

    morefem_source_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), "..", "..", "Sources")

    cmd = \
        [
            "cppcheck",
            "--std=c++14",
            "--enable=all",
            "-f",
            "-q",
            morefem_source_dir,
            "-i",
            "{}/ThirdParty/Source".format(morefem_source_dir),
            "-I{}".format(morefem_source_dir),
            "-DF77_NO_UNDER_SCORE",
            "-DTRANSMESH"
            ]
        

    FILE=open('cpp_check_report.txt', 'w')

    subprocess.Popen(cmd, shell=False, stdout=FILE, stderr=FILE).communicate()

    print("Output written in cpp_check_report.txt")