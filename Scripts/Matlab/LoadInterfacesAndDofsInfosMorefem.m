function [interfaces_indices, dof_infos_indices] = LoadInterfacesAndDofsInfosMoReFEM(path_hh, mesh_number, numbering_subset_number)

interfaces_file_name = [path_hh 'interfaces_1.hhdata'];
dof_infos_name = [path_hh 'Mesh_' num2str(mesh_number) '/NumberingSubset_' num2str(numbering_subset_number) '/dof_information_proc_0.hhdata'];

interfaces_file = fopen( interfaces_file_name, 'rt' );

% size du nombre de noeuds, un indice ici correspond a l'indice du noeud
% dans le maillage de depart
interfaces_indices = [];

while (~feof( interfaces_file ))
    str = fgetl( interfaces_file );
    str = strtrim(str);
    if (strfind(str,'Vertex'))
        beginPos = strfind(str,'[');
        endPos = strfind(str,']');
        interfaces_indices = [interfaces_indices str2num(str(beginPos+1:endPos-1))];
    end
end

fclose( interfaces_file );

dofs_infos_file = fopen( dof_infos_name, 'rt' );

% size du nombre de dofs, un indice stocke ici correspond a un indice de
% vertex
dof_infos_indices = [];

while (~feof( dofs_infos_file ))
    str = fgetl( dofs_infos_file );
    str = strtrim(str);
    if (strfind(str,'Vertex'))
        beginPos = strfind(str,'Vertex');
        endPos = strfind(str,';');
        dof_infos_indices = [dof_infos_indices str2num(str((beginPos+length('Vertex')):(endPos(3)-1)))];
    end
end

fclose( dofs_infos_file );

end