Example of a 1D plot with MoReFEM data.
	
To make sure the results of your 1D simulation in MoReFEM is compatible with this script and especially the 'load_files.m' function, one should create its 1D Ensight mesh with the script 1D_Ensight_Mesh.py available in 'Scripts'. 
The numbering of the mesh is done so that MoReFEM will not do a renumbering hence result files directly useable.