import os
import subprocess
import tempfile

import sys

utilities_abspath = os.path.join(os.path.dirname(os.path.realpath(__file__)), "..", "Utilities")
sys.path.append(utilities_abspath)

from substitute_env_variable import SubstituteEnvVariable


class FileHeader():
    
    def __init__(self, directory, folder_ignore_list, module_ignore_list):
        """
        \param[in] folder_ignore_list Folder within this list will be ignored.
        \param[in] module_ignore_list Module names inside this list won't be associated to a Doxygen group.
        """
        self._directory = SubstituteEnvVariable(directory)
        
        self._copyright = "Copyright (c) Inria. All rights reserved."

        if not  os.path.exists(self._directory):
            print("[WARNING] Directory {0} not found; couldn't correct header guards inside.".format(self._directory))
        else:
            self.Perform(folder_ignore_list, module_ignore_list)
        
    def Perform(self, folder_ignore_list, module_ignore_list):
        """Apply the file header correction on each C++ file within self._directory."""
        main_directory_folders = os.listdir(self._directory) 
        
        main_directory_folders = [item for item in main_directory_folders if os.path.isdir(os.path.join(self._directory, item))]

        # Do the same for folders to ignore.
        modified_folder_ignore_list = []

        for item in folder_ignore_list:
            item = os.path.join(self._directory, SubstituteEnvVariable(item))
            modified_folder_ignore_list.append(item)
            
        print("This may take few minutes...")

        for folder in main_directory_folders:
            
            if folder not in module_ignore_list:
                group = "{0}Group".format(folder)
            else:
                group = None
            
            print("Managing directory {0}".format(folder))
            
            folder_complete_path = os.path.join(self._directory, folder)

            for root, dirs, files in os.walk(folder_complete_path, topdown=True):
                
                skip = False
                for item in modified_folder_ignore_list:
                    if root.startswith(item):
                        skip = True
            
                # Skip files that are located within ignored folders.    
                if skip:
                    continue
            
                # Consider only C++ files.
                files = [item for item in files if os.path.splitext(item)[1] in (".cpp", ".hpp", ".hxx")]
                
                for myfile in files:
                    file_complete_path = os.path.join(root, myfile)
                    
                    # If the file begins by main, skip the group.
                    if myfile.startswith("main"):
                        self.ApplyFileHeader(file_complete_path, None)
                    else:
                        self.ApplyFileHeader(file_complete_path, group)

            
    def ApplyFileHeader(self, file_complete_path, group_name):        
        """Apply the file header correction on the given file."""            
        
        try:
            rewritten_file = tempfile.NamedTemporaryFile(delete = False)
        
            # First generate the proper header.
            cmd = "git log --diff-filter=A --follow --format=\"Created by %aN <%aE> on the %aD\" -- {0} | tail -1".format(file_complete_path)
        
            check = subprocess.Popen(cmd, shell = True, stdout = subprocess.PIPE).communicate()
        
            author_info = (check[0].strip()).decode('utf-8')
                
            header = "// \\file\n//\n//\n// {creation}\n// {copyright}\n//\n".format(creation = author_info,            copyright = self._copyright)
            if group_name:
                header = "//\n{previous}// \\ingroup {group}\n// \\addtogroup {group}\n// \\{{\n".format(previous = header, group = group_name)
            
            header = "/*!\n{}*/\n\n".format(header)
        
            # Now read the original file, and store it into a list (minus its header).
            FILE_in = open(file_complete_path)
        
            rewritten_file.write(bytes(header, 'utf-8'))
        
            original_header_read = False
        
            content = []
        
            for line in FILE_in:
                if not original_header_read and (line.lstrip().startswith("//") or line.lstrip().startswith("/*")):
                    continue
                
                original_header_read = True
            
                content.append(line)
            
            # Skip first empty lines if any.
            first_line = content[0].strip("\t\r\n ")
            while not first_line:
                del content[0]
                first_line = content[0].strip("\t\r\n ")
            
            # See when ingroup closure should be put: read line reversely until a namespace is found.
            if group_name:
                Nline = len(content)
                end_group = "// @}} // addtogroup {0}".format(group_name)
        
                position_found = False
            

                for i in range(Nline -1, 0, -1):
            
                    current_line = content[i]
            
                    # If a previous run already create the end of block, do not recreate one!
                    if end_group in current_line:
                        position_found = True
                    
                    # If a PRAGMA_DIAGNOSTIC(pop) line is found, it's likely we're in a ThirdParty/IncludeWithoutWarning file.
                    elif "PRAGMA_DIAGNOSTIC(pop)" in current_line:
                        content.insert(i + 1, "\n\n{0}\n".format(end_group))
                        position_found = True
                    
                    # More common case: see a closing namespace (addtogroup closure should be just after that).
                    elif "} // namespace" in current_line:
                        content.insert(i + 1, "\n\n{0}\n".format(end_group))
                        position_found = True
                    
                    # If placeholder was put, replace it!
                    elif "<-- END GROUP -->" in current_line:
                        content[i] = "\n\n{0}\n".format(end_group) 
                        position_found = True                    
                    
                    if position_found:
                        break
                
                if not position_found:
                    raise Exception("ERROR for file {0}: didn't manage to correct its header.".format(file_complete_path))
            
        
            rewritten_file.write(bytes('\n', 'utf-8'))
            rewritten_file.write(bytes("".join(content), 'utf-8'))        
        
            os.remove(file_complete_path)
            os.rename(rewritten_file.name, file_complete_path)
        
                    
        except Exception as e:
            print(e)
        

if __name__ == "__main__":
    
    # Directory should really be the root of the main project.
    directory = "${HOME}/Codes/MoReFEM/CoreLibrary/Sources"
        
    folder_ignore_list = (os.path.join("ThirdParty", "Source"), "build", )
    module_ignore_list = ("Test", "ModelInstances", )
    
    FileHeader(directory, folder_ignore_list, module_ignore_list)
    