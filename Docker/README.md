# Purpose of the Dockerfile

The provided Dockerfile aims to build images of tag versions of MoReFEM that will be saved in the registry of the [Gitlab project](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM).

A generated image is an extension of an image from [third party compilation project](https://gitlab.inria.fr/MoReFEM/ThirdPartyCompilationFactory) for which the installed MoReFEM has been added. 

As the purpose is to provide a functioning MoReFEM at a minimal storge cost, only the installed libraries and executables are kept; source files are not (but of course you're free to get them back with git on a contained based on the provided image).


# Generation of the images

The Dockerfile in _Docker/Dockerfile_ is used to generate the image with dedicated args.

For instance (when run from the root of the project):

````
DOCKER_BUILDKIT=1 docker build -t registry.gitlab.inria.fr/morefem/corelibrary/morefem/ubuntu_gcc_debug_shared --progress=plain -f Docker/Dockerfile --build-arg os=fedora --build-arg mode=debug --build-arg compiler=gcc --build-arg is_single_library=False --build-arg  library_type=shared .

DOCKER_BUILDKIT=1 docker build -t registry.gitlab.inria.fr/morefem/corelibrary/morefem/fedora_clang_release_shared --progress=plain -f Docker/Dockerfile --build-arg os=fedora --build-arg mode=release --build-arg compiler=clang --build-arg is_single_library=True --build-arg  library_type=shared .

DOCKER_BUILDKIT=1 docker build -t registry.gitlab.inria.fr/morefem/corelibrary/morefem/fedora_clang_debug_shared --progress=plain -f Docker/Dockerfile --build-arg os=fedora --build-arg mode=debug --build-arg compiler=clang --build-arg is_single_library=False --build-arg  library_type=shared .
````

# Putting images on registry

For instance:

````
docker login registry.gitlab.inria.fr
docker push registry.gitlab.inria.fr/morefem/corelibrary/morefem/fedora_clang_debug_shared
````


# Example of using an image


````
docker run -it registry.gitlab.inria.fr/morefem/corelibrary/morefem/fedora_clang_debug_shared
````

And inside the container clone a model and run its tests:

````
git clone https://gitlab.inria.fr/MoReFEM/Models/AcousticWave
cd AcousticWave
mkdir build
cd build
python /opt/MoReFEM/cmake/configure_cmake_external_model.py --morefem_install_dir=/opt/MoReFEM --cmake_args="-G Ninja" 
ninja
ctest
````





