/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 18 Mar 2016 17:07:38 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#include "OperatorInstances/ParameterOperator/Local/UpdateFiberDeformation.hpp"


namespace MoReFEM
{
    
    
    namespace LocalParameterOperatorNS
    {
        
        
        UpdateFiberDeformation
        ::UpdateFiberDeformation(const ExtendedUnknown::const_shared_ptr& a_unknown_storage,
                                 elementary_data_type&& a_elementary_data,
                                 ParameterAtQuadraturePoint<ParameterNS::Type::scalar, ParameterNS::TimeDependencyNS::None>&  fiber_deformation,
                                 const ParameterAtQuadraturePoint<ParameterNS::Type::scalar, ParameterNS::TimeDependencyNS::None>& contraction_rheology_residual,
                                 const ParameterAtQuadraturePoint<ParameterNS::Type::vector, ParameterNS::TimeDependencyNS::None>& schur_complement)
        : parent(a_unknown_storage, std::move(a_elementary_data), fiber_deformation),
        contraction_rheology_residual_(contraction_rheology_residual),
        schur_complement_(schur_complement)
        {
            const auto& elementary_data = GetElementaryData();
            
            increment_local_displacement_.resize(elementary_data.NdofRow());
        }
        
        
        UpdateFiberDeformation::~UpdateFiberDeformation() = default;
        
        
        
        const std::string& UpdateFiberDeformation::ClassName()
        {
            static std::string name("UpdateFiberDeformation");
            return name;
        }
        
        
        void UpdateFiberDeformation::ComputeEltArray()
        {
            auto& elementary_data = GetNonCstElementaryData();
            
            const auto& infos_at_quad_pt_list = elementary_data.GetInformationsAtQuadraturePointList();
            
            const auto& geom_elt = elementary_data.GetCurrentGeomElt();
            
            const auto& increment_local_displacement = GetIncrementLocalDisplacement();
            
            const auto& ref_felt = elementary_data.GetRefFElt(GetExtendedUnknown());
            
            const unsigned int Ncomponent = elementary_data.GetMeshDimension();

            const unsigned int Nnode = ref_felt.Nnode();
            
            for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
            {
                const auto& quad_pt = infos_at_quad_pt.GetQuadraturePoint();
                
                const double residual = GetContractionRheologyResidual().GetValue(quad_pt, geom_elt);
                
                const auto& schur = GetSchurComplement().GetValue(quad_pt, geom_elt);
                
                double result_scalar_product = 0.;
                
                for (unsigned int node_index = 0; node_index < Nnode; ++node_index)
                {
                    unsigned int dof_index = node_index;
                    
                    for (unsigned int component = 0; component < Ncomponent; ++component, dof_index += Nnode)
                    {
                        result_scalar_product += schur(static_cast<int>(dof_index))
                            * increment_local_displacement[static_cast<std::size_t>(dof_index)];
                    }
                }
                
                auto functor = [residual, result_scalar_product](double& fiber_deformation)
                {
                    fiber_deformation += - (residual + result_scalar_product);
                };
                
                GetNonCstParameter().UpdateValue(quad_pt,
                                                 geom_elt,
                                                 functor);
                
            }
            
        }
        
        
    } // namespace LocalParameterOperatorNS
    
    
} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
