/*!
//
// \file
//
//
// Created by Philippe Moireau <philippe.moireau@inria.fr> on the Fri, 23 Feb 2018 10:31:15 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


/*!
 \file LogI3Penalization.h
 \authors S. Gilles
 \date 25/03/2013
 \brief Class in charge of Ciarlet-Geymonat laws
 */

#include <string>

#include "Utilities/Numeric/Numeric.hpp"
#include "Utilities/Containers/EnumClass.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"

#include "Operators/LocalVariationalOperator/CauchyAndInvariant/InvariantHolder.hpp"
#include "OperatorInstances/HyperelasticLaws/LogI3Penalization.hpp"



namespace MoReFEM
{
    
    
    namespace HyperelasticLawNS
    {
        
        
        const std::string& LogI3Penalization::ClassName()
        {
            static std::string ret("Ciarlet-Geymonat");
            return ret;
        }
        
        
        LogI3Penalization::LogI3Penalization(const Solid& solid)
        : bulk_(solid.GetHyperelasticBulk())
        { }
        
        
        double LogI3Penalization::W(const invariant_holder_type& invariant_holder,
                                  const QuadraturePoint& quad_pt,
                                  const GeometricElt& geom_elt) const
        {
            const double I3 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I3);
            const double sqrt_I3 = std::sqrt(I3);
            
            return GetBulk().GetValue(quad_pt, geom_elt) * (sqrt_I3 - 1. - std::log(sqrt_I3));
        }
        
        
        double LogI3Penalization::FirstDerivativeWThirdInvariant(const invariant_holder_type& invariant_holder,
                                                               const QuadraturePoint& quad_pt,
                                                               const GeometricElt& geom_elt) const
        {
            const double I3 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I3);

            return GetBulk().GetValue(quad_pt, geom_elt) * (0.5 * std::pow(I3, -0.5) - 0.5 / I3);
        }
        
        
        double LogI3Penalization::SecondDerivativeWThirdInvariant(const invariant_holder_type& invariant_holder,
                                                                const QuadraturePoint& quad_pt,
                                                                const GeometricElt& geom_elt) const
        {
            const double I3 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I3);
            
            return GetBulk().GetValue(quad_pt, geom_elt) * (-0.25 * std::pow(I3, -1.5) + 0.5 * std::pow(I3, -2.));
        }
        

    } // namespace HyperelasticLawNS
    
    
} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup

