/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 7 Sep 2015 17:20:58 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_LOCAL_x_P1_xTO_x_P1B_HPP_
# define MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_LOCAL_x_P1_xTO_x_P1B_HPP_

# include "Operators/ConformInterpolator/Lagrangian/LocalLagrangianInterpolator.hpp"


namespace MoReFEM
{


    namespace ConformInterpolatorNS
    {


        namespace Local
        {


            /*!
             * \brief Interpolator P1 -> P1b.
             */
            class P1_to_P1b : public LagrangianNS::LocalLagrangianInterpolator
            {
            private:

                //! Alias to parent.
                using parent = LagrangianNS::LocalLagrangianInterpolator;

            public:

                //! \copydoc doxygen_hide_alias_self
                using self = P1_to_P1b;

                //! Alias to unique pointer.
                using unique_ptr = std::unique_ptr<self>;

                //! Alias to vector of unique pointers.
                using vector_unique_ptr = std::vector<unique_ptr>;

                //! Label of the target shape function label.
                static const std::string& GetTargetShapeFunctionLabel();


            public:

                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \param[in] source_felt_space \a FEltSpace with the P1 unknown.
                 * \param[in] target_ref_local_felt_space \a RefLocalFEltSpace for the P1b unknown.
                 * \copydoc doxygen_hide_conform_interpolator_interpolation_data_arg
                 *
                 */
                explicit P1_to_P1b(const FEltSpace& source_felt_space,
                                  const Internal::RefFEltNS::RefLocalFEltSpace& target_ref_local_felt_space,
                                  const Advanced::ConformInterpolatorNS::InterpolationData& interpolation_data);

                //! Destructor.
                virtual ~P1_to_P1b();

                //! \copydoc doxygen_hide_copy_constructor
                P1_to_P1b(const P1_to_P1b& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                P1_to_P1b(P1_to_P1b&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                P1_to_P1b& operator=(const P1_to_P1b& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                P1_to_P1b& operator=(P1_to_P1b&& rhs) = delete;

                ///@}


            };


        } // namespace Local


    } // namespace ConformInterpolatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


# include "OperatorInstances/ConformInterpolator/Local/P1_to_P1b.hxx"


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_LOCAL_x_P1_xTO_x_P1B_HPP_
