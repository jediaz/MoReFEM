/*!
//
// \file
//
//
// Created by Dominique Chapelle <dominique.chapelle@inria.fr> on the Tue, 20 Feb 2018 12:29:43 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/Wrappers/Seldon/SeldonFunctions.hpp"
#include "ThirdParty/Wrappers/Seldon/MatrixOperations.hpp"

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/NonlinearMembrane.hpp"

#include <iostream>


namespace MoReFEM
{
    
    
    namespace Advanced
    {
    
    
    namespace LocalVariationalOperatorNS
    {
        
        
        NonlinearMembrane::NonlinearMembrane(const ExtendedUnknown::vector_const_shared_ptr& a_unknown_storage,
                                             const ExtendedUnknown::vector_const_shared_ptr& a_test_unknown_storage,
                                             elementary_data_type&& a_elementary_data,
                                             const scalar_parameter& youngs_modulus,
                                             const scalar_parameter& poisson_ratio,
                                             const scalar_parameter& thickness)
        : NonlinearLocalVariationalOperator(a_unknown_storage, a_test_unknown_storage, std::move(a_elementary_data)),
        matrix_parent(),
        vector_parent(),
        youngs_modulus_(youngs_modulus),
        poisson_ratio_(poisson_ratio),
        thickness_(thickness)
        {
            const auto& elementary_data = GetElementaryData();
            
            const auto& unknown_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
            const int Nnode = static_cast<int>(unknown_ref_felt.Nnode());
            
          
            const auto& test_unknown_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
            const int Nnode_test = static_cast<int>(test_unknown_ref_felt.Nnode());

            #ifndef NDEBUG
            {
                const unsigned int ref_felt_space_dimension = unknown_ref_felt.GetFEltSpaceDimension();

                const unsigned int euclidean_dimension = unknown_ref_felt.GetMeshDimension();

                assert(ref_felt_space_dimension < euclidean_dimension && "This operator is a surfacic operator.");
                assert((ref_felt_space_dimension + 1) == euclidean_dimension && "This operator is a surfacic operator.");
            }
            #endif // NDEBUG

            former_local_displacement_.resize(elementary_data.NdofCol());
            
            matrix_parent::InitLocalMatrixStorage({
                {
                    { 3, 3 }, // tangent_tensor
                    { 3, 6 }, // De_membrane
                    { 6, 3 }, // transposed_De_membrane
                    { 6, 3 }, // transposed_De_mult_tangent_tensor
                    { 6, 6 }, // tangent_matrix
                    { 3, 2 }, // displacement_gradient
                    { 3, 2 }, // covariant_basis,
                   // { 3, 2 }, // contravariant_basis,
                    { 2, 3 }, // transposed_covariant_basis,
                    { 2, 2 }, // covariant_metric_tensor,
                    { 2, 2 }, // contravariant_metric_tensor,
                    { 2, 2 }, // gradient_based_block
                    { 2, Nnode }, //transposed_dphi
                    { Nnode_test, 2 }, // dphi_test_mult_gradient_based_block
                    { Nnode_test, Nnode }, // block_contribution
                }});
            
            vector_parent::InitLocalVectorStorage
            ({{
                3, // green-lagrange
                3, // second-PK
                6, // rhs_part
            }});
        }
        
                                                                       
        NonlinearMembrane::~NonlinearMembrane() = default;
        
                                                                       
        const std::string& NonlinearMembrane::ClassName()
        {
            static std::string name("NonlinearMembrane");
            return name;
        }
        
        
        void NonlinearMembrane::ComputeEltArray()
        {            
            auto& elementary_data = GetNonCstElementaryData();

            auto& matrix_result = elementary_data.GetNonCstMatrixResult();
            matrix_result.Zero();

            // Vector related calculation.
            auto& vector_result = elementary_data.GetNonCstVectorResult();
            vector_result.Zero(); 
            
            const auto& unknown_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));             
            const auto& test_unknown_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));            
            
            const auto& infos_at_quad_pt_list = elementary_data.GetInformationsAtQuadraturePointList();

            const int Nnode = static_cast<int>(unknown_ref_felt.Nnode());
            
            const int Nnode_test = static_cast<int>(test_unknown_ref_felt.Nnode());
            
            decltype(auto) youngs_modulus = GetYoungsModulus();
            decltype(auto) poisson_ratio = GetPoissonRatio();
            decltype(auto) thickness = GetThickness();            
            
            const auto& geom_elt = elementary_data.GetCurrentGeomElt();
            
            constexpr auto Ncomponent = 3u;
            constexpr auto Nsurface_comp = 2u;
            
            unsigned int quad_pt_index = 0;
            
            for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
            {
                decltype(auto) quad_pt_unknown_list_data = infos_at_quad_pt.GetUnknownData();
                decltype(auto) test_unknown_data = infos_at_quad_pt.GetTestUnknownData();
                
                const auto& dphi = quad_pt_unknown_list_data.GetGradientRefFEltPhi();
                
                double determinant;
                                
                const auto& quad_pt = infos_at_quad_pt.GetQuadraturePoint();
                
                const double youngs_modulus_value = youngs_modulus.GetValue(quad_pt, geom_elt);
                const double poisson_ratio_value =  poisson_ratio.GetValue(quad_pt, geom_elt);
                const double thickness_value = thickness.GetValue(quad_pt, geom_elt);
                
                ComputeContravariantBasis(quad_pt_unknown_list_data, determinant);
                ComputeDisplacementGradient(quad_pt_unknown_list_data);
                ComputeGreenLagrange();
                ComputeTangentTensor(youngs_modulus_value, poisson_ratio_value);
                ComputeSecondPiolaKirchhoff();
                ComputeDe();
                ComputeTangentMatrixAndRightHandSide();

                const auto weight_meas = quad_pt.GetWeight() * std::sqrt(determinant) * thickness_value; // * quad_pt_unknown_list_data.GetAbsoluteValueJacobianDeterminant();

                const auto& grad_felt_phi_test = test_unknown_data.GetGradientRefFEltPhi();
                
                const auto& dphi_test = grad_felt_phi_test;

                assert(dphi.GetM() == Nnode);                
                assert(dphi_test.GetM() == Nnode_test);

                if (parent::DoAssembleIntoMatrix())
                {
                    auto& tangent_matrix = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tangent_matrix)>();
                    
                    auto& gradient_based_block =
                        matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::gradient_based_block)>();
                    auto& transposed_dphi =
                        matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_dphi)>();

                    Wrappers::Seldon::Transpose(dphi, transposed_dphi);            
                    
                    // LocalMatrix dPhi_mult_gradient_based_block(dPhi.GetM(), static_cast<int>(Ncomponent));
                    auto& dphi_test_mult_gradient_based_block =
                        matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::dphi_test_mult_gradient_based_block)>();
                    auto& block_contribution = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_contribution)>();
                    
                    for (unsigned int row_component = 0u; row_component < Ncomponent; ++row_component)
                    {
                        const auto row_first_index =
                            static_cast<int>(test_unknown_ref_felt.GetIndexFirstDofInElementaryData(row_component));
                        
                        for (unsigned int col_component = 0u; col_component < Ncomponent; ++col_component)
                        {
                            const auto col_first_index = static_cast<int>(test_unknown_ref_felt.GetIndexFirstDofInElementaryData(col_component));
                            
                            Advanced::LocalVariationalOperatorNS::ExtractGradientBasedBlock(tangent_matrix,
                                                                                            row_component,
                                                                                            col_component,
                                                                                            gradient_based_block);

                            Seldon::Mlt(1., dphi_test,
                                        gradient_based_block,
                                        dphi_test_mult_gradient_based_block);
                            
                            Seldon::Mlt(weight_meas,
                                        dphi_test_mult_gradient_based_block,
                                        transposed_dphi,
                                        block_contribution);

                            for (int row_node = 0; row_node < Nnode_test; ++row_node)
                            {
                                assert(row_first_index + row_node < matrix_result.GetM());
                                assert(row_node < block_contribution.GetM());

                                for (int col_node = 0u; col_node < Nnode; ++col_node)
                                {
                                    assert(col_first_index + col_node < matrix_result.GetN());
                                    assert(col_node < block_contribution.GetN());

                                    matrix_result(row_first_index + row_node, col_first_index + col_node)
                                    += block_contribution(row_node, col_node);
                                }
                            }
                        }
                    }
                }
                
                const auto int_Nsurface_comp = static_cast<int>(Nsurface_comp);
                
                if (parent::DoAssembleIntoVector())
                {    
                    auto& rhs_part = GetLocalVector<EnumUnderlyingType(LocalVectorIndex::rhs_part)>();
                    
                    for (unsigned int row_component = 0u; row_component < Ncomponent; ++row_component)
                    {
                        const auto dof_first_index =
                            static_cast<int>(test_unknown_ref_felt.GetIndexFirstDofInElementaryData(row_component));
                        const auto component_first_index = static_cast<int>(row_component * Nsurface_comp);
                        
                        // Compute the new contribution to vector_result here.
                        // Product matrix vector is inlined here to avoid creation of an intermediate subset of \a rhs_part.
                        for (int row_node = 0; row_node < Nnode_test; ++row_node)
                        {
                            double value = 0.;

                            assert(row_node < dphi.GetM());
                            assert(dphi.GetN() == int_Nsurface_comp);
                            
                            for (int col = 0; col < int_Nsurface_comp; ++col)
                            {
                                assert(col + component_first_index < rhs_part.GetSize());
                                value += dphi(row_node, col) * rhs_part(col + component_first_index);
                            }

                            assert(dof_first_index + row_node < vector_result.GetSize());
                            
                            vector_result(dof_first_index + row_node) += value * weight_meas;
                        }
                    }
                }
                
                ++quad_pt_index;                
            } // loop over quadrature points  
        }


        void NonlinearMembrane
        ::ComputeContravariantBasis(const Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS::ForUnknownList& quad_pt_unknown_list_data,
                                    double& determinant)
        {
            auto& elementary_data = GetNonCstElementaryData();
            
            auto& covariant_basis = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::covariant_basis)>();
            covariant_basis.Zero();

            const auto& geom_elt = elementary_data.GetCurrentGeomElt();
            const auto& dphi_geo = quad_pt_unknown_list_data.GetGradientRefGeometricPhi();

            constexpr auto Nsurface_comp = 2;
            constexpr auto euclidean_dimension = 3;
            const unsigned int Nshape_function = static_cast<unsigned int>(dphi_geo.GetM());

            for (unsigned int shape_fct_index = 0u; shape_fct_index < Nshape_function; ++shape_fct_index)
            {
                const int int_shape_fct_index = static_cast<int>(shape_fct_index);

                const auto& coords_in_geom_elt = geom_elt.GetCoord(shape_fct_index);

                for (unsigned int component_shape_function = 0u; component_shape_function < Nsurface_comp; ++component_shape_function)
                {
                    const int int_component_shape_function = static_cast<int>(component_shape_function);
                    
                    for (unsigned int coord_index = 0u; coord_index < euclidean_dimension; ++coord_index)
                    {
                        const int int_coord_index = static_cast<int>(coord_index);

                        covariant_basis(int_coord_index, int_component_shape_function)
                        += coords_in_geom_elt[coord_index]
                         * dphi_geo(int_shape_fct_index, int_component_shape_function);
                    }
                }
            }



            auto& covariant_metric_tensor = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::covariant_metric_tensor)>();
            auto& contravariant_metric_tensor = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::contravariant_metric_tensor)>();

            auto& transposed_covariant_basis = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_covariant_basis)>();
            Wrappers::Seldon::Transpose(covariant_basis, transposed_covariant_basis);
            Seldon::Mlt(1., transposed_covariant_basis, covariant_basis, covariant_metric_tensor);

            Wrappers::Seldon::ComputeInverseSquareMatrix(covariant_metric_tensor,
                                                         contravariant_metric_tensor,
                                                         determinant);

            // #1259: Actually never used!!! Only determinant seems relevant.
            // auto& contravariant_basis = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::contravariant_basis)>();
//            Seldon::Mlt(1., covariant_basis, contravariant_metric_tensor, contravariant_basis);
        }
        
        
        void NonlinearMembrane
        ::ComputeDisplacementGradient(const Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS::ForUnknownList& quad_pt_unknown_list_data)
         {
             auto& displacement_gradient = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::displacement_gradient)>();
             displacement_gradient.Zero();

             auto& local_displacement = GetFormerLocalDisplacement();

             const auto& dphi_geo = quad_pt_unknown_list_data.GetGradientRefGeometricPhi();
             
             constexpr auto Nsurface_comp = 2ul;
             constexpr auto euclidean_dimension = 3ul;

             const unsigned int Nshape_function = static_cast<unsigned int>(dphi_geo.GetM());
                 
             for (auto shape_fct_index = 0ul; shape_fct_index < Nshape_function; ++shape_fct_index)
             {
                 const int int_shape_fct_index = static_cast<int>(shape_fct_index);
                 
                 for (unsigned int component_shape_function = 0u; component_shape_function < Nsurface_comp; ++component_shape_function)
                 {
                     const int int_component_shape_function = static_cast<int>(component_shape_function);

                     for (auto coord_index = 0ul; coord_index < euclidean_dimension; ++coord_index)
                     {
                         const int int_coord_index = static_cast<int>(coord_index);

                         const auto local_displacement_index =
                            euclidean_dimension * shape_fct_index + coord_index;
                         assert(local_displacement_index < local_displacement.size());

                         displacement_gradient(int_coord_index, int_component_shape_function)
                         += local_displacement[local_displacement_index]
                          * dphi_geo(int_shape_fct_index, int_component_shape_function);
                     }
                 }
             }
         }
        
        
        void NonlinearMembrane::ComputeGreenLagrange()
        {
            auto& green_lagrange = GetLocalVector<EnumUnderlyingType(LocalVectorIndex::green_lagrange)>();
            const auto& displacement_gradient =
                GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::displacement_gradient)>();
            const auto& covariant_basis = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::covariant_basis)>();

            assert(green_lagrange.GetSize() == 3);
            green_lagrange.Zero();

            // Green-Lagrange strain tensor entries            
            for (int i = 0; i < 3; ++i)
            {
                const auto disp_grad_0 = displacement_gradient(i, 0);
                const auto disp_grad_1 = displacement_gradient(i, 1);
                const auto cov_basis_0 = covariant_basis(i, 0);
                const auto cov_basis_1 = covariant_basis(i, 1);

                green_lagrange(0) += disp_grad_0 *
                                     (cov_basis_0 + 0.5 * disp_grad_0);
                green_lagrange(1) += disp_grad_1 *
                                     (cov_basis_1 + 0.5 * disp_grad_1);
                green_lagrange(2) += disp_grad_0 * cov_basis_1 +
                                     disp_grad_1 * cov_basis_0 +
                                     disp_grad_0 * disp_grad_1;
            }
        }
        
        
        void NonlinearMembrane::ComputeTangentTensor(double E, double nu)
        {
            auto& tangent_tensor = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tangent_tensor)>();
            const auto& contravariant_metric_tensor =
                GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::contravariant_metric_tensor)>();

            assert(!NumericNS::IsZero(nu));

            const double factor = E / (1. - NumericNS::Square(nu));

            tangent_tensor.Zero();

            const auto contravariant_metric_tensor_0_0 = contravariant_metric_tensor(0, 0);
            const auto contravariant_metric_tensor_0_1 = contravariant_metric_tensor(0, 1);
            const auto contravariant_metric_tensor_1_1 = contravariant_metric_tensor(1, 1);

            // Tangent tensor entries (upper triangular)
            tangent_tensor(0, 0) = factor * NumericNS::Square(contravariant_metric_tensor_0_0);
            tangent_tensor(0, 1) = factor
                                * ((1. - nu) * NumericNS::Square(contravariant_metric_tensor_0_1)
                                   + nu * contravariant_metric_tensor_0_0 * contravariant_metric_tensor_1_1);

            tangent_tensor(0, 2) = factor * contravariant_metric_tensor_0_0 * contravariant_metric_tensor_0_1;
            tangent_tensor(1, 1) = factor * NumericNS::Square(contravariant_metric_tensor_1_1);
            tangent_tensor(1, 2) = factor * contravariant_metric_tensor_1_1 * contravariant_metric_tensor_0_1;
            tangent_tensor(2, 2) = 0.5 * factor
                                * ((1.-nu) * contravariant_metric_tensor_0_0 * contravariant_metric_tensor_1_1
                                   + (1. + nu) * NumericNS::Square(contravariant_metric_tensor_0_1));

            // Symmetry
            tangent_tensor(1, 0) = tangent_tensor(0, 1);
            tangent_tensor(2, 0) = tangent_tensor(0, 2);
            tangent_tensor(2, 1) = tangent_tensor(1, 2);
        }
        
        
        void NonlinearMembrane::ComputeSecondPiolaKirchhoff()
        {
            auto& second_PK = GetLocalVector<EnumUnderlyingType(LocalVectorIndex::second_PK)>();
            const auto& green_lagrange = GetLocalVector<EnumUnderlyingType(LocalVectorIndex::green_lagrange)>();
            const auto& tangent_tensor = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tangent_tensor)>();

            Seldon::Mlt(tangent_tensor, green_lagrange, second_PK);
        }
        
        
        void NonlinearMembrane::ComputeDe()
        {
            auto& De_membrane = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::De_membrane)>();
            auto& transposed_De_membrane = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_De_membrane)>();
            const auto& displacement_gradient =
                GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::displacement_gradient)>();
            const auto& covariant_basis =
                GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::covariant_basis)>();
            
            De_membrane.Zero();

            // De/Dgrady matrix entries
            for (int i = 0; i < 3; ++i)
            {
                const auto cov_basis_0 = covariant_basis(i, 0);
                const auto cov_basis_1 = covariant_basis(i, 1);
                const auto disp_grad_0 = displacement_gradient(i, 0);
                const auto disp_grad_1 = displacement_gradient(i, 1);

                De_membrane(0, 2 * i) = cov_basis_0 + disp_grad_0;
                De_membrane(1, 2 * i + 1) = cov_basis_1 + disp_grad_1;
                De_membrane(2, 2 * i) = cov_basis_1 + disp_grad_1;
                De_membrane(2, 2 * i + 1) = cov_basis_0 + disp_grad_0;
            }

            // Transposed
            Wrappers::Seldon::Transpose(De_membrane, transposed_De_membrane);
        }
        
        
        void NonlinearMembrane::ComputeTangentMatrixAndRightHandSide()
        {
            auto& tangent_matrix = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tangent_matrix)>();
            auto& tangent_tensor = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tangent_tensor)>();
            const auto& De_membrane =
                GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::De_membrane)>();
            const auto& transposed_De_membrane =
                GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_De_membrane)>();
            auto& transposed_De_mult_tangent_tensor =
                GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_De_mult_tangent_tensor)>();
            auto& rhs_part = GetLocalVector<EnumUnderlyingType(LocalVectorIndex::rhs_part)>();
            const auto& second_PK = GetLocalVector<EnumUnderlyingType(LocalVectorIndex::second_PK)>();

            if (parent::DoAssembleIntoMatrix())
            {
                transposed_De_mult_tangent_tensor.Zero();
                tangent_matrix.Zero();

                // Tangent: Rigidity part
                Seldon::Mlt(transposed_De_membrane, tangent_tensor, transposed_De_mult_tangent_tensor);
                Seldon::Mlt(transposed_De_mult_tangent_tensor, De_membrane, tangent_matrix);

                // Tangent: Geometric part
                for (int i = 0; i < 3; ++i)
                {
                    const double second_pk_2 = second_PK(2);

                    tangent_matrix(2 * i, 2 * i) += second_PK(0);
                    tangent_matrix(2 * i + 1, 2 * i + 1) += second_PK(1);
                    tangent_matrix(2 * i, 2 * i + 1) += second_pk_2;
                    tangent_matrix(2 * i + 1, 2 * i) += second_pk_2;
                }
            }

            if (parent::DoAssembleIntoVector())
            {
                rhs_part.Zero();
                Seldon::Mlt(transposed_De_membrane, second_PK, rhs_part);
            }
        }
        
        
    } // namespace LocalVariationalOperatorNS
        
        
    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
