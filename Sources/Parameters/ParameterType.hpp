/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 22 May 2015 12:00:53 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_PARAMETER_TYPE_HPP_
# define MOREFEM_x_PARAMETERS_x_PARAMETER_TYPE_HPP_

# include "Utilities/MatrixOrVector.hpp"


namespace MoReFEM
{


    namespace ParameterNS
    {


        /// \addtogroup ParametersGroup
        ///@{


        //! Type of the parameter to build.
        enum class Type
        {
            scalar,
            vector,
            matrix
        };

        /*!
         * \class doxygen_hide_tparam_parameter_type
         *
         * \tparam TypeT Type of the parameter (real, vector, matrix).
         */

        /*!
         * \brief Traits class that yields relevant C++ types to use for each \a TypeT.
         *
         * One alias is currently expected:
         * - return_type, which is the type to be returned by Parameter::GetValue(). Typically it is expected
         * to be a const reference to lvalue for non POD types (vector, matrix) or the type itself for POD types.
         */
        template<Type T>
        struct Traits;


        // ============================
        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        // ============================

        template<>
        struct Traits<Type::scalar> final
        {
            using return_type = double;

            using non_constant_reference = double&;

            static const std::string& TypeName() noexcept;

            static double AllocateDefaultValue(unsigned int, unsigned int ) noexcept;
        };


        template<>
        struct Traits<Type::vector> final
        {
            using return_type = const LocalVector&;

            using non_constant_reference = LocalVector&;

            static const std::string& TypeName() noexcept;

            static return_type AllocateDefaultValue(unsigned int Nrow, unsigned int ) noexcept;
        };


        template<>
        struct Traits<Type::matrix> final
        {
            using return_type = const LocalMatrix&;

            using non_constant_reference = LocalMatrix&;

            static const std::string& TypeName() noexcept;

            static return_type AllocateDefaultValue(unsigned int Nrow, unsigned int Ncol) noexcept;

        };


        // ============================
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN
        // ============================


        ///@} // \addtogroup Parameters


    } // namespace ParameterNS


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


# include "Parameters/ParameterType.hxx"


#endif // MOREFEM_x_PARAMETERS_x_PARAMETER_TYPE_HPP_
