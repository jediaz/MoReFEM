/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 11 May 2016 17:24:37 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#include "Operators/LocalVariationalOperator/CauchyAndInvariant/InvariantComputation.hpp"


namespace MoReFEM
{
    
    
    namespace // anonymous
    {
        
        // Function called once to initialize a static constant below.
        LocalMatrix InitMatrixSecondDerivativeInvariant2_3D();
        
        // Constant matrix obtained for second derivative of invariant 2 in 3D case
        static const LocalMatrix MatrixSecondDerivativeInvariant2_3D = InitMatrixSecondDerivativeInvariant2_3D();
        
        
        // Function called once to initialize a static constant below.
        LocalMatrix InitMatrixSecondDerivativeInvariant23_2D();
        
        // Constant matrix obtained for second derivative of invariant 2 and 3 in 2D case
        static const LocalMatrix MatrixSecondDerivativeInvariant23_2D = InitMatrixSecondDerivativeInvariant23_2D();
        
        // Function called once to initialize a static constant below.
        LocalVector InitVectorFirstDerivativeInvariant1_2D();
        
        static const LocalVector VectorFirstDerivativeInvariant1_2D = InitVectorFirstDerivativeInvariant1_2D();
        
        // Function called once to initialize a static constant below.
        LocalVector InitVectorFirstDerivativeInvariant1_3D();
        
        static const LocalVector VectorFirstDerivativeInvariant1_3D = InitVectorFirstDerivativeInvariant1_3D();
        
        template<int SizeT>
        LocalMatrix InitEmptyMatrix();
        
        static const LocalMatrix EmptyMatrix_1D = InitEmptyMatrix<1>();
        static const LocalMatrix EmptyMatrix_2D = InitEmptyMatrix<3>();
        static const LocalMatrix EmptyMatrix_3D = InitEmptyMatrix<6>();
        
        
    } // namespace anonymous
    
    
    template<>
    void FirstDerivativeInvariant1CauchyGreen<1>(const LocalVector&, LocalVector& out)
    {
        assert(out.GetLength() == 1);
        out(0) = 1.;
    }
    
    template<>
    void FirstDerivativeInvariant1CauchyGreen<2>(const LocalVector&, LocalVector& out)
    {
        assert(out.GetLength() == 3);
        out.Copy(VectorFirstDerivativeInvariant1_2D);
    }
    
    
    template<>
    void FirstDerivativeInvariant1CauchyGreen<3>(const LocalVector&, LocalVector& out)
    {
        assert(out.GetLength() == 6);
        out.Copy(VectorFirstDerivativeInvariant1_3D);
    }
    
    
    template<>
    void FirstDerivativeInvariant2CauchyGreen<1>(const LocalVector& tensor, LocalVector& out)
    {
        assert(out.GetLength() == 1);
        assert(tensor.GetSize() == 1);
        
        static_cast<void>(tensor);
        
        out(0) = 2.;
    }
    
    
    template<>
    void FirstDerivativeInvariant2CauchyGreen<2>(const LocalVector& tensor, LocalVector& out)
    {
        assert(out.GetLength() == 3);
        assert(tensor.GetSize() == 3);
        out(0) = 1. + tensor(1);
        out(1) = 1. + tensor(0);
        out(2) = -tensor(2);
    }
    
    
    template<>
    void FirstDerivativeInvariant2CauchyGreen<3>(const LocalVector& tensor, LocalVector& out)
    {
        assert(out.GetLength() == 6);
        assert(tensor.GetSize() == 6);
        const double xx = tensor(0);
        const double yy = tensor(1);
        const double zz = tensor(2);
        const double xy = tensor(3);
        const double yz = tensor(4);
        const double xz = tensor(5);
        
        out(0) = yy + zz;
        out(1) = xx + zz;
        out(2) = xx + yy;
        out(3) = -xy;
        out(4) = -yz;
        out(5) = -xz;
    }
    
    
    template<>
    void FirstDerivativeInvariant3CauchyGreen<1>(const LocalVector& tensor, LocalVector& out)
    {
        assert(out.GetLength() == 1);
        assert(tensor.GetSize() == 1);
        
        static_cast<void>(tensor);
        
        out(0) = 1.;
    }
    
    
    template<>
    void FirstDerivativeInvariant3CauchyGreen<2>(const LocalVector& tensor, LocalVector& out)
    {
        assert(out.GetLength() == 3);
        assert(tensor.GetSize() == 3);
        out(0) = tensor(1);
        out(1) = tensor(0);
        out(2) = -tensor(2);
    }
    
    
    template<>
    void FirstDerivativeInvariant3CauchyGreen<3>(const LocalVector& tensor, LocalVector& out)
    {
        using NumericNS::Square;
        
        assert(out.GetLength() == 6);
        assert(tensor.GetSize() == 6);
        const double xx = tensor(0);
        const double yy = tensor(1);
        const double zz = tensor(2);
        const double xy = tensor(3);
        const double yz = tensor(4);
        const double xz = tensor(5);
        
        out(0) = yy * zz - Square(yz);
        out(1) = xx * zz - Square(xz);
        out(2) = xx * yy - Square(xy);
        out(3) = yz * xz - zz * xy;
        out(4) = xy * xz - xx * yz;
        out(5) = xy * yz - yy * xz;
    }
    
    
    template<>
    void FirstDerivativeInvariant4CauchyGreen<1>(const LocalVector& tensor,
                                                 const LocalVector& tau_interpolate_at_quad_point,
                                                 LocalVector& out)
    {
        assert(out.GetLength() == 1);
        assert(tensor.GetSize() == 1);
        
        static_cast<void>(tensor);
        static_cast<void>(tau_interpolate_at_quad_point);
        
        out(0) = 1.;
    }
    
    
    template<>
    void FirstDerivativeInvariant4CauchyGreen<2>(const LocalVector& tensor,
                                                 const LocalVector& tau_interpolate_at_quad_point,
                                                 LocalVector& out)
    {
        assert(out.GetLength() == 3);
        assert(tensor.GetSize() == 3);
        
        static_cast<void>(tensor);
        
        const double nx = tau_interpolate_at_quad_point(0);
        const double ny = tau_interpolate_at_quad_point(1);
        
        out.Zero();
        
        const double norm = nx * nx + ny * ny;
        
        if (!(NumericNS::IsZero(norm)))
        {
            const double inv_norm = 1. / norm;
            out(0) = (nx * nx) * inv_norm;
            out(1) = (ny * ny) * inv_norm;
            out(2) = (nx * ny) * inv_norm;
        }
    }
    
    
    template<>
    void FirstDerivativeInvariant4CauchyGreen<3>(const LocalVector& tensor,
                                                 const LocalVector& tau_interpolate_at_quad_point,
                                                 LocalVector& out)
    {
        using NumericNS::Square;
        
        assert(out.GetLength() == 6);
        assert(tensor.GetSize() == 6);
        
        static_cast<void>(tensor);
        
        const double nx = tau_interpolate_at_quad_point(0);
        const double ny = tau_interpolate_at_quad_point(1);
        const double nz = tau_interpolate_at_quad_point(2);
        
        out.Zero();
        
        const double norm = nx * nx + ny * ny + nz * nz;
        
        if (!(NumericNS::IsZero(norm)))
        {
            const double inv_norm = 1. / norm;
            out(0) = (nx * nx) * inv_norm;
            out(1) = (ny * ny) * inv_norm;
            out(2) = (nz * nz) * inv_norm;
            out(3) = (nx * ny) * inv_norm;
            out(4) = (ny * nz) * inv_norm;
            out(5) = (nx * nz) * inv_norm;
        }
    }
    
    
    template<>
    void FirstDerivativeInvariant6CauchyGreen<1>(const LocalVector& tensor,
                                                 const LocalVector& tau_interpolate_at_quad_point,
                                                 LocalVector& out)
    {
        assert(out.GetLength() == 1);
        assert(tensor.GetSize() == 1);
        
        static_cast<void>(tensor);
        static_cast<void>(tau_interpolate_at_quad_point);
        
        out(0) = 1.;
    }
    
    
    template<>
    void FirstDerivativeInvariant6CauchyGreen<2>(const LocalVector& tensor,
                                                 const LocalVector& tau_interpolate_at_quad_point,
                                                 LocalVector& out)
    {
        assert(out.GetLength() == 3);
        assert(tensor.GetSize() == 3);
        
        static_cast<void>(tensor);
        
        const double nx = tau_interpolate_at_quad_point(0);
        const double ny = tau_interpolate_at_quad_point(1);
        
        out.Zero();
        
        const double norm = nx * nx + ny * ny;
        
        if (!(NumericNS::IsZero(norm)))
        {
            const double inv_norm = 1. / norm;
            out(0) = (nx * nx) * inv_norm;
            out(1) = (ny * ny) * inv_norm;
            out(2) = (nx * ny) * inv_norm;
        }
    }
    
    
    template<>
    void FirstDerivativeInvariant6CauchyGreen<3>(const LocalVector& tensor,
                                                 const LocalVector& tau_interpolate_at_quad_point,
                                                 LocalVector& out)
    {
        using NumericNS::Square;
        
        assert(out.GetLength() == 6);
        assert(tensor.GetSize() == 6);
        
        static_cast<void>(tensor);
        
        const double nx = tau_interpolate_at_quad_point(0);
        const double ny = tau_interpolate_at_quad_point(1);
        const double nz = tau_interpolate_at_quad_point(2);
        
        out.Zero();
        
        const double norm = nx * nx + ny * ny + nz * nz;
        
        if (!(NumericNS::IsZero(norm)))
        {
            const double inv_norm = 1. / norm;
            out(0) = (nx * nx) * inv_norm;
            out(1) = (ny * ny) * inv_norm;
            out(2) = (nz * nz) * inv_norm;
            out(3) = (nx * ny) * inv_norm;
            out(4) = (ny * nz) * inv_norm;
            out(5) = (nx * nz) * inv_norm;
        }
    }
    
    
    template<>
    void SecondDerivativeInvariant1CauchyGreen<1>(const LocalVector&, LocalMatrix& out)
    {
        assert(out.GetM() == 1);
        assert(out.GetN() == 1);
        out.Copy(EmptyMatrix_1D);
    }
    
    template<>
    void SecondDerivativeInvariant1CauchyGreen<2>(const LocalVector&, LocalMatrix& out)
    {
        assert(out.GetM() == 3);
        assert(out.GetN() == 3);
        out.Copy(EmptyMatrix_2D);
    }
    
    
    template<>
    void SecondDerivativeInvariant1CauchyGreen<3>(const LocalVector&, LocalMatrix& out)
    {
        assert(out.GetM() == 6);
        assert(out.GetN() == 6);
        out.Copy(EmptyMatrix_3D);
    }
    
    
    template<>
    void SecondDerivativeInvariant2CauchyGreen<1>(const LocalVector&, LocalMatrix& out)
    {
        assert(out.GetM() == 1);
        assert(out.GetN() == 1);
        out.Copy(EmptyMatrix_1D);
    }
    
    
    template<>
    void SecondDerivativeInvariant2CauchyGreen<2>(const LocalVector&, LocalMatrix& out)
    {
        assert(out.GetM() == 3);
        assert(out.GetN() == 3);
        out.Copy(MatrixSecondDerivativeInvariant23_2D);
    }
    
    
    template<>
    void SecondDerivativeInvariant2CauchyGreen<3>(const LocalVector&, LocalMatrix& out)
    {
        assert(out.GetM() == 6);
        assert(out.GetN() == 6);
        out.Copy(MatrixSecondDerivativeInvariant2_3D);
    }
    
    
    template<>
    void SecondDerivativeInvariant3CauchyGreen<1>(const LocalVector&, LocalMatrix& out)
    {
        assert(out.GetM() == 1);
        assert(out.GetN() == 1);
        out.Copy(EmptyMatrix_1D);
    }
    
    
    template<>
    void SecondDerivativeInvariant3CauchyGreen<2>(const LocalVector&, LocalMatrix& out)
    {
        assert(out.GetM() == 3);
        assert(out.GetN() == 3);
        out.Copy(MatrixSecondDerivativeInvariant23_2D);
    }
    
    
    template<>
    void SecondDerivativeInvariant3CauchyGreen<3>(const LocalVector& tensor, LocalMatrix& out)
    {
        // [0    , Czz  , Cyy  , 0        , -Cyz     , 0        ],
        // [Czz  , 0    , Cxx  , 0        , 0        , -Cxz     ],
        // [Cyy  , Cxx  , 0    , -Cxy     , 0        , 0        ],
        // [0    , 0    , -Cxy , -0.5*Czz , 0.5*Cxz  , 0.5*Cyz  ],
        // [-Cyz , 0    , 0    , 0.5*Cxz  , -0.5*Cxx , 0.5*Cxy  ],
        // [0    , -Cxz , 0    , 0.5*Cyz  , 0.5*Cxy  , -0.5*Cyy ]
        
        assert(out.GetM() == 6);
        assert(out.GetN() == 6);
        assert(tensor.GetSize() == 6);
        
        const double xx = tensor(0);
        const double yy = tensor(1);
        const double zz = tensor(2);
        const double xy = tensor(3);
        const double yz = tensor(4);
        const double xz = tensor(5);
        
        out.Zero();
        
        out(3, 3) = -0.5 * zz;
        out(4, 4) = -0.5 * xx;
        out(5, 5) = -0.5 * yy;
        
        out(1, 0) = out(0, 1) = zz;
        out(2, 0) = out(0, 2) = yy;
        out(1, 2) = out(2, 1) = xx;
        
        out(0, 4) = out(4, 0) = -yz;
        out(1, 5) = out(5, 1) = -xz;
        out(2, 3) = out(3, 2) = -xy;
        
        out(3, 4) = out(4, 3) = 0.5 * xz;
        out(3, 5) = out(5, 3) = 0.5 * yz;
        out(5, 4) = out(4, 5) = 0.5 * xy;
    }
    
    
    template<>
    double Invariant1<1>(const LocalVector& tensor) noexcept
    {
        assert(tensor.GetSize() == 1);
        return tensor(0) + 2.;
    }
    
    
    template<>
    double Invariant1<2>(const LocalVector& tensor) noexcept
    {
        assert(tensor.GetSize() == 3);
        return tensor(0) + tensor(1) + 1.;
    }
    
    
    template<>
    double Invariant1<3>(const LocalVector& tensor) noexcept
    {
        assert(tensor.GetSize() == 6);
        return tensor(0) + tensor(1) + tensor(2);
    }
    
    
    template<>
    double Invariant2<1>(const LocalVector& tensor) noexcept
    {
        assert(tensor.GetSize() == 1);
        const double xx = tensor(0);
        
        return 2. * xx + 1.;
    }
    
    
    template<>
    double Invariant2<2>(const LocalVector& tensor) noexcept
    {
        assert(tensor.GetSize() == 3);
        const double xx = tensor(0);
        const double yy = tensor(1);
        const double xy = tensor(2);
        
        return xx * yy + xx + yy - NumericNS::Square(xy);
    }
    
    
    template<>
    double Invariant2<3>(const LocalVector& tensor) noexcept
    {
        assert(tensor.GetSize() == 6);
        using NumericNS::Square;

        const double xx = tensor(0);
        const double yy = tensor(1);
        const double zz = tensor(2);
        const double xy = tensor(3);
        const double yz = tensor(4);
        const double xz = tensor(5);
        
        return xx * yy + yy * zz + xx * zz - Square(xy) - Square(xz) - Square(yz);
    }
    
    
    template<>
    double Invariant3<1>(const LocalVector& tensor) noexcept
    {
        assert(tensor.GetSize() == 1);
        return tensor(0);
    }
    
    
    template<>
    double Invariant3<2>(const LocalVector& tensor) noexcept
    {
        assert(tensor.GetSize() == 3);
        return tensor(0) * tensor(1) - NumericNS::Square(tensor(2));
    }
    
    
    template<>
    double Invariant3<3>(const LocalVector& tensor) noexcept
    {
        using NumericNS::Square;
        assert(tensor.GetSize() == 6);
        
        const double xx = tensor(0);
        const double yy = tensor(1);
        const double zz = tensor(2);
        const double xy = tensor(3);
        const double yz = tensor(4);
        const double xz = tensor(5);
        
        return xx * yy * zz - xx * Square(yz) - yy * Square(xz) - zz * Square(xy) + 2. * xy * yz * xz;
    }
    
    
    template<>
    double Invariant4<1>(const LocalVector& tensor,
                         const LocalVector& tau_interpolate_at_quad_point) noexcept
    {
        assert(tensor.GetSize() == 1);
        
        const double xx = tensor(0);
        
        const double nx = tau_interpolate_at_quad_point(0);
        
        const double norm = nx * nx;
        
        double I4 = 0.;
        
        if (!(NumericNS::IsZero(norm)))
        {
            I4 = nx * xx * nx;
            I4 /= norm;
        }
        
        return I4;
    }
   
    
    template<>
    double Invariant4<2>(const LocalVector& tensor,
                         const LocalVector& tau_interpolate_at_quad_point) noexcept
    {
        assert(tensor.GetSize() == 3);
    
        const double xx = tensor(0);
        const double yy = tensor(1);
        const double xy = tensor(2);
        
        const double nx = tau_interpolate_at_quad_point(0);
        const double ny = tau_interpolate_at_quad_point(1);
        
        const double norm = nx * nx + ny * ny;
        
        double I4 = 0.;
        
        if (!(NumericNS::IsZero(norm)))
        {
            I4 = nx * (nx * xx + ny * xy) + ny * (nx * xy + ny * yy);
            I4 /= norm;
        }
        
        return I4;
    }
    
    
    template<>
    double Invariant4<3>(const LocalVector& tensor,
                         const LocalVector& tau_interpolate_at_quad_point) noexcept
    {
        assert(tensor.GetSize() == 6);
        
        const double xx = tensor(0);
        const double yy = tensor(1);
        const double zz = tensor(2);
        const double xy = tensor(3);
        const double yz = tensor(4);
        const double xz = tensor(5);
        
        const double nx = tau_interpolate_at_quad_point(0);
        const double ny = tau_interpolate_at_quad_point(1);
        const double nz = tau_interpolate_at_quad_point(2);
        
        const double norm = nx * nx + ny * ny + nz * nz;
        
        double I4 = 0.;
        
        if (!(NumericNS::IsZero(norm)))
        {
            I4 = nx * (nx * xx + ny * xy + nz * xz) + ny * (nx * xy + ny * yy + nz * yz) + nz * (nx * xz + ny * yz + nz * zz);
            I4 /= norm;
        }
        
        return I4;
    }
    
    
    template<>
    double Invariant6<1>(const LocalVector& tensor,
                         const LocalVector& tau_interpolate_at_quad_point) noexcept
    {
        assert(tensor.GetSize() == 1);
        
        const double xx = tensor(0);
        
        const double nx = tau_interpolate_at_quad_point(0);
        
        const double norm = nx * nx;
        
        double I6 = 0.;
        
        if (!(NumericNS::IsZero(norm)))
        {
            I6 = nx * xx * nx;
            I6 /= norm;
        }
        
        return I6;
    }
    
    
    template<>
    double Invariant6<2>(const LocalVector& tensor,
                         const LocalVector& tau_interpolate_at_quad_point) noexcept
    {
        assert(tensor.GetSize() == 3);
        
        const double xx = tensor(0);
        const double yy = tensor(1);
        const double xy = tensor(2);
        
        const double nx = tau_interpolate_at_quad_point(0);
        const double ny = tau_interpolate_at_quad_point(1);
        
        const double norm = nx * nx + ny * ny;
        
        double I6 = 0.;
        
        if (!(NumericNS::IsZero(norm)))
        {
            I6 = nx * (nx * xx + ny * xy) + ny * (nx * xy + ny * yy);
            I6 /= norm;
        }
        
        return I6;
    }
    
    
    template<>
    double Invariant6<3>(const LocalVector& tensor,
                         const LocalVector& tau_interpolate_at_quad_point) noexcept
    {
        assert(tensor.GetSize() == 6);
        
        const double xx = tensor(0);
        const double yy = tensor(1);
        const double zz = tensor(2);
        const double xy = tensor(3);
        const double yz = tensor(4);
        const double xz = tensor(5);
        
        const double nx = tau_interpolate_at_quad_point(0);
        const double ny = tau_interpolate_at_quad_point(1);
        const double nz = tau_interpolate_at_quad_point(2);
        
        const double norm = nx * nx + ny * ny + nz * nz;
        
        double I6 = 0.;
        
        if (!(NumericNS::IsZero(norm)))
        {
            I6 = nx * (nx * xx + ny * xy + nz * xz) + ny * (nx * xy + ny * yy + nz * yz) + nz * (nx * xz + ny * yz + nz * zz);
            I6 /= norm;
        }
        
        return I6;
    }
    
    
    namespace // anonymous
    {
        
        
        // Function called once to initialize a static constant below.
        LocalMatrix InitMatrixSecondDerivativeInvariant2_3D()
        {
            LocalMatrix ret(6, 6);
            ret.Zero();
            
            // [0, 1, 1, 0   , 0   , 0   ],
            // [1, 0, 1, 0   , 0   , 0   ],
            // [1, 1, 0, 0   , 0   , 0   ],
            // [0, 0, 0, -0.5, 0   , 0   ],
            // [0, 0, 0, 0   , -0.5, 0   ],
            // [0, 0, 0, 0   , 0   , -0.5]
            
            ret(3, 3) = ret(4, 4) = ret(5, 5) = -.5;
            
            for (int i = 0; i < 3; ++i)
                for (int j = i + 1; j < 3; ++j)
                    ret(i, j) = ret(j, i) = 1.;
            
            return ret;
        };
        
        
        // Function called once to initialize a static constant below.
        LocalMatrix InitMatrixSecondDerivativeInvariant23_2D()
        {
            LocalMatrix ret(3, 3);
            ret.Zero();
            
            // [0, 1, 0]
            // [1, 0, 0]
            // [0, 0, -0.5]
            
            ret(2, 2) = -0.5;
            ret(0, 1) = ret(1, 0) = 1.;
            
            return ret;
        };
        
        
        // Function called once to initialize a static constant below.
        LocalVector InitVectorFirstDerivativeInvariant1_2D()
        {
            LocalVector ret(3);
            ret(0) = 1.;
            ret(1) = 1.;
            ret(2) = 0.;
            return ret;
        };
        
        
        // Function called once to initialize a static constant below.
        LocalVector InitVectorFirstDerivativeInvariant1_3D()
        {
            LocalVector ret(6);
            ret.Zero();
            ret(0) = 1.;
            ret(1) = 1.;
            ret(2) = 1.;
            return ret;
        }
        
        
        template<int SizeT>
        LocalMatrix InitEmptyMatrix()
        {
            LocalMatrix ret(SizeT, SizeT);
            ret.Zero();
            return ret;
        };
        
        
        
    } // namespace anonymous
  

} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup
