/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 15 Sep 2016 10:41:02 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_EXTRACT_BLOCK_FROM_LINEAR_ALGEBRA_x_EXTRACT_BLOCK_FROM_GLOBAL_MATRIX_HXX_
# define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_EXTRACT_BLOCK_FROM_LINEAR_ALGEBRA_x_EXTRACT_BLOCK_FROM_GLOBAL_MATRIX_HXX_


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            # ifndef NDEBUG


            template<class MatrixTypeT>
            ::Seldon::SubMatrix<MatrixTypeT>
            ExtractBlockFromLocalMatrix(const Advanced::RefFEltInLocalOperator& ref_felt1, const unsigned int component1,
                                        const Advanced::RefFEltInLocalOperator& ref_felt2, const unsigned int component2,
                                        MatrixTypeT& matrix)
            {
                const auto& row_range = ref_felt1.GetLocalDofIndexList(component1);
                const auto& col_range = ref_felt2.GetLocalDofIndexList(component2);

                Wrappers::Seldon::AssertBlockValidity(matrix, row_range, col_range);
                ::Seldon::SubMatrix<MatrixTypeT> ret(matrix, row_range, col_range);

                return ret;
            }


            template<class MatrixTypeT>
            ::Seldon::SubMatrix<MatrixTypeT>
            ExtractBlockFromLocalMatrix(const Advanced::RefFEltInLocalOperator& ref_felt1,
                                        const Advanced::RefFEltInLocalOperator& ref_felt2,
                                        MatrixTypeT& matrix)
            {
                const auto& row_range = ref_felt1.GetLocalDofIndexList();
                const auto& col_range = ref_felt2.GetLocalDofIndexList();

                Wrappers::Seldon::AssertBlockValidity(matrix, row_range, col_range);
                ::Seldon::SubMatrix<MatrixTypeT> ret(matrix, row_range, col_range);

                return ret;
            }


            template<class MatrixTypeT>
            ::Seldon::SubMatrix<MatrixTypeT>
            ExtractBlockFromLocalMatrix(const Advanced::RefFEltInLocalOperator& ref_felt,
                                        const unsigned int component,
                                        MatrixTypeT& matrix)
            {
                const auto& row_and_col_range = ref_felt.GetLocalDofIndexList(component);

                Wrappers::Seldon::AssertBlockValidity(matrix, row_and_col_range, row_and_col_range);
                ::Seldon::SubMatrix<MatrixTypeT>  ret(matrix, row_and_col_range, row_and_col_range);

                return ret;
            }


            # endif // NDEBUG


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_EXTRACT_BLOCK_FROM_LINEAR_ALGEBRA_x_EXTRACT_BLOCK_FROM_GLOBAL_MATRIX_HXX_
