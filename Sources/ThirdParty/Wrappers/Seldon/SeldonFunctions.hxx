/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 9 Sep 2013 09:55:11 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_SELDON_x_SELDON_FUNCTIONS_HXX_
# define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_SELDON_x_SELDON_FUNCTIONS_HXX_


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Seldon
        {


            template<class T1, class Allocator1, class Allocator2>
            ::Seldon::Matrix<T1, ::Seldon::General, ::Seldon::RowMajor, ::Seldon::SELDON_DEFAULT_ALLOCATOR<T1> >
            OuterProd(const ::Seldon::Vector<T1, ::Seldon::VectFull, Allocator1>& X,
                      const ::Seldon::Vector<T1, ::Seldon::VectFull, Allocator2>& Y)
            {
                const int size_x = X.GetSize();
                const int size_y = Y.GetSize();

                ::Seldon::Matrix<T1, ::Seldon::General, ::Seldon::RowMajor, ::Seldon::SELDON_DEFAULT_ALLOCATOR<T1> >
                ret(size_x, size_y);

                OuterProd(X, Y, ret);

                return ret;
            }


            template<class T1, class Allocator1, class Allocator2>
            void
            OuterProd(const ::Seldon::Vector<T1, ::Seldon::VectFull, Allocator1>& X,
                      const ::Seldon::Vector<T1, ::Seldon::VectFull, Allocator2>& Y,
                      ::Seldon::Matrix<T1, ::Seldon::General, ::Seldon::RowMajor, ::Seldon::SELDON_DEFAULT_ALLOCATOR<T1> >& out)
            {
                assert(out.GetM() == X.GetSize());
                assert(out.GetN() == Y.GetSize());

                const int size_x = X.GetSize();
                const int size_y = Y.GetSize();

                for (int i = 0; i < size_x; ++i)
                {
                    for (int j = 0; j < size_y; ++j)
                        out(i, j) = X(i) * Y(j);
                }
            }



            template<class T1, class Allocator1, class Allocator2>
            ::Seldon::Vector<T1, ::Seldon::VectFull, ::Seldon::SELDON_DEFAULT_ALLOCATOR<T1> >
            CrossProduct(const ::Seldon::Vector<T1, ::Seldon::VectFull, Allocator1>& X,
                         const ::Seldon::Vector<T1, ::Seldon::VectFull, Allocator2>& Y)
            {
                const int size = X.GetSize();
                assert(size == 3);
                assert(Y.GetSize() == 3);

                ::Seldon::Vector<T1, ::Seldon::VectFull, ::Seldon::SELDON_DEFAULT_ALLOCATOR<T1> > ret(size);

                CrossProduct(X, Y, ret);

                return ret;
            }


            template<class T1, class Allocator1, class Allocator2, class Allocator3>
            void
            CrossProduct(const ::Seldon::Vector<T1, ::Seldon::VectFull, Allocator1>& X,
                         const ::Seldon::Vector<T1, ::Seldon::VectFull, Allocator2>& Y,
                         ::Seldon::Vector<T1, ::Seldon::VectFull, Allocator3>& out)
            {
                const int size = X.GetSize();
                assert(size == 3);
                assert(Y.GetSize() == 3);
                assert(out.GetSize() == 3);

                ::Seldon::Vector<T1, ::Seldon::VectFull, ::Seldon::SELDON_DEFAULT_ALLOCATOR<T1> > ret(size);

                out(0) = X(1) * Y(2) - X(2) * Y(1);
                out(1) = X(2) * Y(0) - X(0) * Y(2);
                out(2) = X(0) * Y(1) - X(1) * Y(0);
            }


            template <class T0, class Prop0, class Storage0, class Allocator0,
            class T1, class Prop1, class Storage1, class Allocator1>
            void SubstituteLU(const ::Seldon::Matrix<T0, Prop0, Storage0, Allocator0>& system_matrix,
                              ::Seldon::Matrix<T1, Prop1, Storage1, Allocator1>& matrix_to_substitute,
                              double& determinant)
            {
                using namespace ::Seldon;

                const int Ncol = matrix_to_substitute.GetN();
                const int dimension_system_matrix = system_matrix.GetM();

                #ifdef SELDON_CHECK_BOUNDS
                if (system_matrix.GetN() != dimension_system_matrix)
                {
                    std::ostringstream oconv;
                    oconv << "The system matrix should be squared, but is not! (" << dimension_system_matrix
                        << " x " << system_matrix.GetN() << ')';
                    throw ::Seldon::WrongDim("Utilities::::SeldonHH::SubstituteLU",
                                           oconv.str());
                }
                #endif

                Vector<T1> current_col;

                auto lu_matrix(system_matrix);
                determinant = 1.;

                // Perform the LU factorization of the given matrix. Convention is that diagonal terms
                // are those of the Lower matrix.
                # ifdef SELDON_WITH_LAPACK
                Vector<int> permutation;
                GetLU(lu_matrix, permutation);

                for (int i = 0, size = permutation.GetSize(); i < size; ++i)
                {
                    if (i + 1 != permutation(i))
                        determinant *= -1.;
                }
                # else // SELDON_WITH_LAPACK
                GetLU(lu_matrix);
                # endif // SELDON_WITH_LAPACK


                for (int i = 0; i < dimension_system_matrix; ++i)
                {
                    determinant *= lu_matrix(i, i);
                    assert(std::fabs(lu_matrix(i, i)) > 1.e-12 && "If not, inf and nan will appear!");
                }


                // For each column of \a matrix_to_substitute, solve the LU system.
                for (int col_index = 0; col_index < Ncol; ++col_index)
                {
                    GetCol(matrix_to_substitute, col_index, current_col);

                    # ifdef SELDON_WITH_LAPACK
                    SolveLU(lu_matrix, permutation, current_col);
                    # else // SELDON_WITH_LAPACK
                    SolveLU(lu_matrix, current_col);
                    # endif // SELDON_WITH_LAPACK


                    SetCol(current_col, col_index, matrix_to_substitute);
                }
            }



            template <class T, class Prop, class Storage, class Allocator>
            ::Seldon::Matrix<T, Prop, Storage, Allocator>
            Transpose(const ::Seldon::Matrix<T, Prop, Storage, Allocator>& original)
            {
                auto copy(original);
                ::Seldon::Transpose(copy);
                return copy;
            }


            template <class T, class Prop, class Storage, class Allocator>
            ::Seldon::Matrix<T, Prop, Storage, Allocator>
            Transpose(const ::Seldon::SubMatrix<::Seldon::Matrix<T, Prop, Storage, Allocator>>& original)
            {
                int Nrow = original.GetM();
                int Ncol = original.GetN();

                ::Seldon::Matrix<T, Prop, Storage, Allocator> ret(Ncol, Nrow); // the order is on purpose!

                for (int i = 0; i < Nrow; ++i)
                {
                    for (int j = 0; j < Ncol; ++j)
                    {
                        ret(j, i) = original(i, j); // copy and transposition are done in the same time.
                    }
                }

                return ret;
            }


            template <class SeldonMatrixT>
            void Transpose(const SeldonMatrixT& original, LocalMatrix& transposed)
            {
                const auto original_Nrow = original.GetM();
                const auto original_Ncol = original.GetN();

                assert(original.GetM() == transposed.GetN());
                assert(original.GetN() == transposed.GetM());

                for (int i = 0; i < original_Nrow; ++i)
                {
                    for (int j = 0; j < original_Ncol; ++j)
                        transposed(j, i) = original(i, j);
                }
            }



            template<class SeldonMatrixT>
            bool IsZeroMatrix(const SeldonMatrixT& matrix)
            {
                const auto Nrow = matrix.GetM();
                const auto Ncol = matrix.GetN();

                for (auto i = 0; i < Nrow; ++i)
                {
                    for (auto j = 0; j < Ncol; ++j)
                    {
                        if (!NumericNS::IsZero(matrix(i, j)))
                            return false;
                    }
                }

                return true;
            }


            template<class SeldonVectorT>
            bool IsZeroVector(const SeldonVectorT& vector)
            {
                const auto Nitem = vector.GetM();

                for (auto i = 0; i < Nitem; ++i)
                {
                    if (!NumericNS::IsZero(vector(i)))
                        return false;
                }

                return true;
            }



            template<class SeldonMatrixT>
            void CopyMatrixContent(const SeldonMatrixT& source, SeldonMatrixT& target)
            {
                assert(target.GetM() == source.GetM());
                assert(target.GetN() == source.GetN());

                const auto Nrow = source.GetM();
                const auto Ncol = source.GetN();

                for (int i = 0; i < Nrow; ++i)
                {
                    for (int j = 0; j < Ncol; ++j)
                        target(i, j) = source(i, j);
                }
            }



            template<class SeldonVectorT>
            void CopyVectorContent(const SeldonVectorT& source, SeldonVectorT& target)
            {
                assert(target.GetM() == source.GetM());

                const auto Nrow = source.GetM();

                for (int i = 0; i < Nrow; ++i)
                    target(i) = source(i);
            }


            template<class SeldonMatrixT>
            void ScaleMatrix(double factor, const SeldonMatrixT& source, SeldonMatrixT& target)
            {
                assert(target.GetM() == source.GetM());
                assert(target.GetN() == source.GetN());

                const auto Nrow = source.GetM();
                const auto Ncol = source.GetN();

                for (int i = 0; i < Nrow; ++i)
                {
                    for (int j = 0; j < Ncol; ++j)
                        target(i, j) = factor * source(i, j);
                }

            }


            template<class SeldonVectorT>
            void ScaleVector(double factor, const SeldonVectorT& source, SeldonVectorT& target)
            {
                assert(target.GetM() == source.GetM());

                const auto Nrow = source.GetM();

                for (int i = 0; i < Nrow; ++i)
                    target(i) = factor * source(i);
            }


        } // namespace Seldon


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_SELDON_x_SELDON_FUNCTIONS_HXX_
