### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/MatrixOperations.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/SeldonFunctions.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/MatrixOperations.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/MatrixOperations.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/SeldonFunctions.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/SeldonFunctions.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/SubVector.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/SubVector.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/SubVector_Base.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/SubVector_Base.hxx"
)

