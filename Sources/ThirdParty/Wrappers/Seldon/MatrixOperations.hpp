/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 1 Dec 2014 17:14:34 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_SELDON_x_MATRIX_OPERATIONS_HPP_
# define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_SELDON_x_MATRIX_OPERATIONS_HPP_

# include "Utilities/MatrixOrVector.hpp"


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Seldon
        {


            /*!
             * \brief Compute the determinant of \a local_matrix.
             *
             * Works only for dimensions 1 to 3.
             *
             * \param[in] local_matrix Matrix for which determinant is computed.
             *
             * \return Determinant of the matrix.
             */
            double ComputeDeterminant(const LocalMatrix& local_matrix);


            /*!
             * \brief Compute the inverse matrix of \a local_matrix (if existing; if not an exception is throw).
             *
             * Works only for dimensions 1 to 3.
             * \param[in] local_matrix Matrix for which we want to compute the determinant.
             * \param[out] determinant Determinant of \a local_matrix.
             *
             * Use the overload whenever possible: it prevents memory allocation of the return matrix.
             *
             * \return Inverse matrix.
             */
            LocalMatrix ComputeInverseSquareMatrix(const LocalMatrix& local_matrix, double& determinant);



            /*!
             * \brief Compute the inverse matrix of \a local_matrix (if existing; if not an exception is throw).
             *
             * Works only for dimensions 1 to 3.
             * \param[out] inverse Inverse of the matrix.
             * \param[in] local_matrix Matrix for which we want to compute the determinant.
             * \param[out] determinant Determinant of \a local_matrix.
             */
            void ComputeInverseSquareMatrix(const LocalMatrix& local_matrix, LocalMatrix& inverse, double& determinant);


            #ifndef NDEBUG

            /*!
             * \brief Asserts a block may really be extracted from a Seldon matrix.
             *
             * \param[in] matrix Matrix from which a block is to be extracted.
             * \param[in] row_indexes List of indexes of the row to extract. Present debug function checks each of them
             * is legit.
             * \param[in] col_indexes List of indexes of the columns to extract. Present debug function checks each of them
             * is legit.
             */
            template<class MatrixTypeT>
            void AssertBlockValidity(const MatrixTypeT& matrix,
                                     const ::Seldon::Vector<int>& row_indexes,
                                     const ::Seldon::Vector<int>& col_indexes);

            /*!
             * \brief Asserts a block may really be extracted from a Seldon matrix.
             *
             * \param[in] matrix Matrix from which a block is to be extracted.
             * \param[in] row_indexes List of indexes of the row to extract. Present debug function checks each of them
             * is legit.
             */
            template<class MatrixTypeT>
            void AssertBlockRowValidity(const MatrixTypeT& matrix,
                                        const ::Seldon::Vector<int>& row_indexes);


            /*!
             * \brief Asserts a block may really be extracted from a Seldon vector.
             *
             * \param[in] vector vector from which a block is to be extracted.
             * \param[in] row_indexes List of indexes of the row to extract. Present debug function checks each of them
             * is legit.
             */
            template<class VectorTypeT>
            void AssertBlockValidity(const VectorTypeT& vector,
                                     const ::Seldon::Vector<int>& row_indexes);

            #endif // NDEBUG


            /*!
             * \brief Check whether a matrix is filled only with zero.
             *
             * \param[in] matrix Matrix under scrutiny.
             */
            bool IsZeroMatrix(const LocalMatrix& matrix);


            /*!
             * \brief Check whether there is a nan or inf value in a Seldon matrix.
             *
             * \tparam MatrixT A dense Seldon matrix.
             *
             * \param[in] matrix Matrix being scrutinized.
             *
             * \return True if at least one value if NaN or inf.
             */
            template<class MatrixT>
            std::enable_if_t<Utilities::IsSpecializationOf<::Seldon::Matrix, MatrixT>::value, bool>
            AreNanOrInfValues(const MatrixT& matrix);


            /*!
             * \brief Check whether there is a nan or inf value in a Seldon vector.
             *
             * \tparam VectorT A dense Seldon vector.
             *
             * \param[in] vector Vector being scrutinized.
             *
             * \return True if at least one value if NaN or inf.
             */
            template<class VectorT>
            std::enable_if_t<Utilities::IsSpecializationOf<::Seldon::Vector, VectorT>::value, bool>
            AreNanOrInfValues(const VectorT& vector);


        } // namespace Seldon


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


# include "ThirdParty/Wrappers/Seldon/MatrixOperations.hxx"


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_SELDON_x_MATRIX_OPERATIONS_HPP_
