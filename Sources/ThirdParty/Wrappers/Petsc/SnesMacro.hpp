//! \file
//
//
//  SnesMacro.hpp
//  MoReFEM
//
//  Created by sebastien on 05/09/2018.
//Copyright © 2018 Inria. All rights reserved.
//

#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SNES_MACRO_HPP_
# define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SNES_MACRO_HPP_


// ============================
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// ============================

# ifdef MOREFEM_NO_TRAP_SNES_EXCEPTION

#  define MOREFEM_TRAP_SNES_EXCEPTION_TRY
#  define MOREFEM_TRAP_SNES_EXCEPTION_CATCH(Snes)

# else // MOREFEM_NO_TRAP_SNES_EXCEPTION

/*!
 * If the macro is not defined, in standard SnesInterface functions exceptions are caught and transformed into
 * Petsc error codes. This prevent libc++ abi issues if exceptions are thrown from one of functions fed to Petsc
 * through a pointer.
 */
#  define MOREFEM_TRAP_SNES_EXCEPTION_TRY try
#  define MOREFEM_TRAP_SNES_EXCEPTION_CATCH(Snes) \
    catch(const std::exception& e) \
    { \
    std::cerr << "Exception caught in the Petsc Snes definition: " << e.what() << std::endl; \
    return PETSC_ERR_MIN_VALUE; \
    } \
    catch(Seldon::Error& e) \
    { \
    std::cerr << "Exception caught in the Petsc Snes definition: " << e.What() << std::endl; \
    return PETSC_ERR_MIN_VALUE; \
    }

# endif // MOREFEM_NO_TRAP_SNES_EXCEPTION


// ============================
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// ============================


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SNES_MACRO_HPP_
