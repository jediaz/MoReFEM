/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 31 Aug 2017 16:47:35 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_BOOST_x_TEST_HPP_
# define MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_BOOST_x_TEST_HPP_

# include "Utilities/Pragma/Pragma.hpp"

PRAGMA_DIAGNOSTIC(push)
PRAGMA_DIAGNOSTIC(ignored "-Wsign-conversion")
PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")
PRAGMA_DIAGNOSTIC(ignored "-Wfloat-equal")
PRAGMA_DIAGNOSTIC(ignored "-Wparentheses")
PRAGMA_DIAGNOSTIC(ignored "-Wconversion")
PRAGMA_DIAGNOSTIC(ignored "-Wswitch-enum")

# ifdef __clang__
PRAGMA_DIAGNOSTIC(ignored "-Wdeprecated-dynamic-exception-spec")
PRAGMA_DIAGNOSTIC(ignored "-Wredundant-parens")
PRAGMA_DIAGNOSTIC(ignored "-Wreserved-id-macro")
PRAGMA_DIAGNOSTIC(ignored "-Wweak-vtables")
PRAGMA_DIAGNOSTIC(ignored "-Wdisabled-macro-expansion")
PRAGMA_DIAGNOSTIC(ignored "-Wcovered-switch-default")
PRAGMA_DIAGNOSTIC(ignored "-Wmissing-prototypes")
PRAGMA_DIAGNOSTIC(ignored "-Wundef")

    #  if (not defined(__apple_build_version__) || __clang_major__ >= 10)
    PRAGMA_DIAGNOSTIC(ignored "-Wzero-as-null-pointer-constant")
    #  endif

    #  if (not defined(__apple_build_version__) && __clang_major__ >= 8)
    PRAGMA_DIAGNOSTIC(ignored "-Wextra-semi-stmt")
    #  endif


# endif // __clang__

#include "boost/test/unit_test.hpp"

PRAGMA_DIAGNOSTIC(pop)


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_BOOST_x_TEST_HPP_
