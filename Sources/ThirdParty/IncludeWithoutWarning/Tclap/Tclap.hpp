/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 15 Feb 2017 17:13:49 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_TCLAP_x_TCLAP_HPP_
# define MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_TCLAP_x_TCLAP_HPP_

# include "Utilities/Pragma/Pragma.hpp"

PRAGMA_DIAGNOSTIC(push)

# ifdef __clang__
    PRAGMA_DIAGNOSTIC(ignored "-Wweak-vtables")
    PRAGMA_DIAGNOSTIC(ignored "-Wdeprecated")
    #  if (not defined(__apple_build_version__) || __clang_major__ >= 10)
        PRAGMA_DIAGNOSTIC(ignored "-Wzero-as-null-pointer-constant")
        PRAGMA_DIAGNOSTIC(ignored "-Wshadow-field")
    #  endif

    #  if (not defined(__apple_build_version__) && __clang_major__ >= 8)
    PRAGMA_DIAGNOSTIC(ignored "-Wextra-semi-stmt")
    #  endif


# endif // __clang__

PRAGMA_DIAGNOSTIC(ignored "-Wmissing-noreturn")
PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")
PRAGMA_DIAGNOSTIC(ignored "-Wsign-conversion")

#include "tclap/CmdLine.h"

PRAGMA_DIAGNOSTIC(pop)


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_TCLAP_x_TCLAP_HPP_
