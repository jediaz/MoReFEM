add_executable(MoReFEMTestCoordsInParallel
               ${CMAKE_CURRENT_LIST_DIR}/InputData.hpp
               ${CMAKE_CURRENT_LIST_DIR}/Model.cpp
               ${CMAKE_CURRENT_LIST_DIR}/Model.hpp
               ${CMAKE_CURRENT_LIST_DIR}/Model.hxx
               ${CMAKE_CURRENT_LIST_DIR}/main.cpp
              )
          
target_link_libraries(MoReFEMTestCoordsInParallel
    ${ALL_LOAD_BEGIN_FLAG}                    
    ${MOREFEM_MODEL}
    ${ALL_LOAD_END_FLAG})
