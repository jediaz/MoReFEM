add_library(MoReFEMTestOperatorMass_lib ${LIBRARY_TYPE} "")

target_sources(MoReFEMTestOperatorMass_lib
    PRIVATE
                ${CMAKE_CURRENT_LIST_DIR}/ExpectedResults.hpp
                ${CMAKE_CURRENT_LIST_DIR}/ExpectedResults_P1P1.cpp
                ${CMAKE_CURRENT_LIST_DIR}/ExpectedResults_P2P1.cpp
                ${CMAKE_CURRENT_LIST_DIR}/ExpectedResults_P2P2.cpp
                ${CMAKE_CURRENT_LIST_DIR}/InputData.hpp
                ${CMAKE_CURRENT_LIST_DIR}/Model.cpp
                ${CMAKE_CURRENT_LIST_DIR}/Model.hpp
                ${CMAKE_CURRENT_LIST_DIR}/Model.hxx
                ${CMAKE_CURRENT_LIST_DIR}/SameUnknown.cpp
                ${CMAKE_CURRENT_LIST_DIR}/UnknownP1TestP1.cpp
                ${CMAKE_CURRENT_LIST_DIR}/UnknownP2TestP1.cpp
)

target_link_libraries(MoReFEMTestOperatorMass_lib
                      ${MOREFEM_TEST_TOOLS})


add_executable(MoReFEMTestOperatorMass3D ${CMAKE_CURRENT_LIST_DIR}/main_3D.cpp)
target_link_libraries(MoReFEMTestOperatorMass3D MoReFEMTestOperatorMass_lib)

add_executable(MoReFEMTestOperatorMass2D ${CMAKE_CURRENT_LIST_DIR}/main_2D.cpp)
target_link_libraries(MoReFEMTestOperatorMass2D MoReFEMTestOperatorMass_lib)

add_executable(MoReFEMTestOperatorMass1D ${CMAKE_CURRENT_LIST_DIR}/main_1D.cpp)
target_link_libraries(MoReFEMTestOperatorMass1D  MoReFEMTestOperatorMass_lib)

add_test(OperatorMass3D
         MoReFEMTestOperatorMass3D
         --
         ${MOREFEM_ROOT}
         ${MOREFEM_TEST_OUTPUT_DIR})

set_tests_properties(OperatorMass3D PROPERTIES TIMEOUT 5)

add_test(OperatorMass2D
         MoReFEMTestOperatorMass2D
         --
         ${MOREFEM_ROOT}
         ${MOREFEM_TEST_OUTPUT_DIR})

set_tests_properties(OperatorMass2D PROPERTIES TIMEOUT 5)

add_test(OperatorMass1D
         MoReFEMTestOperatorMass1D
         --
         ${MOREFEM_ROOT}
         ${MOREFEM_TEST_OUTPUT_DIR})

set_tests_properties(OperatorMass1D PROPERTIES TIMEOUT 5)
