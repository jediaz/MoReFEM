/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#define BOOST_TEST_MODULE non_linear_membrane_operator
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Tools/Fixture/Model.hpp"

#include "Test/Operators/VariationalInstances/NonlinearMembrane/Model.hpp"
#include "Test/Operators/VariationalInstances/NonlinearMembrane/InputData.hpp"

using namespace MoReFEM;


namespace // anonymous
{


    struct LuaFile
    {


        static std::string GetPath();


    };

    using fixture_type =
        TestNS::FixtureNS::Model
        <
            TestNS::NonLinearMembraneOperatorNS::Model,
            LuaFile
        >;


    using ::MoReFEM::Internal::assemble_into_matrix;

    using ::MoReFEM::Internal::assemble_into_vector;


} // namespace anonymous


PRAGMA_DIAGNOSTIC(push)
#ifdef __clang__
PRAGMA_DIAGNOSTIC(ignored "-Wdisabled-macro-expansion")
#endif // __clang__


BOOST_FIXTURE_TEST_SUITE(same_unknown_for_test, fixture_type)

    BOOST_AUTO_TEST_CASE(same_unknown_for_test_matrix_only)
    {
        GetModel().TestP1P1(assemble_into_matrix::yes, assemble_into_vector::no);
    }


    BOOST_AUTO_TEST_CASE(same_unknown_for_test_vector_only)
    {
        GetModel().TestP1P1(assemble_into_matrix::no, assemble_into_vector::yes);
    }


    BOOST_AUTO_TEST_CASE(same_unknown_for_test_matrix_and_vector)
    {
        GetModel().TestP1P1(assemble_into_matrix::yes, assemble_into_vector::yes);
    }

BOOST_AUTO_TEST_SUITE_END()


BOOST_FIXTURE_TEST_SUITE(different_unknown, fixture_type)

    BOOST_AUTO_TEST_CASE(unknown_p2_test_p1)
    {
        GetModel().TestP2P1();
    }

BOOST_AUTO_TEST_SUITE_END()


namespace // anonymous
{


    std::string LuaFile::GetPath()
    {
        decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

        return
        environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/Operators/"
                                                   "VariationalInstances/NonlinearMembrane/"
                                                   "demo_input_nonlinear_membrane.lua");
    }

    
} // namespace anonymous

