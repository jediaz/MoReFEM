/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#define BOOST_TEST_MODULE grad_grad_operator_2d
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Tools/Fixture/Model.hpp"
#include "Test/Tools/MacroVariationalOperator.hpp"

#include "Test/Operators/VariationalInstances/GradGrad/Model.hpp"
#include "Test/Operators/VariationalInstances/GradGrad/InputData.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    struct LuaFile
    {


        static std::string GetPath();


    };

    using fixture_type =
        TestNS::FixtureNS::Model
        <
            TestNS::GradGrad::Model,
            LuaFile
        >;


} // namespace anonymous


TEST_VARIATIONAL_OPERATOR


namespace // anonymous
{


    std::string LuaFile::GetPath()
    {
        decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

        return
            environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/Operators/"
                                                       "VariationalInstances/GradGrad/"
                                                       "demo_input_parameter_2D.lua");
    }

} // namespace anonymous

