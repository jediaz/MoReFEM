/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 15 Mar 2018 16:50:27 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#include "Test/Operators/VariationalInstances/Pk2/ExpectedResults.hpp"


namespace MoReFEM
{
    
    
    namespace TestNS::Pk2
    {


        namespace // anonymous
        {


            expected_results_type<IsMatrixOrVector::matrix> Matrix3D();
            expected_results_type<IsMatrixOrVector::matrix> Matrix2D();
            expected_results_type<IsMatrixOrVector::matrix> Matrix1D();
            expected_results_type<IsMatrixOrVector::vector> Vector3D();
            expected_results_type<IsMatrixOrVector::vector> Vector2D();
            expected_results_type<IsMatrixOrVector::vector> Vector1D();


        } // namespace anonymous


        expected_results_type<IsMatrixOrVector::matrix> GetExpectedMatrixP1P1(unsigned int dimension)
        {
            switch (dimension)
            {
                case 3u:
                    return Matrix3D();
                case 2u:
                    return Matrix2D();
                case 1u:
                    return Matrix1D();
                default:
                    assert(false && "Invalid case!");
                    exit(EXIT_FAILURE);
            }
        }


        expected_results_type<IsMatrixOrVector::vector> GetExpectedVectorP1P1(unsigned int dimension)
        {
            switch (dimension)
            {
                case 3u:
                    return Vector3D();
                case 2u:
                    return Vector2D();
                case 1u:
                    return Vector1D();
                default:
                    assert(false && "Invalid case!");
                    exit(EXIT_FAILURE);
            }
        }


        namespace // anonymous
        {


            expected_results_type<IsMatrixOrVector::matrix> Matrix3D()
            {
                return expected_results_type<IsMatrixOrVector::matrix>

                {
                    { 0.276918, 0.0109222, 0.0109222, -0.0558125, -0.121755, -0.121755,
                        -0.110553, 0.113216, -0.00238355, -0.110553, -0.00238355, 0.113216 },
                    { 0.0109222, 0.315643, -0.193767, 0.112061, -0.0725773, 0.0456531,
                        -0.0443309, -0.0292346, 0.00736394, -0.0786526, -0.213832, 0.14075 },
                    { 0.0109222, -0.193767, 0.315643, 0.112061, 0.0456531, -0.0725773,
                        -0.0786526, 0.14075, -0.213832, -0.0443309, 0.00736394, -0.0292346 },
                    { -0.0558125, 0.112061, 0.112061, 0.140605, 0.00962944, 0.00962944,
                        -0.0423962, 0.074993, -0.196684, -0.0423962, -0.196684, 0.074993 },
                    { -0.121755, -0.0725773, 0.0456531, 0.00962944, 0.152302, -0.0897037,
                        0.0920734, -0.0617898, 0.0405748, 0.0200523, -0.0179353, 0.00347586 },
                    { -0.121755, 0.0456531, -0.0725773, 0.00962944, -0.0897037, 0.152302,
                        0.0200523, 0.00347586, -0.0179353, 0.0920734, 0.0405748, -0.0617898 },
                    { -0.110553, -0.0443309, -0.0786526, -0.0423962, 0.0920734, 0.0200523,
                        0.243893, -0.112954, 0.133855, -0.0909445, 0.065212, -0.075255 },
                    { 0.113216, -0.0292346, 0.14075, 0.074993, -0.0617898, 0.00347586,
                        -0.112954, 0.918354, -0.873746, -0.075255, -0.82733, 0.72952 },
                    { -0.00238355, 0.00736394, -0.213832, -0.196684, 0.0405748,
                        -0.0179353, 0.133855, -0.873746, 1.0591, 0.065212, 0.825807, -0.82733 },
                    { -0.110553, -0.0786526, -0.0443309, -0.0423962, 0.0200523, 0.0920734,
                        -0.0909445, -0.075255, 0.065212, 0.243893, 0.133855, -0.112954 },
                    { -0.00238355, -0.213832, 0.00736394, -0.196684, -0.0179353, 0.0405748,
                        0.065212, -0.82733, 0.825807, 0.133855, 1.0591, -0.873746 },
                    { 0.113216, 0.14075, -0.0292346, 0.074993, 0.00347586, -0.0617898,
                        -0.075255, 0.72952, -0.82733, -0.112954, -0.873746, 0.918354 },
                };
            }


            expected_results_type<IsMatrixOrVector::matrix> Matrix2D()
            {
                assert(false && "Not yet implemented.");
                exit(EXIT_FAILURE);
            }


            expected_results_type<IsMatrixOrVector::matrix> Matrix1D()
            {
                assert(false && "Not yet implemented.");
                exit(EXIT_FAILURE);
            }


            expected_results_type<IsMatrixOrVector::vector> Vector3D()
            {
                return  expected_results_type<IsMatrixOrVector::vector>
                {
                    0.120398, 0.797626, 0.797626, 0.535767, -0.294971, -0.294971,
                    -0.328082, -0.209101, -0.293554, -0.328082, -0.293554, -0.209101,
                };
            }


            expected_results_type<IsMatrixOrVector::vector> Vector2D()
            {
                assert(false && "Not yet implemented.");
                exit(EXIT_FAILURE);
            }


            expected_results_type<IsMatrixOrVector::vector> Vector1D()
            {
                assert(false && "Not yet implemented.");
                exit(EXIT_FAILURE);
            }


        } // namespace anonymous



    } // namespace TestNS::Pk2


} // namespace MoReFEM


