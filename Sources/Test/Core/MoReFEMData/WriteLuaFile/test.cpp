/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 25 Jul 2018 18:29:56 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include <cstdlib>
#include <tuple>

#include "Utilities/Containers/Print.hpp"

#define BOOST_TEST_MODULE core_write_lua_file
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/Filesystem/File.hpp"
#include "Utilities/Filesystem/Folder.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/TimeManagerInstance.hpp"

#include "ThirdParty/IncludeWithoutWarning/Seldon/Seldon.hpp"
#include "ThirdParty/IncludeWithoutWarning/Mpi/Mpi.hpp"

#include "Test/Tools/InitializeTestMoReFEMData.hpp"
#include "Test/Core/MoReFEMData/WriteLuaFile/InputData.hpp"
#include "Test/Tools/Fixture/Environment.hpp"


using namespace MoReFEM;



PRAGMA_DIAGNOSTIC(push)
# ifdef __clang__
PRAGMA_DIAGNOSTIC(ignored "-Wdisabled-macro-expansion")
# endif // __clang__


BOOST_FIXTURE_TEST_CASE(check_value, TestNS::FixtureNS::Environment)
{
    decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

    const std::string output_directory = environment.SubstituteValues("${MOREFEM_TEST_OUTPUT_DIR}/TestWriteLuaFile/");

    if (FilesystemNS::Folder::DoExist(output_directory))
        FilesystemNS::Folder::Remove(output_directory, __FILE__, __LINE__);

    FilesystemNS::Folder::Create(output_directory, __FILE__, __LINE__);

    auto generated_lua = environment.SubstituteValues(output_directory + "generated_default_lua_file.lua");

    if (FilesystemNS::File::DoExist(generated_lua))
        FilesystemNS::File::Remove(generated_lua, __FILE__, __LINE__);

    using input_data_type = TestNS::WriteLuaFileNS::InputData;

    BOOST_CHECK_THROW(TestNS::InitializeTestMoReFEMData<input_data_type> init(std::move(generated_lua)),
                      std::exception);

    auto full_fledged_lua_file =
        environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/Core/MoReFEMData/WriteLuaFile/demo.lua");

    TestNS::InitializeTestMoReFEMData<input_data_type> init(std::move(full_fledged_lua_file));

    auto generated_by_direct_call_lua_file = output_directory + "/generated_by_direct_call_default_lua_file.lua";

    Utilities::InputDataNS::CreateDefaultInputFile<input_data_type::Tuple>(generated_by_direct_call_lua_file);

    BOOST_CHECK(FilesystemNS::File::AreEquals(generated_lua, generated_by_direct_call_lua_file, __FILE__, __LINE__));

    decltype(auto) morefem_data = init.GetMoReFEMData();

    auto lua_file_with_content = output_directory + "/generated_with_content.lua";
    Utilities::InputDataNS::Write(morefem_data.GetInputData(), lua_file_with_content);
}


BOOST_FIXTURE_TEST_CASE(reload_and_rewrite, TestNS::FixtureNS::Environment)
{
    decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

    const std::string output_directory = environment.SubstituteValues("${MOREFEM_TEST_OUTPUT_DIR}/TestWriteLuaFile/");
    auto lua_file_with_content = output_directory + "/generated_with_content.lua";

    using input_data_type = TestNS::WriteLuaFileNS::InputData;
    TestNS::InitializeTestMoReFEMData<input_data_type> init(std::move(lua_file_with_content));

    decltype(auto) morefem_data = init.GetMoReFEMData();
    auto regenerated_file = output_directory + "/regenerated_with_content.lua";
    Utilities::InputDataNS::Write(morefem_data.GetInputData(), regenerated_file);

    BOOST_CHECK(FilesystemNS::File::AreEquals(lua_file_with_content, regenerated_file, __FILE__, __LINE__));

}

PRAGMA_DIAGNOSTIC(pop)


