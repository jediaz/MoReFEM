-- Result
Result = {
	-- Directory in which all the results will be written. 
	-- Expected format: "VALUE"
    output_directory = "${MOREFEM_WHATEVER_DIR}/Foo",
    
    -- Enables to skip some printing in the console. Can be used to WriteSolution every n time.
    -- Expected format: VALUE
    display_value = 1,
    
    -- Defines the solutions output format. Set to false for ascii or true for binary.
    -- Expected format: VALUE
    binary_output = false
}
