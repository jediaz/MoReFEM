ValueOutsideBrace = 3.14

TransientSource4 = {

    nature = { "lua_function", "constant", "piecewise_constant_by_domain"},

    value = {
        [[
        function (x, y, z)
        return 100. * math.exp(-((x - 0.5)^2 + (y - 0.5)^2)/(0.1^2));
        end
        ]],
        0.,
    { [1] = 0., [3] = 14.5, [4] = -31.231 }} -- didn't pass before #1468

} -- TransientSource4


TransientSource5 = {

    nature = { "lua_function", "constant", "piecewise_constant_by_domain"},

    value = {
        [[
        function (x, y, z)
        return 100. * math.exp(-((x - 0.5)^2 + (y - 0.5)^2)/(0.1^2));
        end
        ]],
        0.,
    { [1] = 0., [3] = 14.5, [4] = -31.231 } },

    whatever = {

        sublevel = { 4 },

        sublevel2 = 3

    }

} -- TransientSource5
