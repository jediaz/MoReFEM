include(${CMAKE_CURRENT_LIST_DIR}/HasMember/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/Environment/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/TupleHasType/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/LuaOptionFile/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/TypeName/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/Now/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/PrintContainer/CMakeLists.txt)

# Needs to be incorporated (or dropped) - see #1272
# include(${CMAKE_CURRENT_LIST_DIR}/InputData/CMakeLists.txt)




