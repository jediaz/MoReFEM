 -- Comment lines are introduced by "--".
-- In a section (i.e. within braces), all entries must be separated by a comma.



-- Unknown1: displacement.
Unknown1 = {
    -- Name of the unknown (used for displays in output).
    -- Expected format: "VALUE"
    name = "generic_scalar_unknown",
    
    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: "VALUE"
    -- Constraint: value_in(v, {'scalar', 'vectorial'})
    nature = "scalar"
}


-- Mesh1
Mesh1 = {
    -- Path of the mesh file to use.
    -- Expected format: "VALUE"
    mesh = "${MOREFEM_ROOT}/Data/Mesh/TwoCubes.mesh",
    
    -- Format of the input mesh.
    -- Expected format: "VALUE"
    -- Constraint: value_in(v, {'Ensight', 'Medit'})
    format = "Medit",
    
    -- Highest dimension of the input mesh. This dimension might be lower than the one effectively read in the mesh file;
    -- in which case Coords will be reduced provided all the dropped values are 0. If not, an exception is thrown.
    -- Expected format: VALUE
    -- Constraint: v <= 3 and v > 0
    dimension = 3
}


-- Domain1 - The 3D elements of the cylinder.
Domain1 = {
    -- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
    -- left empty if domain not limited to one mesh; at most one value is expected here.
    -- Expected format: {VALUE1}
    mesh_index = { 1 },
    
    -- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon
    -- dimensions.
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: value_in(v, {0, 1, 2, 3})
    dimension_list = { 3 },
    
    -- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
    -- labels. This parameter does not make sense if no mesh is defined for the domain.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { },
    
    -- List of geometric element types considered in the domain. Might be left empty if no restriction upon
    -- these.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = { }
}


-- FiniteElementSpace1
FiniteElementSpace1 = {
    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: VALUE
    god_of_dof_index = 1,
    
    -- Index of the domain onto which the finite element space is defined. This domain must be unidimensional.
    -- Expected format: VALUE
    domain_index = 1,
    
    -- List of all unknowns defined in the finite element space. Unknowns here must be defined in this file as
    -- an 'Unknown' block; expected name/identifier is the name given there.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    unknown_list = { "generic_scalar_unknown" },
    
    -- List of the shape function to use for each unknown;
    -- Expected format: {"VALUE1", "VALUE2", ...}
    shape_function_list = {"Q6"},
    
    -- List of the numbering subset to use for each unknown;
    -- Expected format: {VALUE1, VALUE2, ...}
    numbering_subset_list = { 1 }

}



-- BoundaryCondition
BoundaryCondition = {
	-- 0:Pseudo-elimination, 2:penalisation
	-- Expected format: VALUE
	-- Constraint: v >= 0 and v < 3
	essentialBoundaryConditionsMethod = 0,

	-- Comp1, Comp2 or Comp3
	-- Expected format: {"VALUE1", "VALUE2", ...}
	-- Constraint: value_in(v, {'Comp1', 'Comp2', 'Comp3', 'Comp12', 'Comp23', 'Comp13', 'Comp123'})
	component = {"Comp1"},

	-- Variable name the boundary condition apply to
	-- Expected format: {"VALUE1", "VALUE2", ...}
	variable = {"generic_scalar_unknown"},

	-- Variable value at this boundary.
	-- Expected format: {VALUE1, VALUE2, ...}
	value = {0.},

	-- number of labels concerned by this boundary condition
	-- Expected format: {VALUE1, VALUE2, ...}
	-- Constraint: v >= 0
	numLabel = {1},

	-- label (number) in the mesh file. Apply boundary condition on elements with this label
	-- Expected format: {VALUE1, VALUE2, ...}
	-- Constraint: v >= 0
	label = {1},
}





-- Result
Result = {
    -- Directory in which all the results will be written. 
    -- Expected format: "VALUE"
    output_directory = "${MOREFEM_TEST_OUTPUT_DIR}/TestOndomaticNumbering",
    
    -- Defines the solutions output format. Set to false for ascii or true for binary.
    -- Expected format: VALUE
    binary_output = false
  }




