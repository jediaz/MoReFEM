/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 29 Aug 2018 17:29:23 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#define BOOST_TEST_MODULE nan_or_inf
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "ThirdParty/Wrappers/Seldon/MatrixOperations.hpp"


using namespace MoReFEM;


namespace // anonymous
{

    double local_inf = 1. / 0.;

    double local_nan = local_inf * 0.;

}


PRAGMA_DIAGNOSTIC(push)
# ifdef __clang__
PRAGMA_DIAGNOSTIC(ignored "-Wdisabled-macro-expansion")
# endif // __clang__


BOOST_AUTO_TEST_CASE(is_number)
{
    BOOST_CHECK(NumericNS::IsNumber(5.));
    BOOST_CHECK(NumericNS::IsNumber(4.f));
    BOOST_CHECK(NumericNS::IsNumber(local_inf) == false);
    BOOST_CHECK(NumericNS::IsNumber(local_nan) == false);
}



BOOST_AUTO_TEST_CASE(seldon_vector)
{
    LocalVector vector(3);
    vector(0) = 0.;
    vector(1) = -10.;
    vector(2) = 10.;

    BOOST_CHECK(Wrappers::Seldon::AreNanOrInfValues(vector) == false);

    vector(2) = local_nan;
    BOOST_CHECK(Wrappers::Seldon::AreNanOrInfValues(vector) == true);

    vector(2) = 0.;
    vector(0) = local_inf;
    BOOST_CHECK(Wrappers::Seldon::AreNanOrInfValues(vector) == true);
}


BOOST_AUTO_TEST_CASE(seldon_matrix)
{
    LocalMatrix matrix(3, 3);
    matrix.Zero();

    BOOST_CHECK(Wrappers::Seldon::AreNanOrInfValues(matrix) == false);

    matrix(2, 0) = local_nan;
    BOOST_CHECK(Wrappers::Seldon::AreNanOrInfValues(matrix) == true);

    matrix(2, 0) = 0.;
    matrix(1, 1) = local_inf;
    BOOST_CHECK(Wrappers::Seldon::AreNanOrInfValues(matrix) == true);
}

PRAGMA_DIAGNOSTIC(pop)
