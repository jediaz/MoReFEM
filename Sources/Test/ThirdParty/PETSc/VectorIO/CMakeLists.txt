add_executable(MoReFEMTestPetscVectorIO
               ${CMAKE_CURRENT_LIST_DIR}/main.cpp
               ${CMAKE_CURRENT_LIST_DIR}/InputData.hpp
               ${CMAKE_CURRENT_LIST_DIR}/Model.cpp
               ${CMAKE_CURRENT_LIST_DIR}/Model.hpp
               ${CMAKE_CURRENT_LIST_DIR}/Model.hxx
              )
          
target_link_libraries(MoReFEMTestPetscVectorIO
                      ${MOREFEM_TEST_TOOLS}
                      )

add_test(PetscVectorIO
         MoReFEMTestPetscVectorIO
         --
         ${MOREFEM_ROOT}
         ${MOREFEM_TEST_OUTPUT_DIR}/Seq)

set_tests_properties(PetscVectorIO PROPERTIES TIMEOUT 10)

add_test(PetscVectorIO-mpi
         ${OPEN_MPI_INCL_DIR}/../bin/mpirun
         --oversubscribe
         -np 4
         MoReFEMTestPetscVectorIO
         --
         ${MOREFEM_ROOT}
         ${MOREFEM_TEST_OUTPUT_DIR}/Mpi4)

set_tests_properties(PetscVectorIO-mpi PROPERTIES TIMEOUT 10)
