add_executable(MoReFEMTestMpiSendReceive
               ${CMAKE_CURRENT_LIST_DIR}/test.cpp
              )
          
target_link_libraries(MoReFEMTestMpiSendReceive
                      ${MOREFEM_TEST_TOOLS})
                      
add_test(MpiSendReceive
         ${OPEN_MPI_INCL_DIR}/../bin/mpirun
         --oversubscribe
         -np 4
         MoReFEMTestMpiSendReceive
         --
         ${MOREFEM_ROOT}
         ${MOREFEM_TEST_OUTPUT_DIR})

set_tests_properties(MpiSendReceive PROPERTIES TIMEOUT 3)
