/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 16 Mar 2018 12:33:49 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_TOOLS_x_FIXTURE_x_MODEL_HPP_
# define MOREFEM_x_TEST_x_TOOLS_x_FIXTURE_x_MODEL_HPP_

# include <memory>
# include <vector>

# include "Test/Tools/InitializeTestMoReFEMData.hpp"
# include "Test/Tools/Fixture/Environment.hpp"


namespace MoReFEM
{


    namespace TestNS::FixtureNS
    {


        /*!
         * \brief An helper class to build tests with Boost test.
         *
         * The goal is to provide in a main different tests:
         *
         * \code
         namespace // anonymous
         {
            struct LuaFile
            {
                static std::string GetPath();
            };

            using fixture_type =
                TestNS::FixtureNS::Model
                <
                    TestNS::Pk2::Model,
                    LuaFile
                >;
         } // namespace anonymous

         BOOST_FIXTURE_TEST_CASE(fixture_type, "Unknown and test function unknown are identical")
         {
            GetModel().SameUnknown();
         }

         BOOST_FIXTURE_TEST_CASE(fixture_type, "Unknown and test function unknown are different but share the same P1 shape function label")
         {
            GetModel().UnknownP1TestP1();
         }

         BOOST_FIXTURE_TEST_CASE(fixture_type, "Unknown and test function unknown are different; unknown is P2 and test function P1")
         {
            GetModel().UnknownP2TestP1();
         }
         * \endcode
         *
         * The fixture here is a hack to run once and only once most of the initialization steps (MPI initialization,
         * building of the different singletons, etc...).
         *
         * Basically it builds and run the \a Model throughout all of the integration tests.
         *
         * \attention Contrary to the original intent of the fixture, it does NOT make tabula rasa of the state after
         * each test. So the tests should be a independant as possible from one another.
         *
         * \tparam ModelT Type of the model to build.
         * \tparam LuaFileT An ad hoc class which encapsulates in a \a GetPath() method the path to the Lua file.
         */
        template
        <
            class ModelT,
            class LuaFileT
        >
        struct Model : public Environment
        {
        public:

            //! Alias to MoReFEMData.
            using morefem_data_type = typename ModelT::morefem_data_type;

            //! Alias to input parameter list within the model.
            using input_data_type = typename morefem_data_type::input_data_type;

            //! Alias to the proper InitializeTestMoReFEMData type.
            using initialize_type = TestNS::InitializeTestMoReFEMData<input_data_type>;

            /*!
             * \brief Static method which yields the model considered for the tests.
             *
             * \return Constant reference to the \a Model.
             */
            static const ModelT& GetModel();

            /*!
             * \brief Static method which yields the \a MoReFEMData considered for the tests.
             *
             * This static method is not really useful for a full-fledged model, which provides access to this anyway,
             * but is really handy for mock models used in some unit tests.
             *
             * \return Constant reference to the \a MoReFEMData.
             */
            static const morefem_data_type& GetMoReFEMData();

        public:

            /// \name Special members.
            ///@{

            //! Constructor.
            explicit Model();

            //! Destructor.
            ~Model() = default;

            //! \copydoc doxygen_hide_copy_constructor
            Model(const Model& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            Model(Model&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            Model& operator=(const Model& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            Model& operator=(Model&& rhs) = delete;

            ///@}

        };


    } // namespace TestNS::FixtureNS


} // namespace MoReFEM


# include "Test/Tools/Fixture/Model.hxx"


#endif // MOREFEM_x_TEST_x_TOOLS_x_FIXTURE_x_MODEL_HPP_
