//! \file 
//
//
//  test.cpp
//  MoReFEM
//
//  Created by sebastien on 12/04/2019.
//Copyright © 2019 Inria. All rights reserved.
//

#include <cstdlib>
#include <sstream>

#define BOOST_TEST_MODULE ensight_unused_vertices
#include "Utilities/OutputFormat/OutputFormat.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Geometry/Mesh/Mesh.hpp"
#include "Geometry/Mesh/Internal/MeshManager.hpp"

#include "PostProcessing/OutputFormat/Ensight6.hpp"

#include "Test/Tools/Fixture/Environment.hpp"
#include "Test/Tools/CompareEnsightFiles.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    unsigned int GenerateNewMeshId();

    void TestCase(double space_unit);


} // namespace anonymous


PRAGMA_DIAGNOSTIC(push)
# ifdef __clang__
PRAGMA_DIAGNOSTIC(ignored "-Wdisabled-macro-expansion")
# endif // __clang__

BOOST_FIXTURE_TEST_SUITE(ensight_unused_vertices, MoReFEM::TestNS::FixtureNS::Environment)


BOOST_AUTO_TEST_CASE(space_unit_1)
{
    TestCase(1.);
}

BOOST_AUTO_TEST_CASE(space_unit_2)
{
    TestCase(2.);
}

BOOST_AUTO_TEST_SUITE_END()

PRAGMA_DIAGNOSTIC(pop)


namespace // anonymous
{


    unsigned int GenerateNewMeshId()
    {
        static auto ret = 0u;
        return ++ret;
    }


    void TestCase(double space_unit)
    {
        decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::CreateOrGetInstance(__FILE__, __LINE__);
        decltype(auto) environment = Utilities::Environment::GetInstance(__FILE__, __LINE__);
        Utilities::OutputFormat::CreateOrGetInstance(__FILE__, __LINE__, false);

        std::string data_directory =
        environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/PostProcessing/EnsightUnusedVertices/Data");

        auto mesh_unique_id = GenerateNewMeshId();
        std::string mesh_file = data_directory + "/cylinder_unused_vertices.mesh";
        constexpr auto dimension = 3u;

        mesh_manager.Create(mesh_unique_id,
                            mesh_file,
                            dimension,
                            MeshNS::Format::Medit,
                            space_unit);

        decltype(auto) mesh = mesh_manager.GetMesh(mesh_unique_id);

        std::vector<unsigned int> numbering_subset_id_list { 1 };
        std::vector<std::string> unknown_list { "solid_displacement" };
        std::string output_directory = environment.SubstituteValues("${MOREFEM_TEST_OUTPUT_DIR}/Ensight6");

        PostProcessingNS::OutputFormat::Ensight6 ensight_output(data_directory,
                                                                unknown_list,
                                                                numbering_subset_id_list,
                                                                mesh,
                                                                PostProcessingNS::RefinedMesh::no,
                                                                output_directory);

        std::string ref_dir = data_directory + "/../ExpectedResult/Ensight6";

        std::ostringstream oconv;
        for (auto time_iteration = 0ul; time_iteration < 2ul; ++time_iteration)
        {
            oconv.str("");
            oconv << "solid_displacement." << std::setw(5) << std::setfill('0') << time_iteration << ".scl";
            TestNS::CompareEnsightFiles(ref_dir, output_directory, oconv.str(),
                                        __FILE__, __LINE__, 1.e-11);
        }

    }


} // namespace anonymous
