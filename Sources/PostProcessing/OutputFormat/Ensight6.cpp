/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 23 Dec 2014 16:13:29 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup PostProcessingGroup
// \addtogroup PostProcessingGroup
// \{
*/


#include <fstream>

#include "Utilities/Filesystem/File.hpp"
#include "Utilities/String/String.hpp"

#include "Geometry/Mesh/Mesh.hpp"

#include "PostProcessing/PostProcessing.hpp"
#include "PostProcessing/OutputFormat/Ensight6.hpp"


namespace MoReFEM::PostProcessingNS::OutputFormat
{


    namespace // anonymous
    {

        using DofWithValuesType = std::pair<Data::DofInformation::const_shared_ptr, double>;


        class ExtendedDofInformation
        {
        public:
            ExtendedDofInformation(const Data::UnknownInformation& unknown_info);

            const Data::UnknownInformation& GetUnknownInfo() const;

            /*!
             *
             * \param[in] coords_list Processor-wise coordinates.
             * \param[in] dof_with_values The list of all couples (dof, value on the local processor).
             *
             */
            std::vector<std::vector<std::size_t> >&
                ComputeOrGetCoordsMatch(const Coords::vector_unique_ptr& coords_list,
                                        const std::vector<DofWithValuesType>& dof_with_values);


        private:

            const Data::UnknownInformation& unknown_info_;

            //! Index is the position of the Coords in the mesh, starting at 0.
            //! The inner vector is the index of the dof as it is stored in the interface GetVertexCoordsIndexList()
            //! method.
            std::vector<std::vector<std::size_t> > coords_match_;

        };


        void CreateCaseFile(const std::string& output_directory,
                            const Data::UnknownInformation::vector_const_shared_ptr& unknown_list,
                            const Data::TimeIteration::vector_const_unique_ptr& time_iteration_list);

        void CreateUnknownFile(ExtendedDofInformation& dof_info,
                               const unsigned int numbering_subset_id,
                               const PostProcessingNS::Data::TimeIteration& time_iteration,
                               const std::string& data_directory,
                               const PostProcessing& post_processing,
                               RefinedMesh is_mesh_refined);


        // Class in charge of writing dof values in the Ensight file while respecting expected format (6 values per
        // line at most)
        class WriteDofValue
        {
        public:

            // Constructor.
            WriteDofValue(std::ostream& out);

            // Write a single new dof value onto the stream.
            void NewValue(double value);

            // Add \a Nzeros zeros onto the stream.
            void AddZeros(unsigned Nzeros);

        private:

            std::ostream& stream_;

            unsigned int index_ = 0u;
        };


    } // namespace anonymous
            
                   
    Ensight6::Ensight6(const std::string& data_directory,
                       const std::vector<std::string>& unknown_list,
                       const std::vector<unsigned int>& numbering_subset_id_list,
                       const Mesh& mesh,
                       RefinedMesh is_mesh_refined,
                       const std::string& output_directory)
    {
        assert(unknown_list.size() == numbering_subset_id_list.size());
        const auto Nunknown = unknown_list.size();

        if (!FilesystemNS::Folder::DoExist(data_directory))
            throw Exception("Invalid data directory: '" + data_directory + "' doesn't exist!",
                            __FILE__, __LINE__);

        const std::string mesh_directory = data_directory + "/Mesh_"
                                         + std::to_string(mesh.GetUniqueId()) + "/";

        if (!FilesystemNS::Folder::DoExist(mesh_directory))
            throw Exception("Invalid mesh directory: '" + mesh_directory + "' doesn't exist!",
                            __FILE__, __LINE__);

        const std::string ensight_directory =
            output_directory.empty()
            ? mesh_directory + "Ensight6/"
            : output_directory;

        if (FilesystemNS::Folder::DoExist(ensight_directory))
            FilesystemNS::Folder::Remove(ensight_directory, __FILE__, __LINE__);

        FilesystemNS::Folder::Create(ensight_directory, __FILE__, __LINE__);

        PostProcessing post_processing(data_directory,
                                       numbering_subset_id_list,
                                       mesh);

        mesh.Write<MeshNS::Format::Ensight>(ensight_directory + "/mesh.geo");

        const auto& time_iteration_list = post_processing.GetTimeIterationList();

        // Create the case file.
        decltype(auto) complete_unknown_list = post_processing.GetExtendedUnknownList();
        Data::UnknownInformation::vector_const_shared_ptr selected_unknown_list;


        // Associate the more complete unknown object from the name given in input.
        {
            for (const auto& unknown_name : unknown_list)
            {
                const auto it = std::find_if(complete_unknown_list.cbegin(),
                                             complete_unknown_list.cend(),
                                             [&unknown_name](const auto& unknown_ptr)
                                             {
                                                 assert(!(!unknown_ptr));
                                                 return unknown_ptr->GetName() == unknown_name;
                                             });

                if (it == complete_unknown_list.cend())
                    throw Exception("Unknown '" + unknown_name + "' required in the constructor was not one of "
                                    "those used in the model.",
                                    __FILE__, __LINE__);

                selected_unknown_list.push_back(*it);
            }
        }

        assert(selected_unknown_list.size() == Nunknown);
        CreateCaseFile(ensight_directory,
                       selected_unknown_list,
                       time_iteration_list);

        // Sort the data properly.
        std::multimap<unsigned int, ExtendedDofInformation> unknown_list_per_numbering_subset;

        for (auto i = 0ul; i < Nunknown; ++i)
        {
            ExtendedDofInformation buf(*selected_unknown_list[i]);
            unknown_list_per_numbering_subset.insert({numbering_subset_id_list[i], buf});
        }

        // For each Unknown, compute the relationship array which tells for each \a Coord of the Ensight mesh file
        // the matching dofs (if any).

        // Iterate over time:
        for (const auto& time_iteration_ptr : time_iteration_list)
        {
            assert(!(!time_iteration_ptr));
            const auto& time_iteration = *time_iteration_ptr;

            const auto range = unknown_list_per_numbering_subset.equal_range(time_iteration.GetNumberingSubsetId());


            for (auto it = range.first; it != range.second; ++it)
            {
                auto& unknown_list_for_numbering_subset = *it;

                auto& dof_info = unknown_list_for_numbering_subset.second;

                CreateUnknownFile(dof_info,
                                  unknown_list_for_numbering_subset.first,
                                  time_iteration,
                                  ensight_directory,
                                  post_processing,
                                  is_mesh_refined);
            }
        }

    }


    namespace // anonymous
    {


        void CreateCaseFile(const std::string& output_directory,
                            const Data::UnknownInformation::vector_const_shared_ptr& unknown_list,
                            const Data::TimeIteration::vector_const_unique_ptr& time_iteration_list)
        {
            std::string case_file(output_directory);
            case_file += "/problem.case";

            std::ofstream stream;

            FilesystemNS::File::Create(stream, case_file, __FILE__, __LINE__);

            stream << "FORMAT" << std::endl;
            stream << "type: ensight" << std::endl;
            stream << "GEOMETRY" << std::endl;
            stream << "model: 1 mesh.geo" << std::endl;
            stream << "VARIABLE" << std::endl;

            for (const auto& unknown_ptr : unknown_list)
            {
                assert(!(!unknown_ptr));

                switch (unknown_ptr->GetNature())
                {
                    case Data::UnknownNature::scalar:
                        stream << "scalar";
                        break;
                    case Data::UnknownNature::vectorial:
                        stream << "vector";
                        break;
                }

                const auto& name = unknown_ptr->GetName();

                stream << " per node: 1 " << name << ' ' << name << ".*****.scl" << std::endl;
            }

            stream << "TIME" << std::endl;
            stream << "time set: 1" << std::endl;

            unsigned int count = 0u;

            unsigned int previous_index = static_cast<unsigned int>(-1);

            std::ostringstream time_values_stream;
            std::ostringstream time_index_stream;

            auto Ntime_iteration = 0u;

            for (const auto& time_iteration_ptr : time_iteration_list)
            {
                assert(!(!time_iteration_ptr));
                const auto& time_iteration = *time_iteration_ptr;

                const auto current_iteration = time_iteration.GetIteration();

                if (current_iteration == previous_index) // May happen if several unknowns for same time step.
                    continue;

                ++Ntime_iteration;

                time_index_stream << current_iteration;
                time_values_stream << time_iteration.GetTime();

                if (++count % 5u == 0u) // lines must not exceed 79 characters; I'm truly on the very safe side here!
                {
                    time_index_stream << std::endl;
                    time_values_stream << std::endl;
                }
                else
                {
                    time_index_stream << ' ';
                    time_values_stream << ' ';
                }

                previous_index = current_iteration;
            }

            stream << "number of steps: " << Ntime_iteration << std::endl;
            stream << "filename numbers: ";
            stream << time_index_stream.str();

            stream << std::endl;

            stream << "time values: ";
            stream << time_values_stream.str();

            stream << std::endl;
        }


        WriteDofValue::WriteDofValue(std::ostream& out)
        : stream_(out)
        { }


        void WriteDofValue::NewValue(double value)
        {
            stream_ << std::setw(12) << std::scientific << std::setprecision(5) << value;

            if (++index_ % 6u == 0u)
                stream_ << std::endl;
        }


        void WriteDofValue::AddZeros(unsigned Nzeros)
        {
            for (auto i = 0ul; i < Nzeros; ++i)
                NewValue(0.);
        }


        void CreateUnknownFile(ExtendedDofInformation& dof_information,
                               const unsigned int numbering_subset_id,
                               const PostProcessingNS::Data::TimeIteration& time_iteration,
                               const std::string& output_directory,
                               const PostProcessing& post_processing,
                               RefinedMesh is_mesh_refined)
        {
            const auto& unknown = dof_information.GetUnknownInfo();
            const auto& unknown_name = unknown.GetName();

            const auto& mesh = post_processing.GetMesh();

            std::ostringstream oconv;
            oconv << output_directory;
            oconv << '/' << unknown_name << '.' << std::setfill('0') << std::setw(5)
            << time_iteration.GetIteration() << ".scl";

            std::string ensight_output_file(oconv.str());

            std::ofstream ensight_output_stream;

            FilesystemNS::File::Create(ensight_output_stream, ensight_output_file, __FILE__, __LINE__);
            ensight_output_stream << "First line which content must be chosen (but later)" << std::endl;

            const auto unknown_nature = unknown.GetNature();

            const std::size_t Nprocessor = post_processing.Nprocessor();

            std::vector<DofWithValuesType> dof_with_values;

            #ifndef NDEBUG
            if (is_mesh_refined == RefinedMesh::yes)
            {
                assert(Nprocessor == 1ul
                       && "At the moment, restricted to model run sequentially.");
            }
            #endif // NDEBUG

            decltype(auto) filename_prototype = time_iteration.GetSolutionFilename();
            


            for (std::size_t processor = 0u; processor < Nprocessor; ++processor)
            {
                const auto& dof_list = post_processing.GetDofInformationList(numbering_subset_id, processor);

                std::string filename = filename_prototype;

                const auto check = Utilities::String::Replace("*", std::to_string(processor), filename);

                assert(check == 1 && "Exactly one wildcard occurrence is expected in given filename");
                static_cast<void>(check);

                const auto& solution = LoadVector(filename);

                for (const auto& dof_ptr : dof_list)
                {
                    assert(!(!dof_ptr));
                    const auto& dof = *dof_ptr;

                    if (dof.GetUnknown() != unknown_name)
                        continue;

                    if (is_mesh_refined == RefinedMesh::no)
                    {
                        // Very crude: dofs not on vertex are simply ignored.
                        if (dof.GetInterfaceNature() != InterfaceNS::Nature::vertex)
                        {
                            static bool first = true;

                            if (first)
                            {
                                std::cout << "[WARNING] As ensight handles only P1, dofs not on vertices are "
                                "blatantly ignored." << std::endl;
                                first = false;
                            }

                            continue;
                        }

                    }

                    const unsigned int processor_wise_index = dof.GetProcessorWiseIndex();

                    assert(processor_wise_index < solution.size());

                    dof_with_values.push_back({dof_ptr, solution[processor_wise_index]});
                }
            }


            decltype(auto) coords_list = mesh.GetProcessorWiseCoordsList();

            decltype(auto) match = dof_information.ComputeOrGetCoordsMatch(coords_list, dof_with_values);

            WriteDofValue write_dof_value_helper(ensight_output_stream);

            std::vector<std::size_t> dof_index_checker;

            //for (const auto& pair : dof_with_values)
            for (const auto& dof_list_for_coords : match)
            {
                if (dof_list_for_coords.empty())
                {
                    if (unknown_nature == Data::UnknownNature::vectorial)
                        write_dof_value_helper.AddZeros(3u);
                    else
                        write_dof_value_helper.AddZeros(1u);
                }
                else
                {
                    const auto Ndof_list_for_coords = static_cast<unsigned int>(dof_list_for_coords.size());
                    assert(Ndof_list_for_coords >= 1u
                           || Ndof_list_for_coords <= 3u);

                    for (const auto& dof_index : dof_list_for_coords)
                    {
                        assert(dof_index < dof_with_values.size());
                        const auto dof_value = dof_with_values[dof_index].second;

                        write_dof_value_helper.NewValue(dof_value);
                    }

                    if (unknown_nature == Data::UnknownNature::vectorial)
                    {
                        // Ensight format expects 3D!
                        write_dof_value_helper.AddZeros(3u - Ndof_list_for_coords);
                    }
                }
            }
        }


        const Data::UnknownInformation& ExtendedDofInformation::GetUnknownInfo() const
        {
            return unknown_info_;
        }


        ExtendedDofInformation::ExtendedDofInformation(const Data::UnknownInformation& unknown_info)
        : unknown_info_(unknown_info)
        { }

                                                               
        std::vector<std::vector<std::size_t> >& ExtendedDofInformation
        ::ComputeOrGetCoordsMatch(const Coords::vector_unique_ptr& coords_list,
                                  const std::vector<DofWithValuesType>& dof_with_values)
        {
            if (!coords_match_.empty())
                return coords_match_;

            // Reminder:
            // - Post-processing runs in sequential;
            // - The mesh.geo file was built using that very same order.
            auto coords_list_counter = 0u;
            --coords_list_counter;

            coords_match_.resize(coords_list.size());

            for (const auto& coords_ptr : coords_list)
            {
                ++coords_list_counter;
                assert(!(!coords_ptr));
                const auto& coords = *coords_ptr;

                auto dof_with_values_counter = 0ul;
                --dof_with_values_counter;

                // Find - if any - the dofs that match these positions.
                for (const auto& pair : dof_with_values)
                {
                    ++dof_with_values_counter;

                    decltype(auto) dof_ptr = pair.first;
                    assert(!(!dof_ptr));
                    decltype(auto) dof_coords_index_list = dof_ptr->GetInterface().GetVertexCoordsIndexList();

                    assert(dof_coords_index_list.size() == 1ul
                           && "At the moment EnsightOutput deals only "
                           "with P1 (from both geometric and finite element point of view)!");

                    const unsigned int dof_coords_index = dof_coords_index_list.back();

                    assert(dof_coords_index < coords_list.size());
                    decltype(auto) dof_coords_ptr = coords_list[dof_coords_index];
                    const auto& dof_coords = *dof_coords_ptr;

                    if (NumericNS::AreEqual(coords.x(), dof_coords.x())
                        && NumericNS::AreEqual(coords.y(), dof_coords.y())
                        && NumericNS::AreEqual(coords.z(), dof_coords.z()))
                    {
                        assert(coords_list_counter < coords_match_.size());
                        coords_match_[coords_list_counter].push_back(dof_with_values_counter);
                    }
                }
            }

            return coords_match_;
        }


    } // namespace anonymous

    
} // namespace MoReFEM::PostProcessingNS::OutputFormat


/// @} // addtogroup PostProcessingGroup
