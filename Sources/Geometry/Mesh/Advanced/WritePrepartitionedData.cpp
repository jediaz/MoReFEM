//! \file 
//
//
//  WritePrepartitionedData.cpp
//  MoReFEM
//
//  Created by sebastien on 15/07/2019.
//Copyright © 2019 Inria. All rights reserved.
//

#include <sstream>
#include <fstream>

#include "Utilities/Filesystem/File.hpp"

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"

#include "Geometry/Mesh/Mesh.hpp"
#include "Geometry/Mesh/Advanced/WritePrepartitionedData.hpp"
#include "Geometry/Mesh/Internal/Format/Medit.hpp"
#include "Geometry/Mesh/Internal/Format/Ensight.hpp"
#include "Geometry/Mesh/Internal/ComputeInterfaceListInMesh.hpp"


namespace MoReFEM::Advanced::MeshNS
{


    namespace // anonymous
    {


        template<::MoReFEM::MeshNS::Format FormatT>
        using Informations = ::MoReFEM::Internal::MeshNS::FormatNS::Informations<FormatT>;


    } // namespace anonymous


    WritePrepartitionedData::WritePrepartitionedData(const Wrappers::Mpi& mpi,
                                                     const Mesh& mesh,
                                                     const std::string& output_directory,
                                                     ::MoReFEM::MeshNS::Format format)
    {
        const auto rank = mpi.GetRank<int>();

        std::ostringstream oconv;
        oconv << output_directory << "/Mesh_" << mesh.GetUniqueId() << "/mesh_" << rank << '.';

        using namespace ::MoReFEM::MeshNS;

        // Note: I could have made that more elegant by making the function template, but I didn't deem it that
        // useful and
        switch (format)
        {
            case ::MoReFEM::MeshNS::Format::Medit:
            {
                oconv << Informations<Format::Medit>::Extension();
                reduced_mesh_file_ = oconv.str();
                mesh.Write<Format::Medit>(reduced_mesh_file_);
                break;
            }
            case ::MoReFEM::MeshNS::Format::Ensight:
            {
                oconv << Informations<Format::Ensight>::Extension();
                reduced_mesh_file_ = oconv.str();
                mesh.Write<Format::Ensight>(reduced_mesh_file_);
                break;
            }
            case ::MoReFEM::MeshNS::Format::VTK_PolygonalData:
            case ::MoReFEM::MeshNS::Format::End:
            {
                throw Exception("Format not supported for pre-partition.", __FILE__, __LINE__);
            }
        } // switch

        oconv.str("");
        oconv << output_directory << "/Mesh_" << mesh.GetUniqueId() << "/mesh_data_" << rank << ".lua";
        partition_data_file_ = oconv.str();

        std::ofstream out;

        {
            FilesystemNS::File::Create(out, partition_data_file_, __FILE__, __LINE__);
            out << "Nprocessor_wise_coord = " << mesh.NprocessorWiseCoord() << std::endl;
            out << "Nghosted_coord = " << mesh.NghostCoord()<< std::endl;
            out << "space_unit = " << mesh.GetSpaceUnit() << std::endl;
            out << "Nprocessor_wise_vertex = " << mesh.Nvertex() << std::endl;
            out << "Nprogram_wise_vertex = " << mesh.NprogramWiseVertex() << std::endl;
            out << "Nprocessor_wise_edge = " << mesh.Nedge() << std::endl;
            out << "Nprocessor_wise_face = " << mesh.Nface() << std::endl;
            out << "Nprocessor_wise_volume = " << mesh.Nvolume() << std::endl;
            out << "do_build_edges = " << (mesh.AreEdgesBuilt() ? "true" : "false") << std::endl;
            out << "do_build_faces = " << (mesh.AreFacesBuilt() ? "true" : "false") << std::endl;
            out << "do_build_volumes = " << (mesh.AreVolumesBuilt() ? "true" : "false") << std::endl;

            {
                decltype(auto) list = mesh.GetGeometricEltList();

                std::vector<std::size_t> index_list;
                index_list.reserve(list.size());

                for (const auto& ptr : list)
                {
                    assert(!(!ptr));
                    index_list.push_back(ptr->GetIndex());
                }

                out << "geometric_elt_index_list = ";
                Utilities::PrintContainer<>::Do(index_list, out, ", ", "{", "}");
                out << std::endl;
            }

            {
                decltype(auto) list = mesh.GetProcessorWiseCoordsList();

                std::vector<std::size_t> index_list;
                index_list.reserve(list.size());

                for (const auto& ptr : list)
                {
                    assert(!(!ptr));
                    index_list.push_back(ptr->GetIndex());
                }

                out << "processor_wise_coords_index_list = ";
                Utilities::PrintContainer<>::Do(index_list, out, ", ", "{", "}");
                out << std::endl;
            }

            {
                decltype(auto) list = mesh.GetGhostedCoordsList();

                std::vector<std::size_t> index_list;
                index_list.reserve(list.size());

                for (const auto& ptr : list)
                {
                    assert(!(!ptr));
                    index_list.push_back(ptr->GetIndex());
                }

                out << "ghosted_coords_index_list = ";
                Utilities::PrintContainer<>::Do(index_list, out, ", ", "{", "}");
                out << std::endl;
            }

            {
                Internal::MeshNS::ComputeInterfaceListInMesh original_interface_list(mesh);

                {
                    decltype(auto) list = original_interface_list.GetVertexList();

                    std::vector<std::size_t> index_list;
                    index_list.reserve(list.size());

                    for (const auto& ptr : list)
                    {
                        assert(!(!ptr));
                        index_list.push_back(ptr->GetIndex());
                    }

                    out << "vertex_index_list = ";
                    Utilities::PrintContainer<>::Do(index_list, out, ", ", "{", "}");
                    out << std::endl;
                }

                {
                    decltype(auto) list = original_interface_list.GetEdgeList();

                    std::vector<std::size_t> index_list;
                    index_list.reserve(list.size());

                    for (const auto& ptr : list)
                    {
                        assert(!(!ptr));
                        index_list.push_back(ptr->GetIndex());
                    }

                    out << "edge_index_list = ";
                    Utilities::PrintContainer<>::Do(index_list, out, ", ", "{", "}");
                    out << std::endl;
                }

                {
                    decltype(auto) list = original_interface_list.GetFaceList();

                    std::vector<std::size_t> index_list;
                    index_list.reserve(list.size());

                    for (const auto& ptr : list)
                    {
                        assert(!(!ptr));
                        index_list.push_back(ptr->GetIndex());
                    }

                    out << "face_index_list = ";
                    Utilities::PrintContainer<>::Do(index_list, out, ", ", "{", "}");
                    out << std::endl;
                }

                {
                    decltype(auto) list = original_interface_list.GetVolumeList();

                    std::vector<std::size_t> index_list;
                    index_list.reserve(list.size());

                    for (const auto& ptr : list)
                    {
                        assert(!(!ptr));
                        index_list.push_back(ptr->GetIndex());
                    }

                    out << "volume_index_list = ";
                    Utilities::PrintContainer<>::Do(index_list, out, ", ", "{", "}");
                    out << std::endl;
                }
            }
        }
    }


} // namespace MoReFEM::Advanced::MeshNS
