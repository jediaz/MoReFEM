/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 24 Nov 2014 14:59:10 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

# include "Utilities/Containers/UnorderedMap.hpp"
#include "Utilities/Containers/Print.hpp"

#include "Geometry/Mesh/Mesh.hpp"
#include "Geometry/Mesh/Internal/ComputeInterfaceListInMesh.hpp"


namespace MoReFEM
{


    namespace Internal
    {
        
        
        namespace MeshNS
        {
        
        
            namespace // anonymous
            {
                

                template<class InterfaceT>
                void ComputeMeshLevelList(const typename InterfaceT::vector_shared_ptr& geom_elt_interface_list,
                                          std::unordered_set<std::size_t>& set,
                                          typename InterfaceT::vector_shared_ptr& mesh_list);

                void ComputeMeshLevelListForVolume(const GeometricElt& geom_elt,
                                                   Volume::vector_shared_ptr& mesh_volume_list);

                template<class InterfaceT>
                void PrintTypeOfInterface(const typename InterfaceT::vector_shared_ptr& interface_list,
                                          std::ostream& out);
             
                
                
                
            } // namespace anonymous
            
            
            ComputeInterfaceListInMesh::ComputeInterfaceListInMesh(const Mesh& mesh)
            {
                const auto& geom_elt_list = mesh.GetGeometricEltList();
                
                // The list of interfaces is not stored as such in mesh...
                vertex_list_.reserve(mesh.Nvertex());
                edge_list_.reserve(mesh.Nedge());
                face_list_.reserve(mesh.Nface());
                volume_list_.reserve(mesh.Nvolume());

//                std::map<std::size_t, Vertex::shared_ptr> vertex_map;
//                std::map<std::size_t, Edge::shared_ptr> edge_map;
//                std::map<std::size_t, Face::shared_ptr> face_map;


                std::unordered_set<std::size_t> mesh_vertex_index_list, mesh_edge_index_list, mesh_face_index_list;
                mesh_vertex_index_list.max_load_factor(Utilities::DefaultMaxLoadFactor());
                mesh_edge_index_list.max_load_factor(Utilities::DefaultMaxLoadFactor());
                mesh_face_index_list.max_load_factor(Utilities::DefaultMaxLoadFactor());

                // Work variables. Should not lead to too much allocation: std::vector<>::clear() leaves the
                // capacity unchanged.
                Edge::vector_shared_ptr geom_elt_edge_list;
                Face::vector_shared_ptr geom_elt_face_list;

                auto interface_without_orientation =
                [](const auto& oriented_interface_ptr)
                {
                    assert(!(!oriented_interface_ptr));
                    return oriented_interface_ptr->GetUnorientedInterfacePtr();
                };

                for (const auto& geom_elt_ptr : geom_elt_list)
                {
                    assert(!(!geom_elt_ptr));
                    
                    const auto& geom_elt = *geom_elt_ptr;
                    ComputeMeshLevelList<Vertex>(geom_elt.GetVertexList(), mesh_vertex_index_list, vertex_list_);

                    geom_elt_edge_list.clear();
                    decltype(auto) oriented_edge_list = geom_elt.GetOrientedEdgeList();

                    std::transform(oriented_edge_list.cbegin(),
                                   oriented_edge_list.cend(),
                                   std::back_inserter(geom_elt_edge_list),
                                   interface_without_orientation);

                    ComputeMeshLevelList<Edge>(geom_elt_edge_list, mesh_edge_index_list, edge_list_);

                    geom_elt_face_list.clear();
                    decltype(auto) oriented_face_list = geom_elt.GetOrientedFaceList();

                    std::transform(oriented_face_list.cbegin(),
                                   oriented_face_list.cend(),
                                   std::back_inserter(geom_elt_face_list),
                                   interface_without_orientation);

                    ComputeMeshLevelList<Face>(geom_elt_face_list, mesh_face_index_list, face_list_);

                    ComputeMeshLevelListForVolume(geom_elt, volume_list_);
                }

                assert(vertex_list_.size() == mesh.Nvertex());
                assert(edge_list_.size() == mesh.Nedge());
                assert(face_list_.size() == mesh.Nface());
                assert(volume_list_.size() == mesh.Nvolume());
            }
            
            
            void ComputeInterfaceListInMesh::Print(std::ostream& out) const
            {
                out << "#For each geometric interface, give the list of the Coords that delimits it. The index shown "
                "is the position of the Coords in the generated mesh (regardless of the convention used in the "
                "original mesh format)." << std::endl;
                
                {
                    const auto& vertex_list = GetVertexList();
                    for (auto vertex_ptr : vertex_list)
                    {
                        assert(!(!vertex_ptr));
                        out << vertex_ptr->GetNature() << ' ' << vertex_ptr->GetIndex() << ";";
                        
                        // Put it into a vector solely to ensure same print format as for the other interfaces.
                        const auto& vertex_coords_list = vertex_ptr->GetVertexCoordsList();
                        assert(vertex_coords_list.size() == 1ul);
                        assert(!(!vertex_coords_list.back()));
                        
                        std::vector<unsigned int> index
                        { vertex_coords_list.back()->GetPositionInCoordsListInMesh<MpiScale::processor_wise>() };
                        Utilities::PrintContainer<>::Do(index, out);
                    }
                }
                
                {
                    PrintTypeOfInterface<Edge>(GetEdgeList(), out);
                    PrintTypeOfInterface<Face>(GetFaceList(), out);
                    PrintTypeOfInterface<Volume>(GetVolumeList(), out);
                }
            }
            
            
            
            namespace // anonymous
            {
                

                template<class InterfaceT>
                void ComputeMeshLevelList(const typename InterfaceT::vector_shared_ptr& geom_elt_interface_list,
                                          std::unordered_set<std::size_t>& set,
                                          typename InterfaceT::vector_shared_ptr& mesh_list)

                {
                    for (const typename InterfaceT::shared_ptr& interface_ptr : geom_elt_interface_list)
                    {
                        assert(!(!interface_ptr));
                        const std::size_t index = interface_ptr->GetIndex();

                        const auto it = set.find(index);

                        if (it == set.cend())
                        {
                            mesh_list.push_back(interface_ptr);
                            set.insert(index);
                        }
                    }
                }

                
                void ComputeMeshLevelListForVolume(const GeometricElt& geom_elt,
                                                   Volume::vector_shared_ptr& mesh_volume_list)
                {
                    auto volume_ptr = geom_elt.GetVolumePtr();
                    
                    if (!(!volume_ptr))
                        mesh_volume_list.push_back(volume_ptr);
                }
                                
                
                template<class InterfaceT>
                void PrintTypeOfInterface(const typename InterfaceT::vector_shared_ptr& interface_list,
                                          std::ostream& out)
                {
                    for (auto interface_ptr : interface_list)
                    {
                        assert(!(!interface_ptr));
                        const auto& interface = *interface_ptr;
                        
                        out << interface.StaticNature() << ' ' << interface.GetIndex() << ";";
                        
                        const auto& coords_list = interface.GetVertexCoordsList();
                        std::vector<unsigned int> index_list(coords_list.size());
                        
                        std::transform(coords_list.cbegin(), coords_list.cend(),
                                       index_list.begin(),
                                       [](const auto& coords_ptr)
                                       {
                                           assert(!(!coords_ptr));
                                           return coords_ptr->GetIndex();
                                       });
                        
                        std::sort(index_list.begin(), index_list.end());
                        
                        Utilities::PrintContainer<>::Do(index_list, out);
                    }
                    
                }

                
            } // namespace anonymous

        
        
        } // namespace MeshNS
        
        
    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
