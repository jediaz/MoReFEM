/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_MESH_HXX_
# define MOREFEM_x_GEOMETRY_x_MESH_x_MESH_HXX_



namespace MoReFEM
{


    inline unsigned int Mesh::GetDimension() const noexcept
    {
        return dimension_;
    }


    inline unsigned int Mesh::NprocessorWiseCoord() const noexcept
    {
        return static_cast<unsigned int>(processor_wise_coords_list_.size());
    }


    inline unsigned int Mesh::NghostCoord() const noexcept
    {
        return static_cast<unsigned int>(ghosted_coords_list_.size());
    }


    inline const GeometricElt::vector_shared_ptr& Mesh::GetGeometricEltList() const noexcept
    {
        return geometric_elt_list_.All();
    }


    inline RefGeomElt::vector_shared_ptr Mesh::BagOfEltType() const
    {
        return geometric_elt_list_.BagOfEltType();
    }


    inline RefGeomElt::vector_shared_ptr Mesh::BagOfEltType(unsigned int dimension) const
    {
        return geometric_elt_list_.BagOfEltType(dimension);
    }


    inline GeometricElt::vector_shared_ptr Mesh
    ::GeometricEltListInLabel(const MeshLabel::const_shared_ptr& label) const
    {
        return geometric_elt_list_.GeometricEltListInLabel(label);
    }


    inline unsigned int Mesh::NgeometricElt() const
    {
        return geometric_elt_list_.NgeometricElt();
    }


    inline unsigned int Mesh::NgeometricElt(unsigned int dimension) const
    {
        return geometric_elt_list_.NgeometricElt(dimension);
    }


    inline bool Mesh::AreEdgesBuilt() const
    {
        return (Nedge_ != NumericNS::UninitializedIndex<decltype(Nedge_)>());
    }


    inline bool Mesh::AreFacesBuilt() const
    {
        return (Nface_ != NumericNS::UninitializedIndex<decltype(Nface_)>());
    }


    inline bool Mesh::AreVolumesBuilt() const
    {
        return (Nvolume_ != NumericNS::UninitializedIndex<decltype(Nvolume_)>());
    }


    inline const Coords& Mesh::GetCoord(unsigned int index) const
    {
        assert(index < processor_wise_coords_list_.size()
               && "Make sure the index is a result of Coords::GetPositionInCoordsListInMesh() and not "
               "Coords::GetIndex().");
        assert(!(!processor_wise_coords_list_[index]));
        return *processor_wise_coords_list_[index];
    }


    inline unsigned int Mesh::Nvertex() const
    {
        assert("The vertices must have been built before their number is requested!" &&
               Nprocessor_wise_vertex_ != NumericNS::UninitializedIndex<decltype(Nprocessor_wise_vertex_)>());
        return Nprocessor_wise_vertex_;
    }


    inline unsigned int Mesh::Nedge() const
    {
        assert("The edges must have been built before their number is requested!" &&
               Nedge_ != NumericNS::UninitializedIndex<decltype(Nedge_)>());
        return Nedge_;
    }


    inline unsigned int Mesh::Nface() const
    {
        assert("The faces must have been built before their number is requested!" &&
               Nface_ != NumericNS::UninitializedIndex<decltype(Nface_)>());

        return Nface_;
    }


    inline unsigned int Mesh::Nvolume() const
    {
        assert("The volume must have been built before their number is requested!" &&
               Nvolume_ != NumericNS::UninitializedIndex<decltype(Nvolume_)>());

        return Nvolume_;
    }


    inline double Mesh::GetSpaceUnit() const noexcept
    {
        return space_unit_;
    }


    template<>
    inline void Mesh::SetNinterface<Vertex>(unsigned int N)
    {
        Nprocessor_wise_vertex_ = N;
    }


    template<>
    inline void Mesh::SetNinterface<Edge>(unsigned int N)
    {
        Nedge_ = N;
    }


    template<>
    inline void Mesh::SetNinterface<Face>(unsigned int N)
    {
        Nface_ = N;
    }


    template<>
    inline void Mesh::SetNinterface<Volume>(unsigned int N)
    {
        Nvolume_ = N;
    }



    namespace Traits
    {


        template<class InterfaceT>
        struct Orient;


        template<>
        struct Orient<Vertex>
        {
            using type = Vertex;

        };


        template<>
        struct Orient<Volume>
        {
            using type = Volume;
        };



        template<>
        struct Orient<Edge>
        {
            using type = OrientedEdge;
        };


        template<>
        struct Orient<OrientedEdge>
        {
            using type = OrientedEdge;
        };


        template<>
        struct Orient<Face>
        {
            using type = OrientedFace;
        };


        template<>
        struct Orient<OrientedFace>
        {
            using type = OrientedFace;
        };



    } // namespace Traits



    template<class InterfaceT>
    unsigned int Mesh::DetermineNInterface() const
    {
        const auto& geometric_elt_list = GetGeometricEltList();

        using OrientedType = typename Traits::Orient<InterfaceT>::type;

        typename OrientedType::vector_shared_ptr interface_list;

        for (const auto& geometric_elt_ptr : geometric_elt_list)
        {
            assert(!(!geometric_elt_ptr));

            const auto& geometric_elt_interface_list =
                Internal::InterfaceNS::GetInterfaceOfGeometricElt<OrientedType>(*geometric_elt_ptr);

            for (auto interface_ptr : geometric_elt_interface_list)
                interface_list.push_back(interface_ptr);
        }

        // We do want know to reduce the list of interfaces so that one interface appears only once there,
        // regardless of its orientation.
        // Using Utilities::PointerComparison::Less<typename OrientedType::shared_ptr>() as pointer criterion is
        // not enough: operator== for OrientedEdge and OrientedFace also checks for orientation.
        // Using the undelying pointer wouldn't work: Vertex doesn't define this method.
        // That's why the comparison is upon indexes: these indexes are the ones attributes before orientation is
        // considered, and they are present in Vertex, OrientedEdge and OrientedFace objects.


        auto equal_unoriented = [](const typename OrientedType::shared_ptr& lhs,
                                   const typename OrientedType::shared_ptr& rhs)
        {
            assert(!(!lhs));
            assert(!(!rhs));
            return lhs->GetIndex() == rhs->GetIndex();
        };


        Utilities::EliminateDuplicate(interface_list,
                                      Utilities::PointerComparison::Less<typename OrientedType::shared_ptr>(),
                                      equal_unoriented);

        return static_cast<unsigned int>(interface_list.size());
    }



    template<class InterfaceT>
    void Mesh::BuildInterface(typename InterfaceT::InterfaceMap& interface_list)
    {
        for (unsigned int dimension = 0u; dimension < 4; ++dimension)
        {
            RefGeomElt::vector_shared_ptr&& bag = BagOfEltType(dimension);

            if (bag.empty())
                continue;

            for (const RefGeomElt::shared_ptr& default_elt_type_ptr : bag)
            {
                assert(!(!default_elt_type_ptr));
                const auto& default_elt_type = *default_elt_type_ptr;

                auto geometric_elt_range = GetSubsetGeometricEltList(default_elt_type);

                for (auto it = geometric_elt_range.first; it != geometric_elt_range.second; ++it)
                {
                    auto geometric_elt_ptr = *it;
                    assert(!(!geometric_elt_ptr));
                    GeometricElt& geometric_element = *geometric_elt_ptr;
                    assert(geometric_element.GetIdentifier() == default_elt_type.GetIdentifier());

                    geometric_element.BuildInterface<InterfaceT>(geometric_elt_ptr.get(), interface_list);
                }
            }
        }

        const unsigned int Ninterface = static_cast<unsigned int>(interface_list.size());

        SetNinterface<InterfaceT>(Ninterface);
        assert(Ninterface == DetermineNInterface<InterfaceT>());
    }


    inline const Coords::vector_unique_ptr& Mesh::GetProcessorWiseCoordsList() const noexcept
    {
        return processor_wise_coords_list_;
    }


    inline const Coords::vector_unique_ptr& Mesh::GetGhostedCoordsList() const noexcept
    {
        return ghosted_coords_list_;
    }


    inline const MeshLabel::vector_const_shared_ptr& Mesh::GetLabelList() const noexcept
    {
        assert(std::is_sorted(label_list_.cbegin(),
                              label_list_.cend(),
                              Utilities::PointerComparison::Less<MeshLabel::const_shared_ptr>()));
        return label_list_;
    }


    inline Coords::vector_unique_ptr& Mesh::GetNonCstProcessorWiseCoordsList() noexcept
    {
        return const_cast<Coords::vector_unique_ptr&>(GetProcessorWiseCoordsList());
    }


    inline MeshLabel::vector_const_shared_ptr& Mesh::GetNonCstLabelList() noexcept
    {
        return const_cast<MeshLabel::vector_const_shared_ptr&>(GetLabelList());
    }


    inline bool operator==(const Mesh& lhs, const Mesh& rhs)
    {
        return lhs.GetUniqueId() == rhs.GetUniqueId();
    }


    inline unsigned int Mesh::NprogramWiseVertex() const noexcept
    {
        assert(Nprogram_wise_vertex_ != NumericNS::UninitializedIndex<unsigned int>());
        return Nprogram_wise_vertex_;
    }


    inline const GeometricElt& Mesh::GetGeometricEltFromIndex(unsigned int index) const
    {
        return geometric_elt_list_.GetGeometricEltFromIndex(index);
    }


    inline const GeometricElt& Mesh::GetGeometricEltFromIndex(unsigned int index,
                                                                    const RefGeomElt& ref_geom_elt) const
    {
        return geometric_elt_list_.GetGeometricEltFromIndex(index, ref_geom_elt);
    }



} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_MESH_HXX_
