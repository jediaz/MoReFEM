/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 21 Jan 2013 14:21:56 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_T_GEOMETRIC_ELT_HXX_
# define MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_T_GEOMETRIC_ELT_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace GeomEltNS
        {


            /// Deactivate noreturn here: it would trigger false positives. )
            PRAGMA_DIAGNOSTIC(push)
            PRAGMA_DIAGNOSTIC(ignored "-Wmissing-noreturn")

            template<class TraitsRefGeomEltT>
            TGeometricElt<TraitsRefGeomEltT>::TGeometricElt(unsigned int mesh_unique_id)
            : GeometricElt(mesh_unique_id)
            {
                StaticAssertBuiltInConsistency();
            }


            template<class TraitsRefGeomEltT>
            TGeometricElt<TraitsRefGeomEltT>::TGeometricElt(unsigned int mesh_unique_id,
                                                            const Coords::vector_unique_ptr& mesh_coords_list,
                                                            unsigned int index,
                                                            std::vector<unsigned int>&& coords)
            : GeometricElt(mesh_unique_id, mesh_coords_list, index, std::move(coords))
            {
                StaticAssertBuiltInConsistency();
            }


            template<class TraitsRefGeomEltT>
            TGeometricElt<TraitsRefGeomEltT>::TGeometricElt(unsigned int mesh_unique_id,
                                                            const Coords::vector_unique_ptr& mesh_coords_list,
                                                            std::vector<unsigned int>&& coords)
            : GeometricElt(mesh_unique_id, mesh_coords_list, std::move(coords))
            {
                StaticAssertBuiltInConsistency();
            }


            template<class TraitsRefGeomEltT>
            TGeometricElt<TraitsRefGeomEltT>::TGeometricElt(unsigned int mesh_unique_id,
                                                            const Coords::vector_unique_ptr& mesh_coords_list,
                                                            std::istream& stream)
            : GeometricElt(mesh_unique_id)
            {
                StaticAssertBuiltInConsistency();

                std::vector<unsigned int> coords_list(TraitsRefGeomEltT::Ncoords);

                for (unsigned int i = 0u; i < TraitsRefGeomEltT::Ncoords; ++i)
                    stream >> coords_list[i];

                if (stream)
                {
                    // Modify geometric element only if failbit not set.
                    SetCoordsList(mesh_coords_list, std::move(coords_list));
                }
                else
                    throw Exception("Failure to construct a GeometricElt from input stream.", __FILE__, __LINE__);
            }


            template<class TraitsRefGeomEltT>
            TGeometricElt<TraitsRefGeomEltT>::~TGeometricElt() = default;


            template<class TraitsRefGeomEltT>
            Advanced::GeometricEltEnum TGeometricElt<TraitsRefGeomEltT>::GetIdentifier() const
            {
                return TraitsRefGeomEltT::Identifier();
            }


            template<class TraitsRefGeomEltT>
            const std::string TGeometricElt<TraitsRefGeomEltT>::GetName() const
            {
                return TraitsRefGeomEltT::ClassName();
            }


            template<class TraitsRefGeomEltT>
            unsigned int TGeometricElt<TraitsRefGeomEltT>::Ncoords() const
            {
                return static_cast<unsigned int>(TraitsRefGeomEltT::Ncoords);
            }


            template<class TraitsRefGeomEltT>
            unsigned int TGeometricElt<TraitsRefGeomEltT>::Nvertex() const
            {
                return TraitsRefGeomEltT::topology::Nvertex;
            }


            template<class TraitsRefGeomEltT>
            unsigned int TGeometricElt<TraitsRefGeomEltT>::Nedge() const
            {
                return TraitsRefGeomEltT::topology::Nedge;
            }


            template<class TraitsRefGeomEltT>
            unsigned int TGeometricElt<TraitsRefGeomEltT>::Nface() const
            {
                return TraitsRefGeomEltT::topology::Nface;
            }


            template<class TraitsRefGeomEltT>
            unsigned int TGeometricElt<TraitsRefGeomEltT>::GetDimension() const
            {
                return TraitsRefGeomEltT::topology::dimension;
            }


            template<class TraitsRefGeomEltT>
            inline const std::string& TGeometricElt<TraitsRefGeomEltT>::GetEnsightName() const
            {
                if constexpr(!EnsightSupport())
                {
                    throw MoReFEM::ExceptionNS::Format::UnsupportedGeometricElt(TraitsRefGeomEltT::ClassName(),
                                                                                "Ensight",
                                                                                __FILE__, __LINE__);
                }
                else
                {
                    static auto ret =
                        Internal::MeshNS::FormatNS::Support
                        <
                            ::MoReFEM::MeshNS::Format::Ensight,
                            TraitsRefGeomEltT::Identifier()
                        >::EnsightName();

                    return ret;
                }
            }


            template<class TraitsRefGeomEltT>
            inline GmfKwdCod TGeometricElt<TraitsRefGeomEltT>::GetMeditIdentifier() const
            {
                if constexpr(!MeditSupport())
                {
                    throw MoReFEM::ExceptionNS::Format::UnsupportedGeometricElt(TraitsRefGeomEltT::ClassName(),
                                                                                "Medit",
                                                                                __FILE__, __LINE__);
                }
                else
                {
                    return Internal::MeshNS::FormatNS
                        ::Support<::MoReFEM::MeshNS::Format::Medit, TraitsRefGeomEltT::Identifier()>::MeditId();
                }
            }


            template<class TraitsRefGeomEltT>
            inline void TGeometricElt<TraitsRefGeomEltT>
            ::WriteEnsightFormat(std::ostream& stream, bool do_print_index) const
            {
                if constexpr(!EnsightSupport())
                {
                    static_cast<void>(stream);
                    static_cast<void>(do_print_index);
                    throw MoReFEM::ExceptionNS::GeometricElt::FormatNotSupported(TraitsRefGeomEltT::ClassName(),
                                                                                 "Ensight",
                                                                                 __FILE__, __LINE__);
                }
                else
                {
                    if (do_print_index)
                        stream << std::setw(8) << GetIndex();

                    decltype(auto) coords_list = GetCoordsList();

                    for (const auto& coord_ptr : coords_list)
                    {
                        assert(!(!coord_ptr));
                        stream << std::setw(8) << coord_ptr->GetIndex();
                    }

                    stream << '\n';
                }
            }


            template<class TraitsRefGeomEltT>
            void TGeometricElt<TraitsRefGeomEltT>
            ::WriteMeditFormat(const libmeshb_int mesh_index,
                               const std::unordered_map<unsigned int, int>& processor_wise_reindexing) const
            {
                if constexpr(!MeditSupport())
                {
                    static_cast<void>(mesh_index);
                    static_cast<void>(processor_wise_reindexing);
                    throw MoReFEM::ExceptionNS::GeometricElt::FormatNotSupported(TraitsRefGeomEltT::ClassName(),
                                                                                 "Medit",
                                                                                 __FILE__, __LINE__);
                }
                else
                {
                    auto label_ptr = GetMeshLabelPtr();
                    int label_index = (label_ptr ? static_cast<int>(label_ptr->GetIndex()) : 0);

                    const auto& coords_list = GetCoordsList();

                    // Medit assumes indexes between 1 and Ncoord, whereas MoReFEM starts at 0.
                    std::vector<int> coord_index_list;
                    coord_index_list.reserve(coords_list.size());

                    for (const auto& coord_ptr : coords_list)
                    {
                        assert(!(!coord_ptr));
                        auto it = processor_wise_reindexing.find(coord_ptr->GetIndex());

                        if (it == processor_wise_reindexing.cend())
                        {
                            std::vector<unsigned int> list;
                            list.reserve(processor_wise_reindexing.size());

                            for (const auto& item : processor_wise_reindexing)
                                list.push_back(item.first);

                            Utilities::EliminateDuplicate(list);

                            Utilities::PrintContainer<>::Do(list, std::cout);

                            std::cout << "INDEX = " << coord_ptr->GetIndex() << std::endl;
                        }

                        assert(it != processor_wise_reindexing.cend()
                               && "Otherwise processor_wise_reindexing is poorly built.");

                        coord_index_list.push_back(it->second);
                    }

                    ::MoReFEM::Wrappers::Libmesh
                    ::MeditSetLin<TraitsRefGeomEltT::Ncoords>(mesh_index,
                                                              GetMeditIdentifier(),
                                                              coord_index_list,
                                                              label_index);
                }
            }


            template<class TraitsRefGeomEltT>
            void TGeometricElt<TraitsRefGeomEltT>::ReadMeditFormat(const Coords::vector_unique_ptr& mesh_coords_list,
                                                                   libmeshb_int libmesh_mesh_index,
                                                                   unsigned int Ncoord_in_mesh,
                                                                   int& label_index)
            {
                if constexpr(!MeditSupport())
                {
                    static_cast<void>(mesh_coords_list);
                    static_cast<void>(libmesh_mesh_index);
                    static_cast<void>(Ncoord_in_mesh);
                    static_cast<void>(label_index);
                    throw MoReFEM::ExceptionNS::GeometricElt::FormatNotSupported(TraitsRefGeomEltT::ClassName(),
                                                                                 "Medit",
                                                                                 __FILE__, __LINE__);
                }
                else
                {
                    std::vector<unsigned int> coords(TraitsRefGeomEltT::Ncoords);

                    ::MoReFEM::Wrappers::Libmesh
                    ::MeditGetLin<TraitsRefGeomEltT::Ncoords>(libmesh_mesh_index,
                                                              GetMeditIdentifier(),
                                                              coords,
                                                              label_index);

                    // Check here the indexes of the coords are between 1 and Ncoord:
                    for (auto coord_index : coords)
                    {
                        if (coord_index == 0 || coord_index > Ncoord_in_mesh)
                            throw MoReFEM::ExceptionNS::Format::Medit::InvalidCoordIndex(TraitsRefGeomEltT::ClassName(),
                                                                                            coord_index,
                                                                                            Ncoord_in_mesh,
                                                                                            __FILE__,
                                                                                            __LINE__);
                    }

                    SetCoordsList(mesh_coords_list, std::move(coords));
                }
            }



            template<class TraitsRefGeomEltT>
            inline double TGeometricElt<TraitsRefGeomEltT>::ShapeFunction(unsigned int i,
                                                                             const LocalCoords& local_coords) const
            {
                return TraitsRefGeomEltT::ShapeFunction(i, local_coords);
            }


            template<class TraitsRefGeomEltT>
            inline double TGeometricElt<TraitsRefGeomEltT>
            ::FirstDerivateShapeFunction(unsigned int i, unsigned int icoor,
                                         const LocalCoords& local_coords) const
            {
                return TraitsRefGeomEltT::FirstDerivateShapeFunction(i, icoor, local_coords);
            }


            template<class TraitsRefGeomEltT>
            inline double TGeometricElt<TraitsRefGeomEltT>
            ::SecondDerivateShapeFunction(unsigned int i,
                                          unsigned int icoor,
                                          unsigned int jcoor,
                                          const LocalCoords& local_coords) const
            {
                return TraitsRefGeomEltT::SecondDerivateShapeFunction(i, icoor, jcoor, local_coords);
            }


            template<class TraitsRefGeomEltT>
            void TGeometricElt<TraitsRefGeomEltT>::
            BuildVertexList(const GeometricElt* geom_elt_ptr,
                            Vertex::InterfaceMap& existing_list)
            {
                const auto& coords_list = GetCoordsList();

                using Topology = typename TraitsRefGeomEltT::topology;

                auto&& vertex_list =
                    Internal::InterfaceNS::Build<Vertex, Topology>::Perform(geom_elt_ptr, coords_list, existing_list);

                SetVertexList(std::move(vertex_list));
            }


            template<class TraitsRefGeomEltT>
            void TGeometricElt<TraitsRefGeomEltT>::
            BuildEdgeList(const GeometricElt* geom_elt_ptr,
                          Edge::InterfaceMap& existing_list)
            {
                using Topology = typename TraitsRefGeomEltT::topology;

                auto&& oriented_edge_list =
                    Internal::InterfaceNS::ComputeEdgeList<Topology>(geom_elt_ptr,
                                                                     GetCoordsList(),
                                                                     existing_list);

                SetOrientedEdgeList(std::move(oriented_edge_list));
            }


            template<class TraitsRefGeomEltT>
            void TGeometricElt<TraitsRefGeomEltT>::
            BuildFaceList(const GeometricElt* geom_elt_ptr,
                          Face::InterfaceMap& existing_list)
            {
                using Topology = typename TraitsRefGeomEltT::topology;

                auto&& oriented_face_list =
                    Internal::InterfaceNS::ComputeFaceList<Topology>(geom_elt_ptr,
                                                                     GetCoordsList(),
                                                                     existing_list);

                SetOrientedFaceList(std::move(oriented_face_list));
            }


            template<class TraitsRefGeomEltT>
            void TGeometricElt<TraitsRefGeomEltT>::
            BuildVolumeList(const GeometricElt* geom_elt,
                            Volume::InterfaceMap& existing_list)
            {
                Volume::shared_ptr volume_ptr =
                (TraitsRefGeomEltT::topology::Nvolume > 0u ? std::make_shared<Volume>(shared_from_this()) : nullptr);
                SetVolume(volume_ptr);

                if (!(!volume_ptr))
                {
                    assert("One Volume shoudn't be entered twice in the list: it is proper to each GeometricElt."
                           && existing_list.find(volume_ptr) == existing_list.cend());

                    std::vector<const GeometricElt*> vector_raw;
                    vector_raw.push_back(geom_elt);

                    existing_list.insert({volume_ptr, vector_raw});
                }

            }


            template<class TraitsRefGeomEltT>
            void TGeometricElt<TraitsRefGeomEltT>::StaticAssertBuiltInConsistency()
            {
                static_assert(static_cast<unsigned int>(TraitsRefGeomEltT::Nderivate_component_) ==
                              static_cast<unsigned int>(TraitsRefGeomEltT::topology::dimension),
                              "Shape function should be consistent with geometric element!");
            }


            // Reactivate the warning disabled at the beginning of this file.
            PRAGMA_DIAGNOSTIC(pop)


        } // namespace GeomEltNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup




#endif // MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_T_GEOMETRIC_ELT_HXX_
