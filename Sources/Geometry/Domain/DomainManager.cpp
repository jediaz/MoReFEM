/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 20 Mar 2015 16:39:22 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include "Geometry/Domain/DomainManager.hpp"


namespace MoReFEM
{


    DomainManager::~DomainManager() = default;
    
    
    const std::string& DomainManager::ClassName()
    {
        static std::string ret("DomainManager");
        return ret;
    }
    
    
    DomainManager::DomainManager() = default;

    
    void DomainManager::Create(unsigned int unique_id,
                               const std::vector<unsigned int>& mesh_index,
                               const std::vector<unsigned int>& dimension_list,
                               const std::vector<unsigned int>& mesh_label_list,
                               const std::vector<std::string>& geometric_type_list)
    {
        // make_unique is not accepted here: it makes the code yell about private status of the constructor
        // with both clang and gcc.
        Domain* buf = new Domain(unique_id,
                                 mesh_index,
                                 dimension_list,
                                 mesh_label_list,
                                 geometric_type_list);
        
        auto&& ptr = Domain::const_unique_ptr(buf);
        
        assert(ptr->GetUniqueId() == unique_id);
        
        auto&& pair = std::make_pair(unique_id, std::move(ptr));
        
        auto insert_return_value = list_.insert(std::move(pair));
        
        if (!insert_return_value.second)
            throw Exception("Two Domain objects can't share the same unique identifier! (namely "
                            + std::to_string(unique_id) + ").", __FILE__, __LINE__);
    }
    
    
    
    const Domain& DomainManager::GetDomain(unsigned int unique_id,
                                           const char* invoking_file, int invoking_line) const
    {
        auto it = list_.find(unique_id);
        
        if (it == list_.cend())
            throw Exception("Domain " + std::to_string(unique_id) + " is not defined!",
                            invoking_file, invoking_line);
            
        assert(!(!(it->second)));
        
        return *(it->second);
    }
    
    
} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
