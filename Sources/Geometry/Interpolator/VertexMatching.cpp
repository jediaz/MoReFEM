/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 14 Dec 2015 11:09:00 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include <cassert>

#include "Utilities/Exceptions/Exception.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/String/String.hpp"

#include "Geometry/Interpolator/VertexMatching.hpp"


namespace MoReFEM
{
    
    
    namespace MeshNS
    {
        
        
        namespace InterpolationNS
        {
            
            
            void VertexMatching::Read(const std::string& filename)
            {
                std::ifstream in;
                FilesystemNS::File::Read(in, filename, __FILE__, __LINE__);
                
                std::string line;
                
                
                while (getline(in ,line))
                {
                    Utilities::String::Strip(line);
                    
                    // Ignore empty lines and comment lines.
                    if (line.empty())
                        continue;
                    
                    if (Utilities::String::StartsWith(line, "#"))
                        continue;
                    
                    // A valid line should include two integers, that are vertex indexes on each of the mesh.
                    std::istringstream iconv(line);
                    
                    unsigned int indexes_on_line[2];
                    
                    iconv >> indexes_on_line[0];
                    
                    if (iconv.fail())
                        throw Exception("Invalid line in interpolation file " + filename + ": \n" + line + "\n",
                                        __FILE__, __LINE__);
                    
                    iconv >> indexes_on_line[1];
                    
                    if (iconv.fail())
                        throw Exception("Invalid line in interpolation file " + filename + ": \n" + line + "\n",
                                        __FILE__, __LINE__);
                    
                    if (!iconv.eof())
                        throw Exception("Invalid line in interpolation file " + filename + ": \n" + line + "\n",
                                        __FILE__, __LINE__);
                    
                    # ifndef NDEBUG
                    {
                        assert(std::find(source_index_list_.cbegin(),
                                         source_index_list_.cend(),
                                         indexes_on_line[0]) == source_index_list_.cend());
                        
                        assert(std::find(target_index_list_.cbegin(),
                                         target_index_list_.cend(),
                                         indexes_on_line[1]) == target_index_list_.cend());
                    }
                    # endif // NDEBUG
                    
                    
                    source_index_list_.push_back(indexes_on_line[0]);
                    target_index_list_.push_back(indexes_on_line[1]);
                }
            }
            
            
            unsigned int VertexMatching::FindSourceIndex(unsigned int target_index) const
            {
                const auto& target_list = GetTargetIndexList();
                
                const auto target_begin = target_list.cbegin();
                const auto target_end = target_list.cend();
                
                const auto it = std::find(target_begin,
                                          target_end,
                                          target_index);
                
                assert(it != target_end && "Check your interpolation file (given in input parameter) is up-to-date... "
                       "It might also be that  your domains aren't well balanced (typically not the same "
                       "dimension...");
                
                const auto pos = static_cast<std::size_t>(it - target_begin);
                
                const auto& source_list = GetSourceIndexList();
                assert(source_list.size() == target_list.size() && "Key assumption of the class...");
                return source_list[pos];
            }
            
            
            
        } // namespace InterpolationNS
        
        
    } //  namespace MeshNS
    
    
} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
