/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 19 Mar 2014 12:29:26 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_REF_GEOM_ELT_HPP_
# define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_REF_GEOM_ELT_HPP_


# include <memory>
# include <vector>
# include <deque>

# include "Geometry/Coords/Coords.hpp"
# include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
# include "Geometry/RefGeometricElt/Internal/Topology/EnumTopology.hpp"


namespace MoReFEM
{

    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================

    class QuadratureRule;
    class LocalCoords;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================



    /*!
     * \brief Polymorphic class which can access static functions related to shape functions, interface and topology.
     *
     * This class is used to hold polymorphically the generic informations related to a geometric element (shape
     * functions, interface and topology).
     *
     * It can be used for instance to store in a small vector the list of all the kinds of GeometricElt
     * met in the mesh.
     *
     * The equivalent that also holds specific data (Coords involved for instance) is GeometricElt.
     *
     * \internal <b><tt>[internal]</tt></b> RefGeomElt pure virtual methods are all defined in derived class TRefGeomElt.
     * Currently only the static functions required by the current state of the code are implemented, but many
     * others could have been and aren't. It is not very difficult to do it when necessary: the principle
     * is always to call the traits class which defines the static function (take any existing one to
     * understand how it works).
     *
     */
    class RefGeomElt
    {
    public:

        //! Alias for shared pointer.
        using shared_ptr = std::shared_ptr<const RefGeomElt>;

        //! Alias for unique pointer.
        using const_unique_ptr = std::unique_ptr<const RefGeomElt>;

        //! Alias for vector of pointers
        using vector_shared_ptr = std::vector<shared_ptr>;


        /// \name Special members.
        ///@{

    protected:

        //! Default constructor.
        RefGeomElt() = default;

        //! Destructor.
        virtual ~RefGeomElt();

        //! \copydoc doxygen_hide_copy_constructor
        RefGeomElt(const RefGeomElt& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        RefGeomElt(RefGeomElt&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        RefGeomElt& operator=(const RefGeomElt& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        RefGeomElt& operator=(RefGeomElt&& rhs) = delete;

        ///@}


    public:

        /*!
         * \brief Get the identifier of the geometric element.
         *
         * \return The identifier of a GeometricElt as defined within MoReFEM (independant of IO format).
         */
        virtual Advanced::GeometricEltEnum GetIdentifier() const = 0;

        /*!
         * \brief Get the number of Coords object required to characterize completely a GeometricElt of this type.
         *
         * For instance 27 for an Hexahedron27.
         *
         * \return Number of Coords object required to characterize completely a GeometricElt of this type.
         */
        virtual unsigned int Ncoords() const = 0;

        //! Get the dimension of the geometric element.
        virtual unsigned int GetDimension() const = 0;

        /*!
         * \brief Get the name associated to the element (e.g. 'Triangle3').
         *
         * This name is guaranteed to be unique
         * \return Name associated to the element (e.g. 'Triangle3').
         */
        virtual const std::string& GetName() const = 0;

        //! Get the name associated to the Topology (e.g. 'Triangle').
        virtual const std::string& GetTopologyName() const = 0;

        //! Get the enum value associated to the Topology (e.g. 'RefGeomEltNS::TopologyNS::Type::tetrahedron').
        virtual RefGeomEltNS::TopologyNS::Type GetTopologyIdentifier() const = 0;

        //! Get the local coordinates of the barycenter.
        virtual const LocalCoords& GetBarycenter() const = 0;

        //! Get the list of local coordinates of the vertices.
        virtual const std::vector<LocalCoords>& GetVertexLocalCoordsList() const = 0;

        //! Return the number of vertices.
        virtual unsigned int Nvertex() const noexcept = 0;

        //! Return the number of edges.
        virtual unsigned int Nedge() const noexcept = 0;

        //! Return the number of faces.
        virtual unsigned int Nface() const noexcept = 0;

        //! Returns the nature of the interior interface.
        virtual InterfaceNS::Nature GetInteriorInterfaceNature() const noexcept = 0;


    public:

        /// \name Shape function methods.
        ///@{

        //! \copydoc doxygen_hide_shape_function
        virtual double ShapeFunction(unsigned int local_node_index, const LocalCoords& local_coords) const = 0;

        //! \copydoc doxygen_hide_first_derivate_shape_function
        virtual double FirstDerivateShapeFunction(unsigned int local_node_index,
                                                  unsigned int component,
                                                  const LocalCoords& local_coords) const = 0;

        //! \copydoc doxygen_hide_second_derivate_shape_function
        virtual double SecondDerivateShapeFunction(unsigned int local_node_index,
                                                   unsigned int component1,
                                                   unsigned int component2,
                                                   const LocalCoords& local_coords) const = 0;

        ///@}


    private:

        // THIS CLASS IS NOT INTENDED TO HOLD DATA MEMBERS; please read its description first if you want to...

    };


    /*!
     *
     * \copydoc doxygen_hide_operator_equal
     * The comparison is performed with underlying GeometricEltEnum.
     */
    bool operator==(const RefGeomElt& lhs, const RefGeomElt& rhs);


    /*!
     * \copydoc doxygen_hide_operator_less
     *
     * The comparison is performed with underlying GeometricEltEnum.
     *
     */
    bool operator<(const RefGeomElt& lhs, const RefGeomElt& rhs);




} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


# include "Geometry/RefGeometricElt/RefGeomElt.hxx"


#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_REF_GEOM_ELT_HPP_
