/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 21 Jan 2015 15:50:06 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_MO_RE_F_E_M_DATA_HPP_
# define MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_MO_RE_F_E_M_DATA_HPP_


# include "ThirdParty/Wrappers/Mpi/Mpi.hpp"
# include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"
# include "ThirdParty/Wrappers/Petsc/Print.hpp"
# include "ThirdParty/Wrappers/Petsc/Internal/RAII.hpp"
# include "ThirdParty/Wrappers/Tclap/StringPair.hpp"
# include "ThirdParty/IncludeWithoutWarning/Tclap/Tclap.hpp"

# include "Utilities/Filesystem/File.hpp"
# include "Utilities/InputData/Base.hpp"
# include "Utilities/TimeKeep/TimeKeep.hpp"
# include "Utilities/Environment/Environment.hpp"
# include "Utilities/OutputFormat/OutputFormat.hpp"
# include "Utilities/Datetime/Now.hpp"

# include "Core/InputData/Instances/Result.hpp"
# include "Core/InitTimeKeepLog.hpp"


namespace MoReFEM
{


    /// \addtogroup CoreGroup
    ///@{


    //! Empty shell when no additional command line argument are required in the model for which \a MoReFEMData is built.
    struct NoAdditionalCommandLineArgument
    { };



    /*!
     * \brief Init MoReFEM: initialize mpi and read the input data file.
     *
     * \warning As mpi is not assumed to exist until the constructor has done is job, the exceptions there that might
     * happen only on some of the ranks don't lead to a call to MPI_Abort(), which can lead to a dangling program.
     * Make sure each exception is properly communicated to all ranks so that each rank can gracefully throw
     * an exception and hence allow the program to stop properly.
     *
     * \tparam AdditionalCommandLineArgumentsPolicyT Policy if you need additional arguments on the command line.
     * To see a concrete example of this possibility, have a look at Test/Core/MoReFEMData/test_command_line_options.cpp
     * which demonstrate the possibility.
     *
     * http://tclap.sourceforge.net gives a nice manual of how to add additional argument on the command lines.
     * By default, there is one mandatory argument (--input_data *lua file*) and one optional that might be
     * repeated to define pseudo environment variables (-e KEY=VALUE).
     */
    template
    <
        class InputDataT,
        Utilities::InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT = Utilities::InputDataNS::DoTrackUnusedFields::yes,
        class AdditionalCommandLineArgumentsPolicyT = NoAdditionalCommandLineArgument
    >
    class MoReFEMData : public AdditionalCommandLineArgumentsPolicyT
    {

    public:

        //! Alias to self.
        using self = MoReFEMData<InputDataT, DoTrackUnusedFieldsT, AdditionalCommandLineArgumentsPolicyT>;

        //! Alias to unique_ptr.
        using const_unique_ptr = std::unique_ptr<self>;

        //! Alias to \a InputDataT.
        using input_data_type = InputDataT;

        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] argc Number of argument in the command line (including the program name).
         * \param[in] argv List of arguments read.
         */
        explicit MoReFEMData(int argc, char** argv);

        //! Destructor.
        ~MoReFEMData();

        //! \copydoc doxygen_hide_copy_constructor
        MoReFEMData(const MoReFEMData& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        MoReFEMData(MoReFEMData&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        MoReFEMData& operator=(const MoReFEMData& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        MoReFEMData& operator=(MoReFEMData&& rhs) = delete;

        ///@}

        //! Accessor to underlying mpi object.
        const Wrappers::Mpi& GetMpi() const noexcept;

        //! Accessor to underlying InputData object.
        const InputDataT& GetInputData() const noexcept;

        /*!
         * \brief Accessor to the result directory, in which all the outputs of MoReFEM should be written.
         *
         * \tparam MpiScaleT If MpiScale::processor-wise, return the subdirectory which also specifies the rank.
         * If MpiScale::program_wise, returns the
         * result directory. The latter is expected to be used mostly on the root processor.
         */
        template<MpiScale MpiScaleT>
        const std::string& GetResultDirectory() const noexcept;

        //!

    private:

        //! Holds InputData.
        typename InputDataT::const_unique_ptr input_data_ = nullptr;

        //! Directory into which model results will be written (with a folder hierarchy)
        std::string result_directory_;

        //! Subdirectory of \a result_directory_ in which informations related to current rank are written.
        std::string result_directory_with_rank_;


    };


    ///@} // \addtogroup


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


# include "Core/MoReFEMData/MoReFEMData.hxx"


#endif // MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_MO_RE_F_E_M_DATA_HPP_
