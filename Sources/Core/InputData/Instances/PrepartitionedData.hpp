/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 27 Aug 2013 11:26:39 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PREPARTITIONED_DATA_HPP_
# define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PREPARTITIONED_DATA_HPP_


# include <string>

# include "Core/InputData/Instances/Crtp/Section.hpp"


namespace MoReFEM::InputDataNS
{


    //! \copydoc doxygen_hide_core_input_data_section
    struct PrepartitionedData : public Crtp::Section<PrepartitionedData, NoEnclosingSection>
    {


        //! Return the name of the section in the input parameter.
        static const std::string& GetName();


        //! Convenient alias.
        using self = PrepartitionedData;



        //! Friendship to section parent.
        using parent = Crtp::Section<self, NoEnclosingSection>;

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        //! Directory in which preprocessed data are stored.
        struct Directory : public Crtp::InputData<Directory, self, std::string>
        {


            //! Name of the input parameter in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input parameter.
            static const std::string& Description();

            /*!
             * \return Constraint to fulfill.
             *
             * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some text from \a LuaOptionFile example file:
             *
             * An age should be greater than 0 and less than, say, 150. It is possible
             * to check it with a logical expression (written in Lua). The expression
             * should be written with 'v' being the variable to be checked.
             * \a constraint = "v >= 0 and v < 150"
             *
             * It is possible to check whether a variable is in a set of acceptable
             * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
             * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
             *
             * If a vector is retrieved, the constraint must be satisfied on every
             * element of the vector.
             */
            static const std::string& Constraint();


            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no value has been given
             * in the input data file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be \a LuaOptionFile-formatted.
             */
            static const std::string& DefaultValue();
        };


        //! Whether we intend to run a model or just pre-process it for later parallel use.
        struct IsPrepartitionRun : public Crtp::InputData<IsPrepartitionRun, self, bool>
        {


            //! Name of the input parameter in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input parameter.
            static const std::string& Description();

            /*!
             * \return Constraint to fulfill.
             *
             * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some text from \a LuaOptionFile example file:
             *
             * An age should be greater than 0 and less than, say, 150. It is possible
             * to check it with a logical expression (written in Lua). The expression
             * should be written with 'v' being the variable to be checked.
             * \a constraint = "v >= 0 and v < 150"
             *
             * It is possible to check whether a variable is in a set of acceptable
             * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
             * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
             *
             * If a vector is retrieved, the constraint must be satisfied on every
             * element of the vector.
             */
            static const std::string& Constraint();


            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no value has been given
             * in the input data file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be \a LuaOptionFile-formatted.
             */
            static const std::string& DefaultValue();
        };



        //! Alias to the tuple of structs.
        using section_content_type = std::tuple
        <
            Directory,
            IsPrepartitionRun
        >;


    private:

        //! Content of the section.
        section_content_type section_content_;


    }; // struct PrepartitionedData


} // namespace MoReFEM::InputDataNS


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PREPARTITIONED_DATA_HPP_
