/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 24 Mar 2015 11:47:43 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_DIRICHLET_BOUNDARY_CONDITION_x_DIRICHLET_BOUNDARY_CONDITION_HPP_
# define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_DIRICHLET_BOUNDARY_CONDITION_x_DIRICHLET_BOUNDARY_CONDITION_HPP_

# include "Core/InputData/Instances/Crtp/Section.hpp"

# include "Core/InputData/Instances/Geometry/Domain.hpp"
# include "Core/InputData/Instances/DirichletBoundaryCondition/Impl/DirichletBoundaryCondition.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {


        namespace BaseNS
        {


            /*!
             * \brief Common base class from which all InputDataNS::DirichletBoundaryCondition should inherit.
             *
             * This brief class is used to tag domains within the input parameter data (through std::is_base_of<>).
             */
            struct DirichletBoundaryCondition
            { };


        } // namespace BaseNS



        //! \copydoc doxygen_hide_core_input_data_section_with_index
        template<unsigned int IndexT>
        struct DirichletBoundaryCondition
        : public BaseNS::DirichletBoundaryCondition,
        public Crtp::Section<DirichletBoundaryCondition<IndexT>, NoEnclosingSection>
        {

            /*!
             * \brief Return the name of the section in the input parameter list.
             *
             * e.g. 'DirichletBoundaryCondition' for IndexT = 1.
             *
             * \return Name of the section in the input parameter list.
             */
            static const std::string& GetName();

            //! Return the unique id (i.e. 'IndexT').
            static constexpr unsigned int GetUniqueId() noexcept;


            //! Convenient alias.
            using self = DirichletBoundaryCondition<IndexT>;

            //! Friendship to section parent.
            using parent = Crtp::Section<self, NoEnclosingSection>;


            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN


            //! Name of the bc.
            struct Name : public Crtp::InputData<Name, self, std::string>,
            public Impl::DirichletBoundaryConditionNS::Name
            { };

            //! Indicates the domain upon which the finite element space is defined. This domain must be uni-dimensional.
            struct Component : public Crtp::InputData<Component, self, std::string>,
            public Impl::DirichletBoundaryConditionNS::Component
            { };


            //! Indicates which unknowns are defined on the finite element space.
            struct UnknownName : public Crtp::InputData<UnknownName, self, std::string>,
            public Impl::DirichletBoundaryConditionNS::UnknownName
            { };


            //! Indicates for each unknowns the shape function to use.
            struct Values : public Crtp::InputData<Values, self, std::vector<double>>,
            public Impl::DirichletBoundaryConditionNS::Values
            { };


            //! Indicates the numbering subset to use for each unknown.
            struct DomainIndex: public Crtp::InputData<DomainIndex, self, unsigned int>,
            public Impl::DirichletBoundaryConditionNS::DomainIndex
            { };


            //! Indicates whether the boundary condition values might change at each time iteration,
            struct IsMutable: public Crtp::InputData<IsMutable, self, bool>,
            public Impl::DirichletBoundaryConditionNS::IsMutable
            { };


            //! Indicates whether the boundary condition might overlap with another.
            struct MayOverlap: public Crtp::InputData<MayOverlap, self, bool>,
            public Impl::DirichletBoundaryConditionNS::MayOverlap
            { };


            //! Alias to the tuple of structs.
            using section_content_type = std::tuple
            <
                Name,
                Component,
                UnknownName,
                Values,
                DomainIndex,
                IsMutable,
                MayOverlap
            >;


        private:

            //! Content of the section.
            section_content_type section_content_;


        }; //struct DirichletBoundaryCondition


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


# include "Core/InputData/Instances/DirichletBoundaryCondition/DirichletBoundaryCondition.hxx"


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_DIRICHLET_BOUNDARY_CONDITION_x_DIRICHLET_BOUNDARY_CONDITION_HPP_
