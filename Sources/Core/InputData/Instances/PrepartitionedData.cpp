/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 19 Feb 2015 14:38:32 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#include "Utilities/String/EmptyString.hpp"

#include "Core/InputData/Instances/PrepartitionedData.hpp"


namespace MoReFEM
{
    
    
    namespace InputDataNS
    {
        
        
        const std::string& PrepartitionedData::GetName()
        {
            static std::string ret("PrepartitionedData");
            return ret;
        }
        
            
        const std::string& PrepartitionedData::Directory::NameInFile()
        {
            static std::string ret("Directory");
            return ret;
        }
        
        
        const std::string& PrepartitionedData::Directory::Description()
        {
            static std::string ret("Directory in which prepartitioned data are stored. Leave it empty if irrelevant.");
            return ret;
        }
        
        
        const std::string& PrepartitionedData::Directory::Constraint()
        {
            return Utilities::EmptyString();
        }
        
        
        const std::string& PrepartitionedData::Directory::DefaultValue()
        {
            return Utilities::EmptyString();
        }

        
        const std::string& PrepartitionedData::IsPrepartitionRun::NameInFile()
        {
            static std::string ret("is_prepartition_run");
            return ret;
        }
        
        
        const std::string& PrepartitionedData::IsPrepartitionRun::Description()
        {
            static std::string ret("If True, the program runs to generate prepartition data and will stop once it's "
                                   "done. If False, mode will be run with pre-computed prepartitioned data (if "
                                   "directory is not empty) or will not use them (and compute them on the fly if "
                                   "parallel mode).");
            return ret;
        }


        const std::string& PrepartitionedData::IsPrepartitionRun::Constraint()
        {
            return Utilities::EmptyString();
        }
        
        
        
        const std::string& PrepartitionedData::IsPrepartitionRun::DefaultValue()
        {
            static std::string ret("False");
            return ret;
        }
        
        
    } // namespace InputDataNS
    
    
} // namespace MoReFEM


/// @} // addtogroup CoreGroup


