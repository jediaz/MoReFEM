/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 4 Apr 2016 23:42:27 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_INTERNAL_x_COMPONENT_x_COMPONENT_MANAGER_HXX_
# define MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_INTERNAL_x_COMPONENT_x_COMPONENT_MANAGER_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace BoundaryConditionNS
        {


            inline const std::string& ComponentManager::Name() const
            {
                return name_;
            }


            inline bool ComponentManager::IsActive(unsigned int i) const
            {
                assert(i < is_activated_.size());
                return is_activated_[i];
            }


            inline unsigned int ComponentManager::Nactive() const
            {
                return static_cast<unsigned int>(is_activated_.count());
            }


        } // namespace BoundaryConditionNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_INTERNAL_x_COMPONENT_x_COMPONENT_MANAGER_HXX_
