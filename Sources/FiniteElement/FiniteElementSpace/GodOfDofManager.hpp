/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 30 Mar 2015 11:30:31 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_GOD_OF_DOF_MANAGER_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_GOD_OF_DOF_MANAGER_HPP_

# include "Utilities/Singleton/Singleton.hpp"

# include "Geometry/Mesh/Internal/MeshManager.hpp"

# include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"


namespace MoReFEM
{


    /// \addtogroup FiniteElementGroup
    ///@{


    /*!
     * \brief This class is used to create and retrieve GodOfDof objects.
     *
     * GodOfDof objects get private constructor and can only be created through this class. In addition
     * to their creation, this class keeps their address, so it's possible from instance to retrieve a
     * GodOfDof object given its unique id (which is the one that appears in the input data file).
     *
     */
    class GodOfDofManager : public Utilities::Singleton<GodOfDofManager>
    {

    public:

        /*!
         * \brief Returns the name of the class (required for some Singleton-related errors).
         *
         * \return Name of the class.
         */
        static const std::string& ClassName();


        /*!
         * \brief Base type of Mesh as input parameter (requested to identify domains in the input parameter data).
         *
         * Mesh is not an error: there is currently exactly one god of dof created for each mesh.
         */
        using input_data_type = InputDataNS::BaseNS::Mesh;

    public:


        /*!
         * \brief Create a new GodOfDof object from the data of the input data file.
         *
         * Mesh is not a typo here: there is one god of dof per mesh and so GodOfDofs are created by tracking mesh
         * sections in the input parameter data.
         *
         * \tparam MeshSectionT Type of \a section.
         *
         * \param[in] section Section in the input data file which includes all relevant data required to build
         * the mesh.
         * \copydetails doxygen_hide_mpi_param
         *
         */
        template<class MeshSectionT>
        void Create(const MeshSectionT& section,
                    const Wrappers::Mpi& mpi);

        //! Fetch the god of dof object associated with \a unique_id unique identifier.
        //! \unique_id_param_in_accessor{GodOfDof}
        const GodOfDof& GetGodOfDof(unsigned int unique_id) const;

        //! Fetch the god of dof object associated with \a unique_id unique identifier.
        //! \unique_id_param_in_accessor{GodOfDof}
        GodOfDof& GetNonCstGodOfDof(unsigned int unique_id);

        //! Fetch the god of dof object associated with \a unique_id unique identifier.
        //! \unique_id_param_in_accessor{GodOfDof}
        GodOfDof::shared_ptr GetGodOfDofPtr(unsigned int unique_id) const;

        //! Access to the storage.
        const auto& GetStorage() const noexcept;


    private:


        //! \name Singleton requirements.
        ///@{

        //! Constructor.
        GodOfDofManager();

        //! Destructor.
        virtual ~GodOfDofManager() override;

        //! Friendship declaration to Singleton template class (to enable call to constructor).
        friend class Utilities::Singleton<GodOfDofManager>;
        ///@}


    private:

        //! Store the god of dof objects by their unique identifier.
        std::unordered_map<unsigned int, GodOfDof::shared_ptr> list_;

    };


    //! Clear temporary data for each god of dof.
    void ClearGodOfDofTemporaryData();


    ///@} // \addtogroup


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


# include "FiniteElement/FiniteElementSpace/GodOfDofManager.hxx"


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_GOD_OF_DOF_MANAGER_HPP_
