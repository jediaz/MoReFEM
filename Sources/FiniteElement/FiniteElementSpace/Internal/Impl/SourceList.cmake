### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_FELT}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/AttributeProcessorHelper.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/FEltSpaceInternalStorage.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/InterfaceSpecialization.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/AttributeProcessorHelper.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/AttributeProcessorHelper.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/ComputeDofIndexes.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/ComputeDofIndexes.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/FEltSpaceInternalStorage.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/FEltSpaceInternalStorage.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/InterfaceSpecialization.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/InterfaceSpecialization.hxx"
)

