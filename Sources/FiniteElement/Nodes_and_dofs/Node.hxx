/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 9 Apr 2014 15:38:05 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_NODE_HXX_
# define MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_NODE_HXX_


namespace MoReFEM
{


    inline const Unknown& Node::GetUnknown() const noexcept
    {
        return unknown_;
    }


    inline const Dof::vector_shared_ptr& Node::GetDofList() const noexcept
    {
        return dof_list_;
    }


    inline const Dof::shared_ptr& Node::GetDofPtr(unsigned int i) const
    {
        const auto& dof_list = GetDofList();
        assert(i < static_cast<unsigned int>(dof_list.size()));
        assert(!(!dof_list[i]));
        return dof_list[i];
    }


    inline const Dof& Node::GetDof(unsigned int i) const
    {
        return *GetDofPtr(i);
    }


    inline const std::string& Node::GetShapeFunctionLabel() const noexcept
    {
        return shape_function_label_;
    }


    inline unsigned int Node::Ndof() const noexcept
    {
        return static_cast<unsigned int>(dof_list_.size());
    }


    inline const NumberingSubset::vector_const_shared_ptr& Node::GetNumberingSubsetList() const noexcept
    {
        return numbering_subset_list_;
    }



} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_NODE_HXX_
