/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 6 Apr 2016 14:47:48 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_ADVANCED_x_REF_F_ELT_IN_LOCAL_OPERATOR_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_ADVANCED_x_REF_F_ELT_IN_LOCAL_OPERATOR_HPP_

# include <memory>
# include <vector>

# include "ThirdParty/IncludeWithoutWarning/Seldon/Seldon.hpp"

# include "Utilities/LinearAlgebra/Storage/Local/LocalMatrixStorage.hpp"
# include "Utilities/LinearAlgebra/Storage/Local/LocalVectorStorage.hpp"

# include "FiniteElement/RefFiniteElement/Internal/RefFEltInFEltSpace.hpp"



namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    namespace Internal
    {


        namespace LocalVariationalOperatorNS
        {


            class ElementaryDataImpl;


        } // namespace LocalVariationalOperatorNS


    } // namespace Internal


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace Advanced
    {


        /// \addtogroup FiniteElementGroup
        ///@{


        /*!
         * \brief Reference finite element a standard developer should use.
         *
         * \internal <b><tt>[internal]</tt></b> There are lower-level classes in Internal::RefFEltNS namespace that should not be used directly;
         * current class is circumscribed to one operator and should provide all relevant informations a developer might
         * need when implementing a new operator. The only exception is if a new type of reference finite element must be
         * defined; in this case a child class of Internal::RefEltNS::BasicRefFElt should be created (see this class for much more
         * details about how to do it).
         *
         */
        class RefFEltInLocalOperator final
        : public ::MoReFEM::Crtp::LocalMatrixStorage<RefFEltInLocalOperator, 1u>,
        public ::MoReFEM::Crtp::LocalVectorStorage<RefFEltInLocalOperator, 1u>
        {

        public:

            //! Alias to unique pointer.
            using const_unique_ptr = std::unique_ptr<const RefFEltInLocalOperator>;

            //! Alias to vector of unique pointers.
            using vector_const_unique_ptr = std::vector<const_unique_ptr>;

            //! Friendship to ElementaryDataImpl, which should be the only class able to instantiate RefFEltInLocalOperator.
            friend class Internal::LocalVariationalOperatorNS::ElementaryDataImpl;

        public:

            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \param[in] ref_felt The RefFEltInFEltSpace object issued by the FEltSpace which will be embedded
             * in current class.
             * \param[in] index_first_node_in_elementary_data Index of the first node related to the finite
             * element type in the elementary matrices.
             * \param[in] index_first_dof_in_elementary_data Same as \a index_first_node_in_elementary_data for the dof
             * indexes.
             *
             * \internal <b><tt>[internal]</tt></b> Usually I am weary of such low-level constructor arguments, but in
             * this case it is fine: the user should never have to call this constructor... All the required instances
             * of present class are built in ElementaryDataImpl constructor, which is itself in Internal namespace.
             */
            explicit RefFEltInLocalOperator(const Internal::RefFEltNS::RefFEltInFEltSpace& ref_felt,
                                            unsigned int index_first_node_in_elementary_data,
                                            unsigned int index_first_dof_in_elementary_data);

            //! Destructor.
            ~RefFEltInLocalOperator() = default;

            //! \copydoc doxygen_hide_copy_constructor
            RefFEltInLocalOperator(const RefFEltInLocalOperator& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            RefFEltInLocalOperator(RefFEltInLocalOperator&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            RefFEltInLocalOperator& operator=(const RefFEltInLocalOperator& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            RefFEltInLocalOperator& operator=(RefFEltInLocalOperator&& rhs) = delete;


            ///@}

            //! Number of nodes.
            unsigned int Nnode() const noexcept;

            //! Number of dofs.
            unsigned int Ndof() const noexcept;

            //! Return the related reference finite element.
            const Internal::RefFEltNS::BasicRefFElt& GetBasicRefFElt() const noexcept;

            //! Return the related unknown/numbering subset.
            const ExtendedUnknown& GetExtendedUnknown() const noexcept;


        public:

            /*!
             * \brief Return the (contiguous) list of node indexes related to RefFEltInFEltSpace in ElementaryData matrices
             * or vectors.
             *
             * Please treat this method as private and not use it: no class should required this save ElementaryDataImpl
             * and the few free-functions that are used to build it.
             *
             * \return Reference to the list of local node indexes.
             */
            const Seldon::Vector<int>& GetLocalNodeIndexList() const noexcept;

            /*!
             * \brief Return the (contiguous) list of dof indexes related to RefFEltInFEltSpace in ElementaryData matrices
             * or vectors; this method returns only a subset related to a given component of the unknown.
             *
             * Please treat this method as private and not use it: no class should required this save ElementaryDataImpl
             * and the few free-functions that are used to build it.
             *
             * \param[in] component_index Index of the component for which the list is sought.
             *
             * \return Reference to the list of local node indexes associated to \a component_index -th component.
             */
            const Seldon::Vector<int>& GetLocalDofIndexList(unsigned int component_index) const noexcept;


            /*!
             * \brief Return the (contiguous) list of dof indexes related to RefFEltInFEltSpace in ElementaryData matrices or vectors.
             *
             * Please treat this method as private and not use it: no class should required this save ElementaryDataImpl
             * and the few free-functions that are used to build it.
             *
             * \return Reference to the list of local dof indexes.
             */
            const Seldon::Vector<int>& GetLocalDofIndexList() const noexcept;

            //! Returns the number of components.
            unsigned int Ncomponent() const noexcept;

            //! Returns the index of the first dof of current ref elt in elementary data.
            unsigned int GetIndexFirstDofInElementaryData() const noexcept;

            //! Returns the index of the first dof of current ref elt for \a component in elementary data.
            //! \param[in] component Component used as filter.
            unsigned int GetIndexFirstDofInElementaryData(unsigned int component) const noexcept;

            //! Access to the dimension of the mesg in which current finite element is built.
            unsigned int GetMeshDimension() const noexcept;

            //! Access to the dimension of the finite element space for which current reference finite element is built.
            unsigned int GetFEltSpaceDimension() const noexcept;



    //    private:
        public:


            //! Return the underlying RefFEltInFEltSpace, as it is stored in the FEltSpace (see class banner for more details).
            const Internal::RefFEltNS::RefFEltInFEltSpace& GetUnderlyingRefFElt() const noexcept;

            //! Returns the number of dofs per component.
            unsigned int NdofPerComponent() const noexcept;


        private:

            //! Underlying RefFEltInFEltSpace, as it is stored in the FEltSpace (see class banner for more details).
            const Internal::RefFEltNS::RefFEltInFEltSpace& ref_felt_;

            //! Index of the first dof of the ref finite element in elementary data.
            const unsigned int index_first_dof_;

            //! Number of dofs per component.
            const unsigned int Ndof_per_component_;

            /*!
             * \brief (Contiguous) list of node indexes related to RefFEltInFEltSpace in ElementaryData matrices
             * or vectors; this method returns only a subset related to a given component of the unknown.
             */
            std::vector<Seldon::Vector<int>> local_dof_indexes_per_component_;

            //! (Contiguous) list of dof indexes related to RefFEltInFEltSpace in ElementaryData matrices or vectors.
            Seldon::Vector<int> local_dof_indexes_;

            /*!
             * \brief Return the (contiguous) list of node indexes related to RefFEltInFEltSpace in ElementaryData matrices
             * or vectors; this method returns only a subset related to a given component of the unknown.
             */
            Seldon::Vector<int> local_node_indexes_;

        };


        /*!
         * \brief Extract the part of \a full_matrix that is related to \a ref_felt.
         *
         * \internal <b><tt>[internal]</tt></b> The reference points to a mutable data attribute of
         * RefFEltInLocalOperator.
         *
         * \param[in] full_matrix The matrix from which a block is extracted.
         * \param[in,out] ref_felt Reference finite element considered. The computed matrix is stored
         * in a mutable attribute of this object.
         *
         * \return Reference to the extracted sub-matrix.
         */
        const LocalMatrix& ExtractSubMatrix(const LocalMatrix& full_matrix,
                                            const RefFEltInLocalOperator& ref_felt);

        /*!
         * \brief Extract the part of \a full_vector that is related to \a ref_felt.
         *
         * \internal <b><tt>[internal]</tt></b> The reference points to a mutable data attribute of
         * RefFEltInLocalOperator.
         *
         * \param[in] full_vector The vector from which a block is extracted.
         * \param[in,out] ref_felt Reference finite element considered. The computed vector is stored
         * in a mutable attribute of this object.
         *
         * \return Reference to the extracted sub-vector.
         */
        const LocalVector& ExtractSubVector(const LocalVector& full_vector,
                                            const RefFEltInLocalOperator& ref_felt);


        ///@} // \addtogroup


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


# include "FiniteElement/RefFiniteElement/Advanced/RefFEltInLocalOperator.hxx"


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_ADVANCED_x_REF_F_ELT_IN_LOCAL_OPERATOR_HPP_
