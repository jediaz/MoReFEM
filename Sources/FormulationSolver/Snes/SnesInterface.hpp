/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 18 Oct 2016 14:26:05 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FormulationSolverGroup
// \addtogroup FormulationSolverGroup
// \{
*/


#ifndef MOREFEM_x_FORMULATION_SOLVER_x_SNES_x_SNES_INTERFACE_HPP_
# define MOREFEM_x_FORMULATION_SOLVER_x_SNES_x_SNES_INTERFACE_HPP_

# include "ThirdParty/Wrappers/Petsc/Print.hpp"
# include "ThirdParty/Wrappers/Petsc/Solver/Snes.hpp"
# include "ThirdParty/Wrappers/Petsc/SnesMacro.hpp"


namespace MoReFEM
{


    namespace SolverNS
    {


        /*!
         * \brief Class that includes the most possible generic functions required by SNES.
         *
         * \tparam VariationalFormulationT Your \a VariationalFormulation in which a SolveNonLinear() call is issued.
         *
         * \attention A friendship should be written in \a VariationalFormulationT toward current class:
         * \code
         * friend struct SolverNS::SnesInterface<self>;
         * \endcode
         * where self is the internal alias toward the \a VariationalFormulationT.
         *
         * You may as well design your own functions if you need, but the ones here make do for all \a Model written
         * so far.
         * They require that \a VariationalFormulationT defines the following method:
         * - const GlobalVector& GetSnesEvaluationState() const noexcept
         *     which points to the current estimation of the solution (not the delta, the solution itself). Is it NOT
         *     VariationalFormulation::GetSystemSolution(...), which contains the current delta (it contains however
         *     the actual solution at the end. Don't shoot at me: it's directly Petsc design).
         * - void SnesUpdate(const Vec& evaluation_state)
         *     which updates the current estimation from the internal value of Petsc Snes. Typically it should contain
         * at least:
         * \code
         * void SnesUpdate(const Vec& evaluation_state)
         * {
         *      GetNonCstSnesEvaluationState().SetFromPetscVec(evaluation_state);
         * }
         * \endcode
         * or a similar line (see Poromechanics for a more complex line due to lazy evaluation used there).
         * You may as well update other quantities relevant for the resolution in this function.
         * - void ComputeResidual()
         *      which must includes the computation of the residual.(result to store in
         *      VariationalFormulationT::GetNonCstSystemRhs()). If possible keep computation of residual and
         *      tangent separated, ven if it is not that easy in some models.
         * - void ComputeTangent()
         *      which must include the computation of the tangent (result to store in
         *      VariationalFormulationT::GetNonCstSystemMatrix()).
         *
         */
        template<class VariationalFormulationT>
        struct SnesInterface
        {


         
            /*!
             * \brief This function is given to SNES to compute the function.
             *
             * \copydoc doxygen_hide_snes_interface_common_arg
             * \param[in,out] evaluation_state Most recent value of the quantity the solver tries to compute.
             * \param[in,out] residual Petsc vector that include residual. It is not used per se in MoReFEM; there is
             * however a check in debug that VariationalFormulationT::GetSystemRhs() contains the same data as this
             * parameter.
             *

             *
             */
            static PetscErrorCode Function(SNES snes, Vec evaluation_state, Vec residual, void* context_as_void);


            /*!
             * \brief This function is supposed to be given to SNES to compute the jacobian.
             *
             * \copydoc doxygen_hide_snes_interface_common_arg
             * \param[in,out] evaluation_state Most recent value of the quantity the solver tries to compute.
             * \param[in,out] jacobian Jacobian matrix. Actually unused in our wrapper.
             * \param[in,out] preconditioner Preconditioner matrix. Actually unused in our wrapper.
             */
            static PetscErrorCode Jacobian(SNES snes, Vec evaluation_state, Mat jacobian , Mat preconditioner,
                                           void* context_as_void);


            /*!
             * \brief This function is used to monitor evolution of the convergence.
             *
             * Is is intended to be passed to SNESMonitorSet()
             *
             * \copydoc doxygen_hide_snes_interface_common_arg
             * \param[in] its Index of iteration.
             * \param[in] norm Current L^2 norm.
             */
            static PetscErrorCode Viewer(SNES snes, PetscInt its, PetscReal norm, void* context_as_void);



        };


    } // namespace SolverNS


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup


# include "FormulationSolver/Snes/SnesInterface.hxx"


#endif // MOREFEM_x_FORMULATION_SOLVER_x_SNES_x_SNES_INTERFACE_HPP_
