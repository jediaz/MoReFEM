/*!
 */


#ifndef MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_HXX_
# define MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_HXX_


namespace MoReFEM
{


    namespace Utilities
    {

        template<class PrintPolicyT>
        template
        <
            class ContainerT,
            typename StreamT,
            typename StringT1,
            typename StringT2,
            typename StringT3
        >
        void PrintContainer<PrintPolicyT>::Do(const ContainerT& container, StreamT& stream,
                                              StringT1&& separator,
                                              StringT2&& opener,
                                              StringT3&& closer)
        {
            stream << opener;

            Internal::PrintNS::SeparatorFacility<PrintPolicyT, StringT1> separator_facility(stream, separator);

            for (const auto& element : container)
                separator_facility << element;

            stream << closer;
        }


        template<class PrintPolicyT>
        template
        <
            std::size_t N,
            class ContainerT,
            typename StreamT,
            typename StringT1,
            typename StringT2,
            typename StringT3
        >
        void PrintContainer<PrintPolicyT>::Nelt(const ContainerT& container, StreamT& stream,
                                                StringT1&& separator, StringT2&& opener, StringT3&& closer)
        {
            stream << opener;

            Internal::PrintNS::SeparatorFacility<PrintPolicyT, StringT1> separator_facility(stream, separator);

            auto it = container.cbegin();
            auto end = it;
            std::advance(end,
                         static_cast<typename ContainerT::difference_type>(std::min(N, container.size())));

            for (; it != end; ++it)
                separator_facility << *it;

            stream << closer;
        }


        template
        <
            class TupleT,
            typename StreamT,
            typename StringT1,
            typename StringT2,
            typename StringT3
        >
        void PrintTuple(const TupleT& tuple,
                        StreamT& stream,
                        StringT1&& separator,
                        StringT2&& opener,
                        StringT3&& closer)
        {
            stream << opener;
            enum { size = std::tuple_size<TupleT>::value };
            Internal::PrintNS::PrintTupleHelper<StreamT, 0, size, TupleT>::Print(stream, tuple, separator);
            stream << closer;
        }


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup



#endif // MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_HXX_
