//! \file
//
//
//  Pointer.hxx
//  MoReFEM
//
//  Created by sebastien on 24/07/2019.
//Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_POLICY_x_POINTER_HXX_
# define MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_POLICY_x_POINTER_HXX_


namespace MoReFEM::Utilities::PrintPolicyNS
{


    template<class ElementTypeT>
    void Pointer::Do(std::ostream& stream, ElementTypeT&& ptr)
    {
        using type = std::remove_cv_t<std::remove_reference_t<ElementTypeT>>;

        static_assert(IsSharedPtr<type>() || std::is_pointer<type>() || IsUniquePtr<type>(),
                      "ptr must behaves like a pointer!");
        stream << *ptr;
    }


} // namespace MoReFEM::Utilities::PrintPolicyNS


#endif // MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_POLICY_x_POINTER_HXX_
