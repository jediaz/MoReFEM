/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 30 Aug 2013 11:06:17 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_LUA_FUNCTION_HXX_
# define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_LUA_FUNCTION_HXX_


namespace MoReFEM
{


    namespace Utilities
    {


        namespace InputDataNS
        {


            template<typename ReturnTypeT, typename ...Args>
            LuaFunction<ReturnTypeT(Args...)>::LuaFunction()
            : state_(luaL_newstate())
            { }


            template<typename ReturnTypeT, typename ...Args>
            LuaFunction<ReturnTypeT(Args...)>::LuaFunction(const std::string& content)
            : state_(luaL_newstate()),
            content_(content)
            {
                luaL_openlibs(state_);

                std::ostringstream oconv;
                oconv << "f = " << content; // function is arbitrarily called f.

                if (luaL_dostring(state_, oconv.str().c_str()))
                    throw Exception("The string with the definition of the function couldn't be interpreted correctly; "
                                    "it was: \n" + content + "\n", __FILE__, __LINE__);
            }


            template<typename ReturnTypeT, typename ...Args>
            LuaFunction<ReturnTypeT(Args...)>::~LuaFunction()
            {
//                lua_close(state_);
//                state_ = nullptr;
            }


            template<typename ReturnTypeT, typename ...Args>
            inline ReturnTypeT LuaFunction<ReturnTypeT(Args...)>::operator()(Args... args) const
            {
                assert(!(!state_));

                Internal::LuaNS::PutOnStack(state_, "f");

                Internal::LuaNS::PushOnStack(state_, std::forward_as_tuple(args...));

              //  int n = lua_gettop(state_);

                const auto size = static_cast<int>(sizeof...(args));

                if (lua_pcall(state_, size, LUA_MULTRET, 0) != 0)
                {
                    std::ostringstream oconv;
                    oconv << "Failure while trying to compute the function with Lua. FYI the Lua stack was at the time "
                    "of the call: \n";
                    Internal::LuaNS::LuaStackDump(state_, oconv);

                    throw Exception(oconv.str(), __FILE__, __LINE__);
                }

                /* retrieve result */
                if (!lua_isnumber(state_, -1))
                    throw Exception("Function `f' must return a number!", __FILE__, __LINE__);

                ReturnTypeT ret = Internal::LuaNS::PullFromStack<ReturnTypeT>(state_, -1);

                lua_pop(state_, 1);

                return ret;
            }


            template<typename ReturnTypeT, typename ...Args>
            inline const std::string& LuaFunction<ReturnTypeT(Args...)>::GetString() const noexcept
            {
                return content_;
            }

            
        } // namespace InputDataNS


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup



#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_LUA_FUNCTION_HXX_
