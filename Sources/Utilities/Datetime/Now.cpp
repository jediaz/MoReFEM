//! \file 
//
//
//  Now.cpp
//  MoReFEM
//
//  Created by sebastien on 17/07/2019.
//Copyright © 2019 Inria. All rights reserved.
//

#include <ctime>

#include "Utilities/Datetime/Now.hpp"
#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"


namespace MoReFEM::Utilities
{


    std::string Now(const Wrappers::Mpi& mpi)
    {
        struct tm * timeinfo;
        char buffer[20];

        std::vector<time_t> rawtime(1);

        if (mpi.IsRootProcessor())
            time(rawtime.data());

        mpi.Broadcast(rawtime);

        timeinfo = localtime(rawtime.data());

        strftime(buffer, 20, "%Y-%m-%d_%H:%M:%S", timeinfo);

        return std::string(buffer);
    }


} // namespace MoReFEM::Utilities
