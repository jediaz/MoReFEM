/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 10 Apr 2018 17:53:23 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#define BOOST_TEST_MODULE model_heat
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/Filesystem/File.hpp"

#include "ModelInstances/Heat/InputData.hpp"

using namespace MoReFEM;

#include "Test/Tools/CheckIdenticalFiles.hpp"
#include "Test/Tools/CompareEnsightFiles.hpp"
#include "Test/Tools/Fixture/Environment.hpp"


namespace // anonymous
{


    void CommonTestCase(std::string&& seq_or_par,
                        std::string&& dimension);


} // namespace anonymous



PRAGMA_DIAGNOSTIC(push)
# ifdef __clang__
PRAGMA_DIAGNOSTIC(ignored "-Wdisabled-macro-expansion")
# endif // __clang__


BOOST_FIXTURE_TEST_CASE(sequential_1d, TestNS::FixtureNS::Environment)
{
    CommonTestCase("Seq", "1D");
}


BOOST_FIXTURE_TEST_CASE(mpi4_1d, TestNS::FixtureNS::Environment)
{
    CommonTestCase("Mpi4", "1D");
}


BOOST_FIXTURE_TEST_CASE(sequential_2d, TestNS::FixtureNS::Environment)
{
    CommonTestCase("Seq", "2D");
}


BOOST_FIXTURE_TEST_CASE(mpi4_2d, TestNS::FixtureNS::Environment)
{
    CommonTestCase("Mpi4", "2D");
}


PRAGMA_DIAGNOSTIC(pop)


namespace // anonymous
{


    void CommonTestCase(std::string&& seq_or_par,
                        std::string&& dimension)
    {
        decltype(auto) environment = Utilities::Environment::GetInstance(__FILE__, __LINE__);
        std::string root_dir, output_dir;

        /* BOOST_REQUIRE_NO_THROW */(root_dir = environment.GetEnvironmentVariable("MOREFEM_ROOT", __FILE__, __LINE__));
        /* BOOST_REQUIRE_NO_THROW */(output_dir = environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR", __FILE__, __LINE__));

        BOOST_REQUIRE(FilesystemNS::Folder::DoExist(root_dir));
        BOOST_REQUIRE(FilesystemNS::Folder::DoExist(output_dir));

        std::string ref_dir = root_dir + "/Sources/ModelInstances/Heat/ExpectedResults/" + dimension;
        std::string obtained_dir = output_dir + std::string("/") + seq_or_par + std::string("/Heat/") + dimension;

        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "input_data.lua", __FILE__, __LINE__);
        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "model_name.hhdata", __FILE__, __LINE__);
        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "unknowns.hhdata", __FILE__, __LINE__);

        ref_dir += "/Mesh_1/";
        obtained_dir += "/Mesh_1/";

        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "interfaces.hhdata", __FILE__, __LINE__);

        ref_dir += "Ensight6/";
        obtained_dir += "Ensight6/";

        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "mesh.geo", __FILE__, __LINE__);
        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "problem.case", __FILE__, __LINE__);

        std::ostringstream oconv;

        for (auto i = 0; i <= 20; ++i)
        {
            oconv.str("");
            oconv << "temperature." << std::setw(5) << std::setfill('0') << i << ".scl";
            TestNS::CompareEnsightFiles(ref_dir, obtained_dir, oconv.str(), __FILE__, __LINE__);
        }

    }


} // namespace anonymous
