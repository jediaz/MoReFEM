Few models let in the library to show how a model is defined.

The models are:

- Heat
- Laplacian
- Rivlin cube 
- Elasticity
- Hyperelasticity
- Stokes