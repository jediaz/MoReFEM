//! \file 
//
//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "ModelInstances/___VARIABLE_groupName:identifier___/Model.hpp"
#include "ModelInstances/___VARIABLE_groupName:identifier___/InputData.hpp"


using namespace MoReFEM;


int main(int argc, char** argv)
{
    using InputData = ___VARIABLE_problemName:identifier___NS::InputData;
    
    try
    {
        MoReFEMData<InputData> morefem_data(argc, argv);
                
        decltype(auto) input_data = morefem_data.GetInputData();
        decltype(auto) mpi = morefem_data.GetMpi();
        
        try
        {
            ___VARIABLE_problemName:identifier___NS::Model model(morefem_data);
            model.Run();
            
            input_data.PrintUnused(std::cout);
        }
        catch(const std::exception& e)
        {
            ExceptionNS::PrintAndAbort(mpi, e.what());
        }
        catch(Seldon::Error& e)
        {
            ExceptionNS::PrintAndAbort(mpi, e.What());
        }
    }
    catch(const std::exception& e)
    {
        std::ostringstream oconv;
        oconv << "Exception caught from MoReFEMData<InputData>: " << e.what() << std::endl;
        
        std::cout << oconv.str();
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;
}

