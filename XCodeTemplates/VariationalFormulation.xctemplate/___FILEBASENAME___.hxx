//! \file 
//
//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

#ifndef _____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HXX
# define _____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HXX


namespace MoReFEM
{


    namespace ___VARIABLE_problemName:identifier___NS
    {

        
        inline Wrappers::Petsc::Snes::SNESFunction VariationalFormulation::ImplementSnesFunction() const
        {
            // TODO: The three ImplementSnesXXX() methods are there to tell Petsc SNES (Newton) algorithm
            // how to proceed. They must be meaningfully filled if SolveNonLinear() is called.
            // If you do a direct solve and don't want to bother with Newton solve, simply return nullptr.
            return nullptr;
        }
        

        inline Wrappers::Petsc::Snes::SNESJacobian VariationalFormulation::ImplementSnesJacobian() const
        {
            // TODO: Same comment as in ImplementSnesFunction().
            return nullptr;
        }
        

        inline Wrappers::Petsc::Snes::SNESViewer VariationalFormulation::ImplementSnesViewer() const
        {
            // TODO: Same comment as in ImplementSnesFunction().
            return nullptr;
        }
        
        
        inline Wrappers::Petsc::Snes::SNESConvergenceTestFunction VariationalFormulation::ImplementSnesConvergenceTestFunction() const
        {
            // TODO: Same comment as in ImplementSnesFunction().
            // However, this one might be nullptr even if others above are not.
            return nullptr;
        }
        
        
    } // namespace ___VARIABLE_problemName:identifier___NS
    

} // namespace MoReFEM


#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HXX) */
