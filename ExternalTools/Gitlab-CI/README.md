- common.yml encompasses the stuff that is used in all Yaml files (template to provide compilation and check warnings jobs for macos and linux, the jobs to handle Doxygen documentation).
- generate_yaml.py is a script to generate a tailored Yaml file.

A possible use is for instance to type:

python generate_configuration.py > my-gitlab-ci.yml

which will put the configuration displayed in the main of this program in the target Yaml file.

- gitlab-ci-no-macos.yml and gitlab-ci.yml are two such outputs; both provide 6 Linux cases and one of them add two macOS build on top of this.