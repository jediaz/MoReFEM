

<!-- toc -->

- [Introduction](#introduction)
- [Basic set up of the VM](#basic-set-up-of-the-vm)
  * [VM choice on CI platform](#vm-choice-on-ci-platform)
  * [Update Ubuntu](#update-ubuntu)
  * [Connect the additional disk](#connect-the-additional-disk)
  * [Install and set-up Docker](#install-and-set-up-docker)
  * [Install and set-up gitlab-runner](#install-and-set-up-gitlab-runner)
  * [Fix communication error](#fix-communication-error)

<!-- tocstop -->

# Introduction

The given procedure here explains how to set up a basic virtual machine on which a Docker executor may be installed.

Currently 6 such VMs are created on the [CI platform](https://ci.inria.fr) provided by Inria; at some point a Docker farm may be deployed in which case we won't need anymore to set up our own machines to run Docker executors.

# Basic set up of the VM

## VM choice on CI platform

The choice is made to take the most beefy configuration provided by the CI service: 4 processors with 12 Go RAM (FYI the immediately inferior one is 1 processor with 8 Go) with an additional 100 Go storage (all Linux VMs are provided only 20 Go so we would reach it in the very first use of the executor).

I chose to use Ubuntu 16.04 as system, but as the code itself will be run into a Docker container it doesn't matter much (I found Docker installation is slightly easier in Ubuntu than in Fedora).

## Update Ubuntu

Once the VM is created, log onto it and update it.

````
sudo apt update
sudo apt upgrade
````

## Connect the additional disk

````
sudo fdisk /dev/vda
````
Choose 'n', 'p', validate twice, and 'w'.

Then type:

````
sudo mkfs -t ext4 /dev/vda
sudo mkdir /media/suppl_drive
sudo nano -Bw /etc/fstab
````
In this file, add line:

``
    /dev/vda /media/suppl_drive ext4 defaults 0 2
``

Then type:
    
````
sudo mount -a
sudo chown -R ci:ci /media/suppl_drive
````


## Install and set-up Docker

````
sudo apt install docker.io
````

Remove the default Docker directory and create a new one on the additional disk:

````
sudo rm -rf /var/lib/docker
cd /var/lib
mkdir /media/suppl_drive/docker
sudo ln -s /media/suppl_drive/docker docker
sudo systemctl restart docker
sudo systemctl enable docker
````

## Install and set-up gitlab-runner

````
sudo wget -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
sudo chmod +x /usr/local/bin/gitlab-runner
mkdir /media/suppl_drive/gitlab-runner
sudo gitlab-runner install --user=ci --working-directory=/media/suppl_drive/gitlab-runner
sudo gitlab-runner start
````

Once this is done, you may register your runner through:
 
````
sudo gitlab-runner register
````

(you may have a look at the [official documentation](https://docs.gitlab.com/runner/register/index.html)).

## Fix communication error 

There is currently an issue with Inria configuration, so you need to add an option in config.toml file to make things run smoothly (see [here](https://gitlab.inria.fr/siteadmin/doc/wikis/faq#using-a-docker-executor) for the explanation). 

````
sudo nano /etc/gitlab-runner/config.toml
````
Add ``network_mode = "host"`` in _[runners.docker]_ section.


