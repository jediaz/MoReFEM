# How to add new members to both MoReREM CI projects?

## Connect on ci.inria.fr

Admistrator login is m3disim-ci@inria.fr

Password is the usual administration password of the team followed by the two characters ';_' (usual one was not accepted by the interface).

## Dashboard

* Click on _Dashboard_; you should see two projects __MoReREM__ and __MoReFEM-gitlab-ci__.

* Click on _Manage project_ on one of them.

## Add members

* Click on _Add members_, and then specify user's email in the block (this email must already been registered on the platform).

* Refresh the page until the email address is modified with (dot) and (at) instead of . and @.

* Click on _Slave Admin_.

* And do the exact same procedure for the second project.


